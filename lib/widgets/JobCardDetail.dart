import 'package:flutter/material.dart';
import 'package:flutter_dtic/models/Job.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../config.dart';

class JobCardDetail extends StatelessWidget {
  final Job job;

  const JobCardDetail({Key key, this.job}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final sizeAware = MediaQuery.of(context).size;

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      child: Container(
        margin: EdgeInsets.only(bottom: 8),
        height: 120,
        width: sizeAware.width,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(15),
            boxShadow: [
              BoxShadow(
                blurRadius: 5,
                color: Colors.grey[300],
                offset: Offset(1, 2),
                spreadRadius: 5,
              )
            ]),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                child: Image.network(
                  job.image,
                  height: 80,
                ),
              ),
              SizedBox(
                width: 30,
              ),
              Text(job.title_en),
            ],
          ),
        ),
      ),
    );
  }
}
