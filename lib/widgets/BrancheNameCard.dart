import 'package:flutter/material.dart';
import 'package:flutter_dtic/config.dart';
import 'package:flutter_dtic/models/Branche.dart';
import 'package:flutter_dtic/models/Category.dart';
import 'package:flutter_svg/svg.dart';

class BrancheNameCard extends StatelessWidget {
  final Branche branche;
  final bool choosen;

  const BrancheNameCard({Key key, this.branche, this.choosen})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    final sizeAware = MediaQuery.of(context).size;

    return Container(
      height: 50,
      child: Card(
        color: choosen ? Config.primaryColor : Colors.white,
        shape: ContinuousRectangleBorder(
          borderRadius: BorderRadius.circular(25),
        ),
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              branche.region ?? '',
              style: TextStyle(
                color: choosen ? Colors.white : Config.primaryColor,
                fontSize: sizeAware.width > sizeAware.height
                    ? sizeAware.height * 0.035
                    : sizeAware.width * 0.035,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
