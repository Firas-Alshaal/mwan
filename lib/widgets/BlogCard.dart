import 'package:flutter/material.dart';
import 'package:flutter_dtic/models/Blog.dart';

class BLogCard extends StatelessWidget {
  final Blog blog;

  const BLogCard({Key key, this.blog}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final sizeAware = MediaQuery.of(context).size;

    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, '/blog details',
            arguments: {'blog': blog});
      },
      child: Container(
        width: 200,
        height: sizeAware.height,
        padding: EdgeInsets.all(8),
        child: Card(
          elevation: 5,
          clipBehavior: Clip.hardEdge,
          shape: ContinuousRectangleBorder(
            borderRadius: BorderRadius.circular(35),
          ),
          child: Column(
            children: [
              Expanded(
                flex: 3,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(15),
                  child: Image.network(
                    blog.image_path,
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text(
                        blog.article_title_en,
                        style: TextStyle(fontSize: 16),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
