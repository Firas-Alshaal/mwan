import 'package:flutter/material.dart';
import 'package:skeleton_text/skeleton_text.dart';

class SkeletonText extends StatelessWidget {
  const SkeletonText({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        width: 100,
        height: 30,
        margin: EdgeInsets.all(8),
        child: SkeletonAnimation(
          shimmerColor: Colors.grey[300],
          // borderRadius:
          //     BorderRadius.circular(20),
          shimmerDuration: 1000,
          child: Container(
            decoration: BoxDecoration(
              color: Colors.grey[300],
              // borderRadius:
              //     BorderRadius.circular(20),
              // boxShadow: shadowList,
            ),
            // margin:
            //     EdgeInsets.only(top: 40),
          ),
        ),
      ),
    );
  }
}
