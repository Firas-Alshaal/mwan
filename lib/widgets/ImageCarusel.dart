import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter_dtic/config.dart';

class ImageCarusel extends StatelessWidget {
  final List images;
  final int product_id;

  const ImageCarusel({Key key, this.images, this.product_id}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Widget> list = [];
    for (int i = 0; i < images.length; i++) {
      list.add(
        ClipRRect(
          borderRadius: BorderRadius.circular(25),
          child: Hero(
            tag: '${product_id}${images[i]}+$i',
            child: CachedNetworkImage(
              imageUrl: images[i],
              placeholder: (context, url) => Center(child: CircularProgressIndicator()),
              errorWidget: (context, url, error) => Icon(Icons.error),
              fit: BoxFit.fill,
//              color: Colors.grey[200],
            ),
          ),
        ),
        // CachedNetworkImage(
        //   imageUrl: images[i],
        //   fit: BoxFit.fill,
        //   progressIndicatorBuilder: (context, url, downloadProgress) => Center(
        //       child:
        //           CircularProgressIndicator(value: downloadProgress.progress)),
        //   errorWidget: (context, url, error) => Icon(Icons.error),
        // ),
      );
    }
    return Carousel(
      boxFit: BoxFit.contain,
      images: list,
      autoplay: false,
      animationCurve: Curves.fastOutSlowIn,
      autoplayDuration: Duration(
        milliseconds: 10000,
      ),
      animationDuration: Duration(
        milliseconds: 1000,
      ),
      dotSize: 5.0,
      indicatorBgPadding: 5.0,
      dotBgColor: Colors.transparent,
      dotIncreasedColor: Config.primaryColor,
    );
  }
}
