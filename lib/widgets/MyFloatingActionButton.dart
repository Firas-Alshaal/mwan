import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../config.dart';

class MyFloatingActionButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      onPressed: () {
        Navigator.popUntil(context, (route) => route.isFirst);
        Navigator.pushNamed(context, '/products');
      },
      backgroundColor: Config.primaryColor,
      child: SvgPicture.asset(
        'assets/box.svg',
        width: 30,
      ),
    );
  }
}
