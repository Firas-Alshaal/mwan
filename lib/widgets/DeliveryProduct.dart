import 'package:flutter/material.dart';
import 'package:flutter_dtic/constants.dart';
import 'package:flutter_dtic/models/CartItem.dart';
import 'package:flutter_dtic/models/Product.dart';

import '../config.dart';

class DeliveryProduct extends StatelessWidget {
  final CartItem cartItem;

  const DeliveryProduct({Key key, this.cartItem}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final sizeAware = MediaQuery.of(context).size;

    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: Container(
        color: Colors.white,
        height: 130,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(15),
              child: Image.network(
                cartItem.product.images[0],
                height: sizeAware.height,
                width: sizeAware.width * 0.3,
                fit: BoxFit.fill,
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text(cartItem.product.name_en),
                    // Text(cartItem.product.overview_en),
                    Text('${cartItem.total_price}'),
                    Text(LANGUAGE == 'en'
                        ? 'quantity: ${cartItem.qty}'
                        : 'الكمية : ${cartItem.qty}'),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
