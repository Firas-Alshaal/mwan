import 'package:flutter/material.dart';
import 'package:flutter_dtic/config.dart';
import 'package:flutter_dtic/models/Category.dart';
import 'package:flutter_svg/svg.dart';

class CategoryCard extends StatelessWidget {
  final Category category;
  final bool choosen;

  const CategoryCard({Key key, this.category, this.choosen}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final sizeAware = MediaQuery.of(context).size;

    return Container(
      width: sizeAware.width * 0.35,
      height: sizeAware.height * 0.25,
      child: Card(
        color: choosen ? Config.primaryColor : Colors.white,
        shape: ContinuousRectangleBorder(
          borderRadius: BorderRadius.circular(25),
        ),
        child: Column(
          children: [
            Flexible(
              flex: 3,
              child: Container(
                child: Image.network(
                  choosen ? category.image2 : category.image,
                  fit: BoxFit.fill,
                  width: sizeAware.width * 0.35,
                  height: sizeAware.height * 0.25,
                ),
              ),
            ),
            Flexible(
              flex: 1,
              child: Container(
                child: Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: Text(
                    category.name_en ?? '',
                    style: TextStyle(
                      color: choosen ? Colors.white : Config.primaryColor,
                      fontWeight: FontWeight.w500,
                      fontSize: sizeAware.width > sizeAware.height
                          ? sizeAware.height * 0.03
                          : sizeAware.width * 0.03,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
