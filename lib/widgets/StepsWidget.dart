import 'package:flutter/material.dart';
import 'package:flutter_dtic/config.dart';

class StepsWidget extends StatelessWidget {
  final int step;

  const StepsWidget({Key key, this.step}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: 50,
            height: 50,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(100),
              color: step == 1 || step == 2 || step == 3
                  ? Config.primaryColor
                  : Colors.white,
              boxShadow: [
                BoxShadow(
                  blurRadius: 5,
                  color: Colors.grey[300],
                  offset: Offset(0, 2),
                  spreadRadius: 4,
                )
              ],
            ),
            child: Center(
                child: Text(
              '1',
              style: TextStyle(
                fontSize: 17,
                fontWeight: FontWeight.bold,
                color: step == 1 || step == 2 || step == 3
                    ? Colors.white
                    : Config.primaryColor,
              ),
            )),
          ),
          Container(
            color: Colors.grey,
            height: 1,
            width: 20,
          ),
          Container(
            width: 50,
            height: 50,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(100),
              color:
                  step == 2 || step == 3 ? Config.primaryColor : Colors.white,
              boxShadow: [
                BoxShadow(
                  blurRadius: 5,
                  color: Colors.grey[300],
                  offset: Offset(0, 2),
                  spreadRadius: 4,
                )
              ],
            ),
            child: Center(
              child: Text(
                '2',
                style: TextStyle(
                  fontSize: 17,
                  fontWeight: FontWeight.bold,
                  color: step == 2 || step == 3
                      ? Colors.white
                      : Config.primaryColor,
                ),
              ),
            ),
          ),
          Container(
            color: Colors.grey,
            height: 1,
            width: 20,
          ),
          Container(
            width: 50,
            height: 50,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(100),
              color: step == 3 ? Config.primaryColor : Colors.white,
              boxShadow: [
                BoxShadow(
                  blurRadius: 5,
                  color: Colors.grey[300],
                  offset: Offset(0, 2),
                  spreadRadius: 4,
                )
              ],
            ),
            child: Center(
                child: Text(
              '3',
              style: TextStyle(
                fontSize: 17,
                fontWeight: FontWeight.bold,
                color: step == 3 ? Colors.white : Config.primaryColor,
              ),
            )),
          ),
        ],
      ),
    );
  }
}
