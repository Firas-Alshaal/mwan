import 'package:flutter/material.dart';
import 'package:flutter_dtic/models/event.dart';

class EventsCard extends StatelessWidget {
  final Event event;

  const EventsCard({Key key, this.event}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final sizeAware = MediaQuery.of(context).size;

    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, '/event details', arguments: {
          'event': event,
        });
      },
      child: Container(
        width: 200,
        height: sizeAware.height,
        padding: EdgeInsets.all(8),
        child: Card(
          elevation: 5,
          clipBehavior: Clip.hardEdge,
          shape: ContinuousRectangleBorder(
            borderRadius: BorderRadius.circular(35),
          ),
          child: Column(
            children: [
              Expanded(
                flex: 3,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(15),
                  child: Image.network(
                    event.images[0],
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text(
                        event.name,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontSize: 16),
                      ),
                      Text(
                        event.start_date,
                        style: TextStyle(fontSize: 12, color: Colors.grey),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
