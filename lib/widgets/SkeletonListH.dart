import 'package:flutter/material.dart';
import 'package:skeleton_text/skeleton_text.dart';

class SkeletonList extends StatelessWidget {
  const SkeletonList({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        children: List.generate(
          5,
          (e) => Container(
            width: 250,
            height: 160,
            padding: EdgeInsets.all(8),
            child: SkeletonAnimation(
              shimmerColor: Colors.grey[300],

              // borderRadius:
              //     BorderRadius.circular(20),
              shimmerDuration: 1000,
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.grey[300],
                  borderRadius: BorderRadius.circular(20),
                  // boxShadow: shadowList,
                ),
                // margin:
                //     EdgeInsets.only(top: 40),
              ),
            ),
          ),
        ).toList(),
      ),
    );
  }
}
