import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dtic/blocs/cart/cart_bloc.dart';
import 'package:flutter_dtic/blocs/cart/cart_event.dart';
import 'package:flutter_dtic/constants.dart';
import 'package:flutter_dtic/models/CartItem.dart';

import '../config.dart';

class CartItemCard extends StatelessWidget {
  final CartItem cartItem;

  const CartItemCard({Key key, this.cartItem}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final sizeAware = MediaQuery.of(context).size;
    final cartBloc = BlocProvider.of<CartBloc>(context);

    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: Container(
        height: 130,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: Colors.white,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(15),
              child: Image.network(
                cartItem.product.images[0],
                height: sizeAware.height,
                width: sizeAware.width * 0.3,
                fit: BoxFit.fill,
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text(
                      LANGUAGE == 'en'
                          ? cartItem.product.name_en
                          : cartItem.product.name_ar,
                      overflow: TextOverflow.ellipsis,
                    ),
                    Text('${cartItem.total_price}'),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  IconButton(
                    icon: Icon(Icons.delete),
                    color: Colors.grey,
                    onPressed: () {
                      cartBloc.add(
                        DeleteItem(cartItem: cartItem),
                      );
                    },
                  ),
                  Container(
                    width: 100,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(color: Colors.grey[300]),
                      borderRadius: BorderRadius.circular(25),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        GestureDetector(
                          onTap: () {
                            cartBloc.add(Decrease(cartItem: cartItem));
                          },
                          child: Container(
                            width: 28,
                            height: 28,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(100),
                              color: Colors.red,
                            ),
                            child: Icon(
                              Icons.remove,
                              color: Colors.white,
                              size: 28,
                            ),
                          ),
                        ),
                        Text('${cartItem.qty}'),
                        GestureDetector(
                          onTap: () {
                            cartBloc.add(Increase(cartItem: cartItem));
                          },
                          child: Container(
                            width: 28,
                            height: 28,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(100),
                              color: Config.primaryColor,
                            ),
                            child: Icon(
                              Icons.add,
                              color: Colors.white,
                              size: 28,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
