import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dtic/blocs/auth/auth_bloc.dart';
import 'package:flutter_dtic/blocs/auth/auth_event.dart';
import 'package:flutter_dtic/blocs/auth/auth_state.dart';
import 'package:flutter_dtic/config.dart';
import 'package:flutter_dtic/localization/localization_constants.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:qrscans/qrscan.dart' as scanner;

import '../main.dart';

class MyDrawer extends StatefulWidget {
  @override
  _MyDrawerState createState() => _MyDrawerState();
}

class _MyDrawerState extends State<MyDrawer> {
  String prefLang = "EN";
  String _scanBarcode;
  String bar;
  Future<void> scanQR() async {
    String barcode = await scanner.scan();
    if (barcode == null) {
      print('nothing return.');
    } else {
      setState(() {
        // this._scanBarcode = barcode;
        this._scanBarcode = barcode;
      });
      Navigator.pushNamed(context, '/barcode search', arguments: {
        'code': _scanBarcode,
      });
    }
    print(_scanBarcode);
  }

  @override
  Widget build(BuildContext context) {
    final sizeAware = MediaQuery.of(context).size;
    final authBloc = BlocProvider.of<AuthBloc>(context);

    return Drawer(
      child: Scaffold(
        backgroundColor: Colors.white,
        /*appBar: AppBar(
          actions: [
            InkWell(
              onTap: () {
                Navigator.of(context).pop(context);
              },
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Icon(
                  Icons.close,
                  color: Colors.black,
                ),
              ),
            )
          ],
          leading: Container(),
          backgroundColor: Colors.white,
          elevation: 0,
          centerTitle: true,
        ),*/
        body: BlocBuilder(
          cubit: authBloc,
          builder: (context, state) {
            if (state is NotAuthenticated) {
              return Container(
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.bottomLeft,
                        end: Alignment.topRight,
                        colors: [HexColor('#F93E4F'), HexColor('#8D2CD3')])),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ListView(children: <Widget>[
                    Column(
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Image.asset(
                              'assets/mwanWhite.png',
                              width: 100,
                              height: 100,
                            ),
                            SizedBox(
                              height: 15,
                            ),
                          ],
                        ),
                        Divider(
                          height: sizeAware.height * 0.01,
                          color: Colors.black26,
                          thickness: 0.7,
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.pushNamed(context, '/login');
                          },
                          child: Container(
                            height: sizeAware.height * 0.08,
                            child: Center(
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    height: sizeAware.height * 0.04,
                                    width: sizeAware.width * 0.1,
                                    child: SvgPicture.asset(
                                      'assets/login.svg',
                                      color: Colors.white,
                                    ),
                                  ),
                                  SizedBox(
                                    width: sizeAware.width * 0.04,
                                  ),
                                  Text(
                                    getTranslated(context, 'Login'),
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 18,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Divider(
                          height: sizeAware.height * 0.01,
                          color: Colors.black26,
                          thickness: 0.7,
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.pushNamed(context, '/register');
                          },
                          child: Container(
                            height: sizeAware.height * 0.08,
                            child: Center(
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    height: sizeAware.height * 0.04,
                                    width: sizeAware.width * 0.1,
                                    child: SvgPicture.asset(
                                        'assets/register.svg',
                                        color: Colors.white),
                                  ),
                                  SizedBox(
                                    width: sizeAware.width * 0.04,
                                  ),
                                  Text(
                                    getTranslated(context, 'Register'),
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 18,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Divider(
                          height: sizeAware.height * 0.01,
                          color: Colors.black26,
                          thickness: 0.7,
                        ),
                        // InkWell(
                        //   onTap: () {
                        //     Navigator.pushNamed(context, '/blogs');
                        //   },
                        //   child: Container(
                        //     height: sizeAware.height * 0.08,
                        //     child: Center(
                        //       child: Row(
                        //         children: <Widget>[
                        //           Container(
                        //             height: sizeAware.height * 0.04,
                        //             width: sizeAware.width * 0.1,
                        //             child:
                        //                 SvgPicture.asset('assets/blogging.svg'),
                        //           ),
                        //           SizedBox(
                        //             width: sizeAware.width * 0.04,
                        //           ),
                        //           Text(
                        //             getTranslated(context, 'Blogs'),
                        //             style: TextStyle(
                        //               fontSize: 18,
                        //             ),
                        //           ),
                        //         ],
                        //       ),
                        //     ),
                        //   ),
                        // ),
                        // Divider(
                        //   height: sizeAware.height * 0.01,
                        //   color: Colors.black26,
                        //   thickness: 0.7,
                        // ),
                        InkWell(
                          onTap: () {
                            Navigator.pushNamed(context, '/contact us');
                          },
                          child: Container(
                            height: sizeAware.height * 0.08,
                            child: Center(
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    height: sizeAware.height * 0.04,
                                    width: sizeAware.width * 0.1,
                                    child: SvgPicture.asset('assets/office.svg',
                                        color: Colors.white),
                                  ),
                                  SizedBox(
                                    width: sizeAware.width * 0.04,
                                  ),
                                  Text(
                                    getTranslated(context, 'Contact Us'),
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 18,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Divider(
                          height: sizeAware.height * 0.01,
                          color: Colors.black26,
                          thickness: 0.7,
                        ),
                        InkWell(
                          onTap: () {},
                          child: Container(
                            height: sizeAware.height * 0.08,
                            child: Center(
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    height: sizeAware.height * 0.04,
                                    width: sizeAware.width * 0.1,
                                    child: SvgPicture.asset(
                                        'assets/language.svg',
                                        color: Colors.white),
                                  ),
                                  SizedBox(
                                    width: sizeAware.width * 0.04,
                                  ),
                                  Expanded(
                                    child: Center(
                                      child: Container(
                                        width: sizeAware.width,
                                        height: sizeAware.height * 0.08,
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(30.0),
                                        ),
                                        child: Center(
                                          child: DropdownButton<String>(
                                            icon: Icon(
                                              Icons.keyboard_arrow_down,
                                              color: Colors.white,
                                            ),
                                            isExpanded: true,
                                            underline: SizedBox(),
                                            items: [
                                              DropdownMenuItem<String>(
                                                value: "ar",
                                                child: Text(
                                                  "العربية",
                                                ),
                                              ),
                                              DropdownMenuItem<String>(
                                                value: "en",
                                                child: Text(
                                                  "English",
                                                ),
                                              ),
                                            ],
                                            onChanged: (value) async {
                                              if (value == "en") {
                                                setState(() {
                                                  prefLang = "English";
                                                });
                                              } else {
                                                setState(() {
                                                  prefLang = value;
                                                });
                                              }

                                              await changeLanguage(value);
                                              Navigator.popUntil(context,
                                                  (route) => route.isFirst);
                                              Navigator.pushReplacementNamed(
                                                  context, '/home');
                                            },
                                            hint: Text(
                                              "${getTranslated(context, "Preferred Language")}",
                                              style: TextStyle(
                                                  color: Colors.white),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Divider(
                          height: sizeAware.height * 0.01,
                          color: Colors.black26,
                          thickness: 0.7,
                        ),
                        // InkWell(
                        //   onTap: () {
                        //     Navigator.pushNamed(context, '/jobs');
                        //   },
                        //   child: Container(
                        //     height: sizeAware.height * 0.08,
                        //     child: Center(
                        //       child: Row(
                        //         children: <Widget>[
                        //           Container(
                        //             height: sizeAware.height * 0.04,
                        //             width: sizeAware.width * 0.1,
                        //             child: SvgPicture.asset(
                        //                 'assets/job-seeker.svg',
                        //                 color: Colors.white),
                        //           ),
                        //           SizedBox(
                        //             width: sizeAware.width * 0.04,
                        //           ),
                        //           Text(
                        //             getTranslated(context, 'Apply to job'),
                        //             style: TextStyle(
                        //               color: Colors.white,
                        //               fontSize: 18,
                        //             ),
                        //           ),
                        //         ],
                        //       ),
                        //     ),
                        //   ),
                        // ),
                        // Divider(
                        //   height: sizeAware.height * 0.01,
                        //   color: Colors.black26,
                        //   thickness: 0.7,
                        // ),

                        InkWell(
                          onTap: () {
                            Navigator.pushNamed(context, '/contactus');
                          },
                          child: Container(
                            height: sizeAware.height * 0.08,
                            child: Center(
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    height: sizeAware.height * 0.04,
                                    width: sizeAware.width * 0.1,
                                    child: SvgPicture.asset(
                                        'assets/contact.svg',
                                        color: Colors.white),
                                  ),
                                  SizedBox(
                                    width: sizeAware.width * 0.04,
                                  ),
                                  Text(
                                    getTranslated(context, 'ContactUs'),
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 18,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Divider(
                          height: sizeAware.height * 0.01,
                          color: Colors.black26,
                          thickness: 0.7,
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.pushNamed(context, '/about us');
                          },
                          child: Container(
                            height: sizeAware.height * 0.08,
                            child: Center(
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    height: sizeAware.height * 0.04,
                                    width: sizeAware.width * 0.1,
                                    child: SvgPicture.asset('assets/about.svg',
                                        color: Colors.white),
                                  ),
                                  SizedBox(
                                    width: sizeAware.width * 0.04,
                                  ),
                                  Text(
                                    getTranslated(context, 'About Us'),
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 18,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    )
                  ]),
                ),
              );
            }
            if (state is Authenticated) {
              return Container(
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topRight,
                        end: Alignment.bottomLeft,
                        colors: [HexColor('#F93E4F'), HexColor('#8D2CD3')])),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ListView(children: <Widget>[
                    Column(
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Image.asset(
                              'assets/mwanWhite.png',
                              width: 100,
                              height: 100,
                            ),
                            SizedBox(
                              height: 15,
                            ),
                          ],
                        ),
                        Divider(
                          height: sizeAware.height * 0.01,
                          color: Colors.black26,
                          thickness: 0.7,
                        ),
                        // InkWell(
                        //   onTap: () {
                        //     Navigator.pushNamed(context, '/blogs');
                        //   },
                        //   child: Container(
                        //     height: sizeAware.height * 0.08,
                        //     child: Center(
                        //       child: Row(
                        //         children: <Widget>[
                        //           Container(
                        //             height: sizeAware.height * 0.04,
                        //             width: sizeAware.width * 0.1,
                        //             child:
                        //                 SvgPicture.asset('assets/blogging.svg'),
                        //           ),
                        //           SizedBox(
                        //             width: sizeAware.width * 0.04,
                        //           ),
                        //           Text(
                        //             getTranslated(context, 'Blogs'),
                        //             style: TextStyle(
                        //               fontSize: 18,
                        //             ),
                        //           ),
                        //         ],
                        //       ),
                        //     ),
                        //   ),
                        // ),
                        // Divider(
                        //   height: sizeAware.height * 0.01,
                        //   color: Colors.black26,
                        //   thickness: 0.7,
                        // ),
                        InkWell(
                          onTap: () {
                            scanQR();
                          },
                          child: Container(
                            height: sizeAware.height * 0.08,
                            child: Center(
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    height: sizeAware.height * 0.04,
                                    width: sizeAware.width * 0.1,
                                    child: SvgPicture.asset(
                                        'assets/qr-code.svg',
                                        color: Colors.white),
                                  ),
                                  SizedBox(
                                    width: sizeAware.width * 0.04,
                                  ),
                                  Text(
                                    getTranslated(context, 'Barcode Scanner'),
                                    style: TextStyle(
                                        fontSize: 18, color: Colors.white),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Divider(
                          height: sizeAware.height * 0.01,
                          color: Colors.black26,
                          thickness: 0.7,
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.pushNamed(context, '/search order');
                          },
                          child: Container(
                            height: sizeAware.height * 0.08,
                            child: Center(
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    height: sizeAware.height * 0.04,
                                    width: sizeAware.width * 0.1,
                                    child: SvgPicture.asset(
                                      'assets/checked.svg',
                                      color: Colors.white,
                                    ),
                                  ),
                                  SizedBox(
                                    width: sizeAware.width * 0.04,
                                  ),
                                  Text(
                                    getTranslated(context, 'Check Order'),
                                    style: TextStyle(
                                        fontSize: 18, color: Colors.white),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Divider(
                          height: sizeAware.height * 0.01,
                          color: Colors.black26,
                          thickness: 0.7,
                        ),

                        // InkWell(
                        //   onTap: () {
                        //     Navigator.pushNamed(context, '/jobs');
                        //   },
                        //   child: Container(
                        //     height: sizeAware.height * 0.08,
                        //     child: Center(
                        //       child: Row(
                        //         children: <Widget>[
                        //           Container(
                        //             height: sizeAware.height * 0.04,
                        //             width: sizeAware.width * 0.1,
                        //             child: SvgPicture.asset(
                        //                 'assets/job-seeker.svg',
                        //                 color: Colors.white),
                        //           ),
                        //           SizedBox(
                        //             width: sizeAware.width * 0.04,
                        //           ),
                        //           Text(
                        //             getTranslated(context, 'Apply to job'),
                        //             style: TextStyle(
                        //                 fontSize: 18, color: Colors.white),
                        //           ),
                        //         ],
                        //       ),
                        //     ),
                        //   ),
                        // ),
                        // Divider(
                        //   height: sizeAware.height * 0.01,
                        //   color: Colors.black26,
                        //   thickness: 0.7,
                        // ),

                        InkWell(
                          onTap: () {
                            Navigator.pushNamed(context, '/contactus');
                          },
                          child: Container(
                            height: sizeAware.height * 0.08,
                            child: Center(
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    height: sizeAware.height * 0.04,
                                    width: sizeAware.width * 0.1,
                                    child: SvgPicture.asset(
                                        'assets/contact.svg',
                                        color: Colors.white),
                                  ),
                                  SizedBox(
                                    width: sizeAware.width * 0.04,
                                  ),
                                  Text(
                                    getTranslated(context, 'ContactUs'),
                                    style: TextStyle(
                                        fontSize: 18, color: Colors.white),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Divider(
                          height: sizeAware.height * 0.01,
                          color: Colors.black26,
                          thickness: 0.7,
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.pushNamed(context, '/about us');
                          },
                          child: Container(
                            height: sizeAware.height * 0.08,
                            child: Center(
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    height: sizeAware.height * 0.04,
                                    width: sizeAware.width * 0.1,
                                    child: SvgPicture.asset('assets/about.svg',
                                        color: Colors.white),
                                  ),
                                  SizedBox(
                                    width: sizeAware.width * 0.04,
                                  ),
                                  Text(
                                    getTranslated(context, 'About Us'),
                                    style: TextStyle(
                                        fontSize: 18, color: Colors.white),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Divider(
                          height: sizeAware.height * 0.01,
                          color: Colors.black26,
                          thickness: 0.7,
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.pushNamed(context, '/contact us');
                          },
                          child: Container(
                            height: sizeAware.height * 0.08,
                            child: Center(
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    height: sizeAware.height * 0.04,
                                    width: sizeAware.width * 0.1,
                                    child: SvgPicture.asset('assets/office.svg',
                                        color: Colors.white),
                                  ),
                                  SizedBox(
                                    width: sizeAware.width * 0.04,
                                  ),
                                  Text(
                                    getTranslated(context, 'Contact Us'),
                                    style: TextStyle(
                                        fontSize: 18, color: Colors.white),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Divider(
                          height: sizeAware.height * 0.01,
                          color: Colors.black26,
                          thickness: 0.7,
                        ),

                        InkWell(
                          onTap: () {},
                          child: Container(
                            height: sizeAware.height * 0.08,
                            child: Center(
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    height: sizeAware.height * 0.04,
                                    width: sizeAware.width * 0.1,
                                    child: SvgPicture.asset(
                                        'assets/language.svg',
                                        color: Colors.white),
                                  ),
                                  SizedBox(
                                    width: sizeAware.width * 0.04,
                                  ),
                                  Expanded(
                                    child: Center(
                                      child: Container(
                                        width: sizeAware.width,
                                        height: sizeAware.height * 0.08,
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(30.0),
                                        ),
                                        child: Center(
                                          child: DropdownButton<String>(
                                            icon: Icon(
                                              Icons.keyboard_arrow_down,
                                              color: Colors.white,
                                            ),
                                            isExpanded: true,
                                            underline: SizedBox(),
                                            items: [
                                              DropdownMenuItem<String>(
                                                value: "ar",
                                                child: Text(
                                                  "العربية",
                                                ),
                                              ),
                                              DropdownMenuItem<String>(
                                                value: "en",
                                                child: Text(
                                                  "English",
                                                ),
                                              ),
                                            ],
                                            onChanged: (value) async {
                                              if (value == "en") {
                                                setState(() {
                                                  prefLang = "English";
                                                });
                                              } else {
                                                setState(() {
                                                  prefLang = value;
                                                });
                                              }

                                              await changeLanguage(value);
                                              Navigator.popUntil(context,
                                                  (route) => route.isFirst);
                                              Navigator.pushReplacementNamed(
                                                  context, '/home');
                                            },
                                            hint: Text(
                                              "${getTranslated(context, "Preferred Language")}",
                                              style: TextStyle(
                                                  color: Colors.white),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Divider(
                          height: sizeAware.height * 0.01,
                          color: Colors.black26,
                          thickness: 0.7,
                        ),
                        InkWell(
                          onTap: () {
                            authBloc.add(LogoutRequested());
                          },
                          child: Container(
                            height: sizeAware.height * 0.08,
                            child: Center(
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    height: sizeAware.height * 0.04,
                                    width: sizeAware.width * 0.1,
                                    child: SvgPicture.asset('assets/exit.svg',
                                        color: Colors.white),
                                  ),
                                  SizedBox(
                                    width: sizeAware.width * 0.04,
                                  ),
                                  Text(
                                    getTranslated(context, 'Logout'),
                                    style: TextStyle(
                                        fontSize: 18, color: Colors.white),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    )
                  ]),
                ),
              );
            }
            return Container();
          },
        ),
      ),
    );
  }

  void changeLanguage(languageCode) async {
    Locale temp = await setLocale(languageCode);
    MyApp.setLocale(context, temp);
  }
}
