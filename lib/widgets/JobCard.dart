import 'package:flutter/material.dart';
import 'package:flutter_dtic/localization/localization_constants.dart';
import 'package:flutter_dtic/models/Job.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hexcolor/hexcolor.dart';

import '../config.dart';

class JobCard extends StatelessWidget {
  final Job job;

  const JobCard({Key key, this.job}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final sizeAware = MediaQuery.of(context).size;

    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, '/single job', arguments: {
          'job': job,
        });
      },
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        child: Container(
          margin: EdgeInsets.only(bottom: 8),
          height: 120,
          width: sizeAware.width,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(15),
              boxShadow: [
                BoxShadow(
                  blurRadius: 5,
                  color: Colors.grey[300],
                  offset: Offset(1, 2),
                  spreadRadius: 5,
                )
              ]),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  child: job.image == null? Image.asset('assets/Tracking.png') :Image.network(
                    job.image,
                    width: 50,
                  ),
                ),
                Expanded(child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(job.title_en),
                )),
                Container(
                  width: 100,
                  height: 100,
                  decoration: BoxDecoration(
                    //color: Config.primaryColor,
                    gradient: LinearGradient(
                        begin: Alignment.topRight,
                        end: Alignment.bottomLeft,
                        colors: [HexColor('#F93E4F'), HexColor('#8D2CD3')]),
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Center(
                    child: Text(
                      getTranslated(context, 'Apply'),
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
