import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dtic/blocs/profile/profile_bloc.dart';
import 'package:flutter_dtic/blocs/profile/profile_event.dart';
import 'package:flutter_dtic/blocs/profile/profile_state.dart';
import 'package:flutter_dtic/config.dart';
import 'package:flutter_dtic/models/Order.dart';
import 'package:fluttertoast/fluttertoast.dart';

class OrderCard extends StatelessWidget {
  final Order order;
  final ProfileBloc profileBloc;

  const OrderCard({Key key, this.order, this.profileBloc}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: Container(
        height: 60,
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              blurRadius: 5,
              color: Colors.grey[300],
              offset: Offset(1, 2),
              spreadRadius: 5,
            ),
          ],
          borderRadius: BorderRadius.circular(15),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text(
              order.order_no,
              style: TextStyle(color: Config.primaryColor),
            ),
            Text(
              order.requested_delivery_date,
              style: TextStyle(color: Config.primaryColor),
            ),
            order.status != 0
                ? Text('verified')
                : RaisedButton(
                    onPressed: () {
                      profileBloc.add(ResendCodeRequested(order_id: order.id));
                    },
                    color: Config.primaryColor,
                    child: Text(
                      'verify',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
          ],
        ),
      ),
    );
  }
}
