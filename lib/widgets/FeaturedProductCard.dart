import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dtic/constants.dart';
import 'package:flutter_dtic/models/Product.dart';

class FeaturedProductCard extends StatelessWidget {
  final Product product;

  const FeaturedProductCard({Key key, this.product}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final sizeAware = MediaQuery.of(context).size;

    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, '/product details', arguments: {
          'product': product,
        });
      },
      child: Container(
        width: 200,
        height: sizeAware.height,
        padding: EdgeInsets.all(8),
        child: Card(
          elevation: 5,
          clipBehavior: Clip.hardEdge,
          shape: ContinuousRectangleBorder(
            borderRadius: BorderRadius.circular(35),
          ),
          child: Column(
            children: [
              Expanded(
                flex: 3,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(15),
                  // child: Hero(
                  //   tag: '${product.images[0]}',
                  child: CachedNetworkImage(
                    imageUrl: product.images[0],
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              // ),
              Expanded(
                flex: 1,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    LANGUAGE == 'en' ? product.name_en : product.name_ar,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
