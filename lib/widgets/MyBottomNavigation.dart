import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dtic/blocs/cart/cart_bloc.dart';
import 'package:flutter_dtic/blocs/cart/cart_event.dart';
import 'package:flutter_dtic/blocs/cart/cart_state.dart';
import 'package:flutter_dtic/config.dart';
import 'package:flutter_dtic/constants.dart';
import 'package:flutter_dtic/localization/localization_constants.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';

class MyBottomNavigation extends StatefulWidget {
  final int index;

  const MyBottomNavigation({Key key, this.index}) : super(key: key);

  @override
  _MyBottomNavigationState createState() => _MyBottomNavigationState();
}

class _MyBottomNavigationState extends State<MyBottomNavigation> {
  int itemNumber = 0;
  CartBloc cartBloc;

  @override
  void initState() {
    super.initState();
    cartBloc = BlocProvider.of<CartBloc>(context);
    cartBloc.add(CartRequested());
  }

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      shape: CircularNotchedRectangle(),
      notchMargin: 6.0,
      color: Colors.transparent,
      elevation: 9.0,
      clipBehavior: Clip.antiAlias,
      child: Container(
        height: 60.0,
        decoration: BoxDecoration(
          color: Colors.white,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Expanded(
              child: Container(
                height: 45.0,
                child: GestureDetector(
                  onTap: () {
                    if (widget.index != 0) {
                      Navigator.pushReplacementNamed(context, '/home');
                      /*if (ResgisterOk) {
                        Fluttertoast.showToast(toastLength: Toast.LENGTH_LONG,
                          msg: getTranslated(context, 'PhoneCityRequried'),
                        );
                      } else {
                        Navigator.pushReplacementNamed(context, '/home');
                      }*/
                    }
                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        child: SvgPicture.asset(
                          'assets/home.svg',
                          height: 20,
                          color: widget.index == 0
                              ? Config.primaryColor
                              : Colors.grey,
                        ),
                      ),
                      Text(
                        getTranslated(context, 'Home'),
                        style: TextStyle(
                          fontSize: 12,
                          color: widget.index == 0
                              ? Config.primaryColor
                              : Colors.grey,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Expanded(
              child: Container(
                height: 45.0,
                child: GestureDetector(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        child: SvgPicture.asset(
                          'assets/search.svg',
                          height: 20,
                          color: widget.index == 1
                              ? Config.primaryColor
                              : Colors.grey,
                        ),
                      ),
                      Text(
                        getTranslated(context, 'Search'),
                        style: TextStyle(
                          fontSize: 12,
                          color: widget.index == 1
                              ? Config.primaryColor
                              : Colors.grey,
                        ),
                      )
                    ],
                  ),
                  onTap: () {
                    if (widget.index != 1) {
                      Navigator.popUntil(context, (route) => route.isFirst);
                      Navigator.pushNamed(context, '/search');
                      /*if (ResgisterOk) {
                        Fluttertoast.showToast(toastLength: Toast.LENGTH_LONG,
                          msg: getTranslated(context, 'PhoneCityRequried'),
                        );
                      } else {
                        Navigator.popUntil(context, (route) => route.isFirst);
                        Navigator.pushNamed(context, '/search');
                      }*/
                    }
                  },
                ),
              ),
            ),
            SizedBox(
              width: 30,
            ),
            BlocBuilder(
              cubit: cartBloc,
              builder: (context, state) {
                if (state is CartLoadSuccess ||
                    state is AddedToCart ||
                    state is OrderFaield) {
                  if (state is CartLoadSuccess || state is AddedToCart) {
                    itemNumber = 0;
                    for (int i = 0; i < state.items.length; i++) {
                      itemNumber = min(99, itemNumber + state.items[i].qty);
                    }
                  }
                  return itemNumber != 0
                      ? Expanded(
                          child: Container(
                            height: 45.0,
                            clipBehavior: Clip.none,
                            child: GestureDetector(
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Expanded(
                                    child: Stack(
                                      overflow: Overflow.visible,
                                      children: [
                                        SvgPicture.asset(
                                          'assets/shopping-cart.svg',
                                          height: 25,
                                          width: 25,
                                          color: widget.index == 2
                                              ? Config.primaryColor
                                              : Colors.grey,
                                        ),
                                        Positioned(
                                          right: -3,
                                          child: Container(
                                            clipBehavior: Clip.none,
                                            width: 17,
                                            height: 17,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(
                                                100,
                                              ),
                                              color: Colors.red,
                                            ),
                                            child: Center(
                                              child: Text(
                                                '$itemNumber',
                                                style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 10,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Text(getTranslated(context, 'Cart'),
                                      style: TextStyle(
                                        fontSize: 12,
                                        color: widget.index == 2
                                            ? Config.primaryColor
                                            : Colors.grey,
                                      ))
                                ],
                              ),
                              onTap: () {
                                if (widget.index != 2) {
                                  Navigator.popUntil(
                                      context, (route) => route.isFirst);

                                  Navigator.pushNamed(context, '/cart');
                                  /*if (ResgisterOk) {
                                    Fluttertoast.showToast(toastLength: Toast.LENGTH_LONG,
                                      msg: getTranslated(
                                          context, 'PhoneCityRequried'),
                                    );
                                  } else {
                                    Navigator.popUntil(
                                        context, (route) => route.isFirst);

                                    Navigator.pushNamed(context, '/cart');
                                  }*/
                                }
                              },
                            ),
                          ),
                        )
                      : Expanded(
                          child: Container(
                            height: 45.0,
                            child: GestureDetector(
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Expanded(
                                    child: SvgPicture.asset(
                                      'assets/shopping-cart.svg',
                                      height: 20,
                                      color: widget.index == 2
                                          ? Config.primaryColor
                                          : Colors.grey,
                                    ),
                                  ),
                                  Text(getTranslated(context, 'Cart'),
                                      style: TextStyle(
                                        fontSize: 12,
                                        color: widget.index == 2
                                            ? Config.primaryColor
                                            : Colors.grey,
                                      ))
                                ],
                              ),
                              onTap: () {
                                if (widget.index != 2) {
                                  Navigator.popUntil(
                                      context, (route) => route.isFirst);

                                  Navigator.pushNamed(context, '/cart');
                                  /*if (ResgisterOk) {
                                    Fluttertoast.showToast(toastLength: Toast.LENGTH_LONG,
                                      msg: getTranslated(
                                          context, 'PhoneCityRequried'),
                                    );
                                  } else {
                                    Navigator.popUntil(
                                        context, (route) => route.isFirst);

                                    Navigator.pushNamed(context, '/cart');
                                  }*/
                                }
                              },
                            ),
                          ),
                        );
                } else
                  return Expanded(
                    child: Container(
                      height: 45.0,
                      child: GestureDetector(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Expanded(
                              child: SvgPicture.asset(
                                'assets/shopping-cart.svg',
                                height: 25,
                                width: 25,
                                color: widget.index == 2
                                    ? Config.primaryColor
                                    : Colors.grey,
                              ),
                            ),
                            Text(getTranslated(context, 'Cart'),
                                style: TextStyle(
                                  fontSize: 12,
                                  color: widget.index == 2
                                      ? Config.primaryColor
                                      : Colors.grey,
                                ))
                          ],
                        ),
                        onTap: () {
                          if (widget.index != 2) {
                            Navigator.popUntil(
                                context, (route) => route.isFirst);

                            Navigator.pushNamed(context, '/cart');
                            /*if (ResgisterOk) {
                              Fluttertoast.showToast(toastLength: Toast.LENGTH_LONG,
                                msg:
                                    getTranslated(context, 'PhoneCityRequried'),
                              );
                            } else {
                              Navigator.popUntil(
                                  context, (route) => route.isFirst);

                              Navigator.pushNamed(context, '/cart');
                            }*/
                          }
                        },
                      ),
                    ),
                  );
              },
            ),
            Expanded(
              child: Container(
                height: 45.0,
                child: GestureDetector(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        child: SvgPicture.asset(
                          'assets/user.svg',
                          height: 20,
                          color: widget.index == 3
                              ? Config.primaryColor
                              : Colors.grey,
                        ),
                      ),
                      Text(
                        getTranslated(context, 'Profile'),
                        style: TextStyle(
                          fontSize: 12,
                          color: widget.index == 3
                              ? Config.primaryColor
                              : Colors.grey,
                        ),
                      ),
                    ],
                  ),
                  onTap: () {
                    if (widget.index != 3) {
                      if (ISAuth) {
                        Navigator.popUntil(context, (route) => route.isFirst);
                        Navigator.pushNamed(context, '/profile');
                      } else {
                        Fluttertoast.showToast(
                            msg: 'You should to login first',
                            toastLength: Toast.LENGTH_LONG,
                        );
                      }
                    }
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
