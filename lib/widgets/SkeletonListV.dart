import 'package:flutter/material.dart';
import 'package:skeleton_text/skeleton_text.dart';

class SkeletonListV extends StatelessWidget {
  const SkeletonListV({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final sizeAware = MediaQuery.of(context).size;

    return GridView.count(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      crossAxisCount: 2,
      crossAxisSpacing: 3,
      childAspectRatio: 0.8,
      children: List.generate(4, (i) {
        return Container(
          width: 200,
          height: sizeAware.height,
          decoration: BoxDecoration(),
          margin: EdgeInsets.all(8),
          child: SkeletonAnimation(
            shimmerColor: Colors.grey[300],

            // borderRadius:
            //     BorderRadius.circular(20),
            shimmerDuration: 1000,
            child: Container(
              decoration: BoxDecoration(
                color: Colors.grey[300],
                borderRadius: BorderRadius.circular(20),
                // boxShadow: shadowList,
              ),
              // margin:
              //     EdgeInsets.only(top: 40),
            ),
          ),
        );
      }),
    );
  }
}
