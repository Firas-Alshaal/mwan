import 'package:flutter/material.dart';
import 'package:flutter_dtic/config.dart';
import 'package:flutter_dtic/localization/localization_constants.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Loading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final sizeAware = MediaQuery.of(context).size;

    return Scaffold(
      body: Container(
        width: sizeAware.width,
        height: sizeAware.height,
//        decoration: BoxDecoration(color: Config.primaryColor),
        child: Center(
          child: CircularProgressIndicator(),
        ),
      ),
    );
  }
}
