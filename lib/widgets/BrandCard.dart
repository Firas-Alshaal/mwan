import 'package:flutter/material.dart';
import 'package:flutter_dtic/models/Brand.dart';

class BrandCard extends StatelessWidget {
  final Brand brand;

  const BrandCard({Key key, this.brand}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, '/single brand', arguments: {
          'brand': brand,
        });
      },
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          width: 220,
          height: 150,
          child: Image.network(
            brand.logo,
            fit: BoxFit.fill,
          ),
        ),
      ),
    );
  }
}
