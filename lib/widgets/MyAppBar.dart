import 'package:flutter/cupertino.dart';

import 'package:flutter/material.dart';
import 'package:flutter_dtic/config.dart';
import 'package:hexcolor/hexcolor.dart';

Widget myAppBar(title, actions) {
  return AppBar(
      title: Text(title),
      actions: actions,
      shape: continuousRectangleBorder,
      centerTitle: true,
      //backgroundColor: Config.primaryColor,
      flexibleSpace: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: <Color>[ HexColor('#8D2CD3'),HexColor('#F93E4F'),])),
      ));
}

ContinuousRectangleBorder continuousRectangleBorder =
    ContinuousRectangleBorder();
