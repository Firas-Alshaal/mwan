import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dtic/blocs/cart/cart_bloc.dart';
import 'package:flutter_dtic/blocs/cart/cart_event.dart';
import 'package:flutter_dtic/config.dart';
import 'package:flutter_dtic/localization/localization_constants.dart';
import 'package:flutter_dtic/models/CartItem.dart';
import 'package:flutter_dtic/models/Product.dart';

class ProductCard extends StatefulWidget {
  final Product product;

  ProductCard({Key key, this.product}) : super(key: key);

  @override
  _ProductCardState createState() => _ProductCardState();
}

class _ProductCardState extends State<ProductCard> {
  int qty = 1;

  @override
  Widget build(BuildContext context) {
    final sizeAware = MediaQuery.of(context).size;
    final cartBloc = BlocProvider.of<CartBloc>(context);

    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, '/product details', arguments: {
          'product': widget.product,
        });
      },
      child: Padding(
        padding: const EdgeInsets.all(24.0),
        child: Container(
          height: sizeAware.height*0.65,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(25),
            gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [
                Colors.grey[200],
                Colors.grey[300],
              ],
            ),
            boxShadow: [
              BoxShadow(
                blurRadius: 11,
                color: Colors.black26,
                offset: Offset(2, 2),
                spreadRadius: 0.5,
              ),
            ],
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                flex: 5,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(25),
                  child: Image.network(widget.product.images[0],height: sizeAware.height*0.5,),
                ),
              ),
              Flexible(flex: 1,child: Text(widget.product.name_en)),
              Flexible(
                flex: 1,
                child: Container(
                  width: 100,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(25),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GestureDetector(
                        onTap: () {
                          if (qty > 1) {
                            setState(() {
                              qty--;
                            });
                          }
                        },
                        child: Container(
                          width: 28,
                          height: 28,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(100),
                            color: Colors.red,
                          ),
                          child: Icon(
                            Icons.remove,
                            color: Colors.white,
                            size: 28,
                          ),
                        ),
                      ),
                      Expanded(
                        child: Center(
                          child: Text('$qty'),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            qty++;
                          });
                        },
                        child: Container(
                          width: 28,
                          height: 28,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(100),
                            color: Config.primaryColor,
                          ),
                          child: Icon(
                            Icons.add,
                            color: Colors.white,
                            size: 28,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Flexible(
                flex: 2,
                child: InkWell(
                  hoverColor: Colors.red,
                  splashColor: Colors.red,
                  onTap: () {
                    cartBloc.add(AddToCartRequested(
                        cartItem: CartItem(
                      product: widget.product,
                      qty: qty,
                      total_price: qty * widget.product.price,
                    )));
                  },
                  child: Container(
                    width: sizeAware.width * 0.7,
                    height: sizeAware.width * 0.1,
                    decoration: BoxDecoration(
                      color: Config.primaryColor,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(15),
                        topRight: Radius.circular(15),
                      ),
                    ),
                    child: Center(
                      child: Text(
                        getTranslated(context, 'Add To Cart'),
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
