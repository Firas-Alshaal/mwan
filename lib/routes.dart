import 'package:flutter/material.dart';
import 'package:flutter_dtic/blocs/cart/cart_event.dart';
import 'package:flutter_dtic/screens/AboutUsPage.dart';
import 'package:flutter_dtic/screens/BarcodeSearch.dart';
import 'package:flutter_dtic/screens/BlogDetails.dart';
import 'package:flutter_dtic/screens/BlogsPage.dart';
import 'package:flutter_dtic/screens/CartPage.dart';
import 'package:flutter_dtic/screens/ConfirmOrderPage.dart';
import 'package:flutter_dtic/screens/ContactUs.dart';
import 'package:flutter_dtic/screens/ContactUsPage.dart';
import 'package:flutter_dtic/screens/DeliveryPage.dart';
import 'package:flutter_dtic/screens/EventDetails.dart';
import 'package:flutter_dtic/screens/EventsPage.dart';
import 'package:flutter_dtic/screens/HomePage.dart';
import 'package:flutter_dtic/screens/JobsPage.dart';
import 'package:flutter_dtic/screens/OrderSearchPage.dart';
import 'package:flutter_dtic/screens/ProductDetails.dart';
import 'package:flutter_dtic/screens/ProductsPage.dart';
import 'package:flutter_dtic/screens/ProfilePage.dart';
import 'package:flutter_dtic/screens/SearchPage.dart';
import 'package:flutter_dtic/screens/SingleBrandPage.dart';
import 'package:flutter_dtic/screens/SingleJobPage.dart';
import 'package:flutter_dtic/screens/auth/ForgotPasswordPage.dart';
import 'package:flutter_dtic/screens/auth/LoginPage.dart';
import 'package:flutter_dtic/screens/auth/RegisterPage.dart';
import 'package:flutter_dtic/screens/auth/VerificationPage.dart';
import 'package:flutter_dtic/screens/start_video.dart';

class Routes {
  static Map<String, WidgetBuilder> getAll() {
    return {
      "/home": (context) => HomePage(),
      "/products": (context) => ProductsPage(),
      "/product details": (context) => ProductDetailes(),
      "/cart": (context) => CartPage(),
      "/delivery": (context) => DeliveryPage(),
      "/blogs": (context) => BlogsPage(),
      "/events": (context) => EventsPage(),
      "/blog details": (context) => BlogDetailes(),
      "/event details": (context) => EventDetailes(),
      "/profile": (context) => ProfilePage(),
      "/login": (context) => LoginPage(),
      "/register": (context) => RegisterPage(),
      "/jobs": (context) => JobsPage(),
      "/single job": (context) => SingleJobPage(),
      "/about us": (context) => AboutUsPage(),
      "/forgot password": (context) => ForgotPasswordPage(),
      "/search": (context) => SearchPage(),
      "/contact us": (context) => ContactUsPage(),
      "/verification": (context) => VerificationPage(),
      "/search order": (context) => OrderSearchPage(),
      "/verify order": (context) => ConfirmOrderPage(),
      "/barcode search": (context) => BarcodeSearchPage(),
      "/single brand": (context) => SingleBrandPage(),
      "/contactus": (context) => ContactUs(),
      "/splash": (context) => AssetVideo(),
    };
  }
}
