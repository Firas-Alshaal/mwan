import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dtic/blocs/cart/cart_bloc.dart';
import 'package:flutter_dtic/blocs/cart/cart_state.dart';
import 'package:flutter_dtic/blocs/product/product_bloc.dart';
import 'package:flutter_dtic/blocs/product/product_event.dart';
import 'package:flutter_dtic/blocs/product/product_state.dart';
import 'package:flutter_dtic/localization/localization_constants.dart';
import 'package:flutter_dtic/models/Brand.dart';
import 'package:flutter_dtic/models/Category.dart';
import 'package:flutter_dtic/models/Product.dart';
import 'package:flutter_dtic/services/ProductApi.dart';
import 'package:flutter_dtic/widgets/BrandCard.dart';
import 'package:flutter_dtic/widgets/CategoryCard.dart';
import 'package:flutter_dtic/widgets/Loading.dart';
import 'package:flutter_dtic/widgets/MyAppBar.dart';
import 'package:flutter_dtic/widgets/ProductCard.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter_dtic/config.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:http/http.dart' as http;

import '../config.dart';

class ProductsPage extends StatefulWidget {
  @override
  _ProductsPageState createState() => _ProductsPageState();
}

class _ProductsPageState extends State<ProductsPage> {
  ProductApi productApi = ProductApi(httpClient: http.Client());
  ProductBloc productBloc;
  List<Product> products = [];
  List<Category> categories = [];
  List<Brand> brands = [];

  int choosen = 0;
  int choosen_sub = -1;

  @override
  void initState() {
    super.initState();
    productBloc = ProductBloc(productApi: productApi);
    productBloc.add(ProductsRequested());
  }

  @override
  Widget build(BuildContext context) {
    final sizeAware = MediaQuery.of(context).size;
    final cartBloc = BlocProvider.of<CartBloc>(context);

    return Scaffold(
      appBar: myAppBar(getTranslated(context, 'Products'), null),
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: BlocListener(
              cubit: cartBloc,
              listener: (context, state) {
                if (state is AddedToCart) {
                  Fluttertoast.showToast(
                    toastLength: Toast.LENGTH_LONG,
                    backgroundColor: HexColor('#F93E4F'),
                    textColor: Colors.white,
                    msg: getTranslated(
                      context,
                      'Item Added To Cart',
                    ),
                  );
                }
              },
              child: BlocBuilder(
                cubit: productBloc,
                buildWhen: (previous, current) {
                  if (current is ProductsLoadSuccess) {
                    return true;
                  } else
                    return false;
                },
                builder: (context, state) {
                  if (state is ProductsLoadInProgress) {
                    return Container(
                      height: 50,
                      child: Center(
                        child: CircularProgressIndicator(),
                      ),
                    );
                  }
                  if (state is ProductsLoadSuccess) {
                    categories = state.categories;
                    return Container(
                      decoration: BoxDecoration(
                        color: Colors.grey[200],
                        // borderRadius: BorderRadius.only(
                        //   bottomLeft: Radius.circular(25),
                        //   bottomRight: Radius.circular(25),
                        // ),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                          vertical: 18,
                          horizontal: 8,
                        ),
                        // child: Container(
                        //   width: sizeAware.width * 0.4,
                        //   height: sizeAware.height * 0.2,
                        //   child: ListView.builder(
                        //     scrollDirection: Axis.horizontal,
                        //     shrinkWrap: true,
                        //     itemCount: categories.length,
                        //     itemBuilder: (context, index) {
                        //       return GestureDetector(
                        //         onTap: () {
                        //           choose(index, categories[index].id);
                        //         },
                        //         child: CategoryCard(
                        //           category: categories[index],
                        //           choosen: choosen == index,
                        //         ),
                        //       );
                        //     },
                        //   ),
                        // ),
                      ),
                    );
                  }
                  if (state is ProductsLoadFailure) {
                    return Scaffold(
                      body: SafeArea(
                        child: Container(
                          width: sizeAware.width,
                          height: sizeAware.height,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                getTranslated(context, 'Connection Error'),
                                style: TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.bold),
                              ),
                              SizedBox(height: 20),
                              FlatButton(
                                color: Config.primaryColor,
                                onPressed: () {
                                  productBloc.add(ProductsRequested());
                                },
                                child: Text(
                                  getTranslated(context, 'Refresh'),
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    );
                  }
                  return Container();
                },
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: BlocBuilder(
              cubit: productBloc,
              buildWhen: (previous, current) {
                if (current is ProductsLoadSuccess) {
                  return true;
                } else
                  return false;
              },
              builder: (context, state) {
                if (state is ProductsLoadInProgress) {
                  return Container(
                    height: 50,
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  );
                }
                if (state is ProductsLoadSuccess) {
                  categories = state.categories;
                  return categories[choosen].subCategories.length == 0
                      ? Container()
                      : Container(
                          decoration: BoxDecoration(
                            color: Colors.grey[200],
                            borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(25),
                              bottomRight: Radius.circular(25),
                            ),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                              vertical: 18,
                              horizontal: 8,
                            ),
                            child: Container(
                              width: sizeAware.width,
                              height: 120,
                              child: ListView.builder(
                                scrollDirection: Axis.horizontal,
                                shrinkWrap: true,
                                itemCount:
                                    categories[choosen].subCategories.length,
                                itemBuilder: (context, index) {
                                  return GestureDetector(
                                    onTap: () {
                                      chooseSub(
                                        index,
                                        categories[choosen]
                                            .subCategories[index]
                                            .id,
                                      );
                                    },
                                    child: CategoryCard(
                                      category: categories[choosen]
                                          .subCategories[index],
                                      choosen: choosen_sub == index,
                                    ),
                                  );
                                },
                              ),
                            ),
                          ),
                        );
                }
                if (state is ProductsLoadFailure) {
                  return Scaffold(
                    body: SafeArea(
                      child: Container(
                        width: sizeAware.width,
                        height: sizeAware.height,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              getTranslated(context, 'Connection Error'),
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                            SizedBox(height: 20),
                            FlatButton(
                              color: Config.primaryColor,
                              onPressed: () {
                                productBloc.add(ProductsRequested());
                              },
                              child: Text(
                                getTranslated(context, 'Refresh'),
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  );
                }
                return Container();
              },
            ),
          ),
          SliverToBoxAdapter(
            child: BlocBuilder(
              cubit: productBloc,
              buildWhen: (previous, current) {
                if (current is ProductsLoadSuccess) {
                  return true;
                } else
                  return false;
              },
              builder: (context, state) {
                if (state is ProductsLoadInProgress) {
                  return Container(
                    height: 50,
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  );
                }
                if (state is ProductsLoadSuccess) {
                  brands = state.brands;
                  return brands.length == 0
                      ? Container()
                      : Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 18.0,
                                vertical: 8,
                              ),
                              child: Container(
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      getTranslated(context, 'Brands'),
                                      style: TextStyle(
                                        color: Config.primaryColor,
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            brands.length == 0
                                ? Center(
                                    child: Text(
                                        getTranslated(context, 'No Brands')),
                                  )
                                : Container(
                                    width: sizeAware.width,
                                    height: 180,
                                    child: ListView.builder(
                                      scrollDirection: Axis.horizontal,
                                      shrinkWrap: true,
                                      itemCount: brands.length,
                                      itemBuilder: (context, index) {
                                        return BrandCard(
                                          brand: brands[index],
                                        );
                                      },
                                    ),
                                  ),
                          ],
                        );
                }
                if (state is ProductsLoadFailure) {
                  return Scaffold(
                    body: SafeArea(
                      child: Container(
                        width: sizeAware.width,
                        height: sizeAware.height,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              getTranslated(context, 'Connection Error'),
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                            SizedBox(height: 20),
                            FlatButton(
                              color: Config.primaryColor,
                              onPressed: () {
                                productBloc.add(ProductsRequested());
                              },
                              child: Text(
                                getTranslated(context, 'Refresh'),
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  );
                }
                return Container();
              },
            ),
          ),
          SliverToBoxAdapter(
            child: BlocBuilder(
              cubit: productBloc,
              builder: (context, state) {
                if (state is ProductsLoadFailure) {
                  return Container();
                }
                if (state is ProductsLoadInProgress) {
                  return Container(
                    height: sizeAware.height / 2,
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  );
                }
                if (state is CategoryProductsLoadSuccess ||
                    state is ProductsLoadSuccess) {
                  products = state.products;
                  return Container(
                    width: sizeAware.width,
                    child: ListView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: products.length,
                      itemBuilder: (context, index) {
                        return ProductCard(
                          product: products[index],
                        );
                      },
                    ),
                  );
                }
                return Container();
              },
            ),
          ),
        ],
      ),
    );
  }

  choose(index, id) {
    setState(() {
      choosen = index;
      choosen_sub = -1;
    });
    productBloc.add(CategoryProductsRequested(id: id));
  }

  chooseSub(index, id) {
    setState(() {
      choosen_sub = index;
    });
    productBloc.add(CategoryProductsRequested(id: id));
  }
}
