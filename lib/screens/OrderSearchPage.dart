import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dtic/blocs/home/home_bloc.dart';
import 'package:flutter_dtic/blocs/home/home_event.dart';
import 'package:flutter_dtic/blocs/home/home_state.dart';
import 'package:flutter_dtic/config.dart';
import 'package:flutter_dtic/constants.dart';
import 'package:flutter_dtic/localization/localization_constants.dart';
import 'package:flutter_dtic/models/Order.dart';
import 'package:flutter_dtic/services/HomeApi.dart';
import 'package:flutter_dtic/widgets/Loading.dart';
import 'package:flutter_dtic/widgets/MyAppBar.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:http/http.dart' as http;

class OrderSearchPage extends StatefulWidget {
  @override
  _OrderSearchPageState createState() => _OrderSearchPageState();
}

class _OrderSearchPageState extends State<OrderSearchPage> {
  TextEditingController query = TextEditingController();
  HomeApi homeApi = HomeApi(httpClient: http.Client());
  HomeBloc homeBloc;
  Order order;
  @override
  void initState() {
    super.initState();
    homeBloc = HomeBloc(homeApi: homeApi);
  }

  @override
  Widget build(BuildContext context) {
    final sizeAware = MediaQuery.of(context).size;
    return BlocListener(
      cubit: homeBloc,
      listener: (context, state) {
        if (state is CheckOrderNotFound) {
          Fluttertoast.showToast(
            toastLength: Toast.LENGTH_LONG,
            msg: getTranslated(context, 'Order Not Found'),
          );
        }
      },
      child: BlocBuilder(
        cubit: homeBloc,
        builder: (context, state) {
          if (state is HomeLoadInProgress) {
            return Loading();
          }
          if (state is HomeLoadFailure) {
            return Scaffold(
              body: SafeArea(
                child: Container(
                  width: sizeAware.width,
                  height: sizeAware.height,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        getTranslated(context, 'Connection Error'),
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(height: 20),
                      FlatButton(
                        color: Config.primaryColor,
                        onPressed: () {
                          setState(() {
                            homeBloc = HomeBloc(homeApi: homeApi);
                          });
                        },
                        child: Text(
                          getTranslated(context, 'Refresh'),
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            );
          }
          if (state is CheckOrderLoadSuccess) {
            order = state.order;
            return Scaffold(
              appBar: myAppBar('Check Order', null),
              body: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(height: sizeAware.height*0.3,),
                      Text(
                        LANGUAGE == 'en'
                            ? 'Thanks for your trust'
                            : 'شكرا لثقتك',
                        style: TextStyle(
                          color: Config.primaryColor,
                          fontSize: 19,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      // Center(child: Image.asset('assets/Tracking.png')),
                      SizedBox(
                        height: 30,
                      ),
                      Text(
                        LANGUAGE == 'en'
                            ? 'Your order has been received and is in progress'
                            : 'تم استلام طلبكم و هو قيد التوصيل',
                        style: TextStyle(
                          fontSize: 19,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      Text(
                        LANGUAGE == 'en' ? 'Order Status' : 'حالة الطلب',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        '${getStatus()}',
                        style: TextStyle(),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            LANGUAGE == 'en'
                                ? 'Total Price'
                                : 'المبلغ الاجمالي',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            order.total,
                            style: TextStyle(
                              color: Colors.green,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ),
              ),
            );
          }

          if (state is HomeInitial || state is CheckOrderNotFound) {
            return Scaffold(
              appBar: myAppBar(getTranslated(context, 'Check Order'), null),
              body: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [

                      // Center(child: Image.asset('assets/Tracking.png')),
                      SizedBox(height: sizeAware.height*0.3,),
                      Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(15),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: TextFormField(
                            controller: query,
                            decoration: InputDecoration(
                              prefixIcon: Icon(Icons.search),
                              border: InputBorder.none,
                              hintText: getTranslated(context, 'order number'),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      GestureDetector(
                        onTap: () {
                          if (query.text.length == 0) {
                            Fluttertoast.showToast(
                                toastLength: Toast.LENGTH_LONG,
                                msg: getTranslated(
                                    context, 'Please Enter Order Number'));
                            return;
                          }
                          homeBloc.add(CheckOrder(id: query.text));
                        },
                        child: Container(
                          width: sizeAware.width * 0.85,
                          height: 50.0,
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                                begin: Alignment.topRight,
                                end: Alignment.bottomLeft,
                                colors: [
                                  HexColor('#F93E4F'),
                                  HexColor('#8D2CD3')
                                ]),
                            //color: Config.primaryColor,
                            borderRadius: BorderRadius.horizontal(
                              left: Radius.circular(40),
                              right: Radius.circular(40),
                            ),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  getTranslated(context, 'Search'),
                                  style: TextStyle(
                                    fontSize: 20,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          }
          return Container();
        },
      ),
    );
  }

  String getStatus() {
    switch (order.status) {
      case 0:
        return getTranslated(context, 'Not Verified');
      case 1:
        return getTranslated(context, 'Verified');
      case 2:
        return getTranslated(context, 'Pending');
      case 3:
        return getTranslated(context, 'Delivery');
      case 4:
        return getTranslated(context, 'Done');
      case 5:
        return getTranslated(context, 'Rejected');
      default:
        return '';
    }
  }
}
