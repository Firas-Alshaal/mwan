import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dtic/blocs/auth/auth_bloc.dart';
import 'package:flutter_dtic/blocs/auth/auth_event.dart';
import 'package:flutter_dtic/blocs/auth/auth_state.dart';
import 'package:flutter_dtic/blocs/cart/cart_bloc.dart';
import 'package:flutter_dtic/blocs/home/home_bloc.dart';
import 'package:flutter_dtic/blocs/home/home_event.dart';
import 'package:flutter_dtic/blocs/home/home_state.dart';
import 'package:flutter_dtic/config.dart';
import 'package:flutter_dtic/localization/localization_constants.dart';
import 'package:flutter_dtic/models/Brand.dart';
import 'package:flutter_dtic/models/Product.dart';
import 'package:flutter_dtic/models/event.dart';
import 'package:flutter_dtic/services/HomeApi.dart';
import 'package:flutter_dtic/widgets/BrandCard.dart';
import 'package:flutter_dtic/widgets/EventCard.dart';
import 'package:flutter_dtic/widgets/FeaturedProductCard.dart';
import 'package:flutter_dtic/widgets/ImageCarusel.dart';
import 'package:flutter_dtic/widgets/Loading.dart';
import 'package:flutter_dtic/widgets/MyAppBar.dart';
import 'package:flutter_dtic/widgets/MyBottomNavigation.dart';
import 'package:flutter_dtic/widgets/MyDrawer.dart';
import 'package:flutter_dtic/widgets/MyFloatingActionButton.dart';
import 'package:flutter_dtic/widgets/SkeletonListH.dart';
import 'package:flutter_dtic/widgets/SkeletonListV.dart';

import 'package:flutter_dtic/widgets/Slider.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:http/http.dart' as http;
import 'package:skeleton_text/skeleton_text.dart';
import '../constants.dart';

import '../main.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  ScrollController _scrollController = new ScrollController();
  int page = 1;
  bool isLoading = false;
  HomeApi homeApi = HomeApi(httpClient: http.Client());
  HomeBloc homeBloc;
  List<Product> products = [];
  List<Event> events = [];
  List<String> images = [];
  AuthBloc authBloc;
  bool isMoreP = true;
  @override
  void initState() {
    super.initState();
    homeBloc = HomeBloc(homeApi: homeApi);
    authBloc = BlocProvider.of<AuthBloc>(context);
    authBloc.add(AuthRequested());
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final sizeAware = MediaQuery.of(context).size;
    final cartBloc = BlocProvider.of<CartBloc>(context);

    return BlocListener(
      cubit: authBloc,
      listener: (context, state) {
        if (state is Authenticated || state is NotAuthenticated) {
          homeBloc.add(HomeCaruselRequested());
          homeBloc.add(HomeEventsRequested());
          homeBloc.add(HomeProductsRequested(page: page));
        }
      },
      child: Scaffold(
        appBar: myAppBar(getTranslated(context, 'Home'), null),
        drawer: MyDrawer(),
        bottomNavigationBar: MyBottomNavigation(
          index: 0,
        ),
        floatingActionButton: MyFloatingActionButton(),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        body: RefreshIndicator(
          onRefresh: () {
            homeBloc.add(RefreshHomeRequested());
            return Future.delayed(Duration(seconds: 1));
          },
          child: ListView(
            children: [
              //slider
              BlocBuilder(
                cubit: homeBloc,
                buildWhen: (previous, current) {
                  if (current is HomeCaruselLoadInProgress ||
                      current is HomeInitial ||
                      current is HomeCaruselLoadSuccess ||
                      current is HomeLoadSuccess) {
                    return true;
                  } else
                    return false;
                },
                builder: (context, state) {
                  if (state is HomeCaruselLoadInProgress ||
                      state is HomeInitial) {
                    return Container(
                      width: sizeAware.width,
                      height: 170,
                      // padding: EdgeInsets.all(8),
                      child: SkeletonAnimation(
                        shimmerColor: Colors.grey[300],

                        // borderRadius:
                        //     BorderRadius.circular(20),
                        shimmerDuration: 1000,
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.grey[300],
                            // borderRadius: BorderRadius.circular(20),
                            // boxShadow: shadowList,
                          ),
                          // margin:
                          //     EdgeInsets.only(top: 40),
                        ),
                      ),
                    );
                  }
                  if (state is HomeCaruselLoadSuccess) {
                    images = state.images;
                  }
                  if (state is HomeLoadSuccess) {
                    images = state.images;
                  }
                  return Container(
                    height: 170,
                    child: ImageSlider(
                      images: images,
                    ),
                  );
                },
              ),
              SizedBox(
                height: 15,
              ),
              // latest events
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 18.0,
                  vertical: 8,
                ),
                child: Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        getTranslated(context, 'Latest Events'),
                        style: TextStyle(
                          color: Config.primaryColor,
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.pushNamed(context, '/events');
                        },
                        child: Text(
                          getTranslated(context, 'See all'),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              BlocBuilder(
                cubit: homeBloc,
                buildWhen: (previous, current) {
                  if (current is HomeEventsLoadInProgress ||
                      current is HomeInitial ||
                      current is HomeEventsLoadSuccess ||
                      current is HomeLoadSuccess) {
                    return true;
                  } else
                    return false;
                },
                builder: (context, state) {
                  if (state is HomeEventsLoadInProgress ||
                      state is HomeInitial) {
                    return Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SkeletonList(),
                      ],
                    );
                  }
                  if (state is HomeEventsLoadSuccess) {
                    events = state.events;
                  }
                  if (state is HomeLoadSuccess) {
                    events = state.events;
                  }
                  return SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: events.length == 0
                        ? Text(getTranslated(context, 'No Events'))
                        : Container(
                            width: sizeAware.width,
                            height: 160,
                            child: ListView.builder(
                              scrollDirection: Axis.horizontal,
                              shrinkWrap: true,
                              itemCount: events.length,
                              itemBuilder: (context, index) {
                                return EventCard(
                                  event: events[index],
                                );
                              },
                            ),
                          ),
                  );
                },
              ),
              SizedBox(
                height: 15,
              ),
              // products
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 18.0,
                  vertical: 8,
                ),
                child: Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        getTranslated(context, 'Products'),
                        style: TextStyle(
                          color: Config.primaryColor,
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              BlocBuilder(
                cubit: homeBloc,
                buildWhen: (previous, current) {
                  if (current is HomeProductsLoadInProgress ||
                      current is HomeInitial ||
                      current is HomeProductsLoadSuccess ||
                      current is HomeGetMoreProductsLoadSuccess ||
                      current is HomeLoadSuccess) {
                    return true;
                  } else
                    return false;
                },
                builder: (context, state) {
                  if (state is HomeProductsLoadInProgress ||
                      state is HomeInitial) {
                    return Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SkeletonListV(),
                      ],
                    );
                  }
                  if (state is HomeLoadSuccess) {
                    products = state.products;
                    page = 1;
                    isMoreP = true;
                  }
                  if (state is HomeProductsLoadSuccess) {
                    products = (state.products);
                    isMoreP = state.isMore;
                  }
                  // if (state is HomeGetMoreProductsLoadSuccess) {
                  //   if (state.products.length != 0) {
                  //     products.addAll(state.products);
                  //     page++;
                  //   } else {
                  //     isMoreP = state.isMore;
                  //   }
                  // }
                  return Column(
                    children: [
                      GridView.count(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        crossAxisCount: 2,
                        crossAxisSpacing: 3,
                        childAspectRatio: 0.9,
                        children: getProducts(),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      isMoreP
                          ? Center(
                              child: InkWell(
                                onTap: () {
                                  setState(() {
                                    page++;
                                  });
                                  homeBloc.add(
                                      HomeGetMoreProductsRequested(page: page));
                                },
                                child: Text(
                                  getTranslated(context, 'seeMore'),
                                  style: TextStyle(fontSize: 16),
                                ),
                              ),
                            )
                          : Container(),
                      SizedBox(
                        height: 40,
                      ),
                    ],
                  );
                },
              ),
              // brands
            ],
          ),
        ),
      ),
    );
  }

//
  List<FeaturedProductCard> getProducts() {
    List<FeaturedProductCard> list = [];
    for (int i = 0; i < products.length; i++) {
      list.add(
        FeaturedProductCard(
          product: products[i],
        ),
      );
    }
    return list;
  }
}
//  if (state is HomeLoadFailure) {
//             return Scaffold(
//               body: SafeArea(
//                 child: Container(
//                   width: sizeAware.width,
//                   height: sizeAware.height,
//                   child: Column(
//                     mainAxisAlignment: MainAxisAlignment.center,
//                     crossAxisAlignment: CrossAxisAlignment.center,
//                     children: [
//                       Text(
//                         getTranslated(context, 'Connection Error'),
//                         style: TextStyle(
//                             fontSize: 20, fontWeight: FontWeight.bold),
//                       ),
//                       SizedBox(height: 20),
//                       FlatButton(
//                         color: Config.primaryColor,
//                         onPressed: () {
//                           setState(() {
//                             authBloc.add(AuthRequested());
//                             homeBloc.add(HomeRequested());
//                           });
//                         },
//                         child: Text(
//                           getTranslated(context, 'Refresh'),
//                           style: TextStyle(
//                             color: Colors.white,
//                           ),
//                         ),
//                       )
//                     ],
//                   ),
//                 ),
//               ),
//             );
//           }
