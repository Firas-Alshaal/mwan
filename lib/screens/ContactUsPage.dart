import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dtic/blocs/home/home_bloc.dart';
import 'package:flutter_dtic/blocs/home/home_event.dart';
import 'package:flutter_dtic/blocs/home/home_state.dart';
import 'package:flutter_dtic/config.dart';
import 'package:flutter_dtic/localization/localization_constants.dart';
import 'package:flutter_dtic/models/Branche.dart';
import 'package:flutter_dtic/services/HomeApi.dart';
import 'package:flutter_dtic/widgets/BrancheNameCard.dart';
import 'package:flutter_dtic/widgets/Loading.dart';
import 'package:flutter_dtic/widgets/MyAppBar.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';

class ContactUsPage extends StatefulWidget {
  @override
  _ContactUsPageState createState() => _ContactUsPageState();
}

class _ContactUsPageState extends State<ContactUsPage> {
  HomeApi homeApi = HomeApi(httpClient: http.Client());
  HomeBloc homeBloc;
  List<Branche> branches = [];
  int choosen = 0;

  Set<Marker> markers = Set<Marker>();
  GoogleMapController _mapController;

  @override
  void initState() {
    super.initState();
    homeBloc = HomeBloc(homeApi: homeApi);
    homeBloc.add(ContactUsRequested());
  }

  newCameraPosition(point) async {
    await _mapController.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: LatLng(
            point['lan'],
            point['lon'],
          ),
          zoom: 15,
        ),
      ),
    );
  }

  map(lat, lang) async {
    String googleUrl =
        'https://www.google.com/maps/search/?api=1&query=$lat,$lang';
    if (await canLaunch(googleUrl)) {
      await launch(googleUrl);
    } else {
      throw 'Could not launch $googleUrl';
    }
  }

  @override
  Widget build(BuildContext context) {
    final sizeAware = MediaQuery.of(context).size;

    return Scaffold(
      appBar: myAppBar(getTranslated(context, 'Contact Us'), null),
      body: BlocBuilder(
        cubit: homeBloc,
        builder: (context, state) {
          if (state is HomeLoadInProgress) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          if (state is HomeLoadFailure) {
            return Scaffold(
              body: SafeArea(
                child: Container(
                  width: sizeAware.width,
                  height: sizeAware.height,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        getTranslated(context, 'Connection Error'),
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(height: 20),
                      FlatButton(
                        color: Config.primaryColor,
                        onPressed: () {
                          setState(() {
                            homeBloc.add(ContactUsRequested());
                          });
                        },
                        child: Text(
                          getTranslated(context, 'Refresh'),
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            );
          }
          if (state is ContactUsLoadSuccess) {
            branches = state.branches;
            var mark = jsonDecode(branches[choosen].coordinates);
            markers.clear();
            markers.add(
              Marker(
                markerId: MarkerId('${branches[choosen].coordinates}'),
                position: LatLng(double.parse(mark['lan'].toString()), double.parse(mark['lon'].toString())),
              ),
            );
            return CustomScrollView(
              slivers: [
                SliverToBoxAdapter(
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.grey[200],
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(25),
                          bottomRight: Radius.circular(25),
                        ),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                          vertical: 18,
                          horizontal: 8,
                        ),
                        child: Container(
                          width: sizeAware.width,
                          height: 50,
                          child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            shrinkWrap: true,
                            itemCount: branches.length,
                            itemBuilder: (context, index) {
                              return GestureDetector(
                                onTap: () {
                                  setState(() {
                                    choosen = index;
                                    mark =
                                        jsonDecode(branches[choosen].coordinates);
                                  });

                                  newCameraPosition(mark);
                                },
                                child: BrancheNameCard(
                                  branche: branches[index],
                                  choosen: choosen == index,
                                ),
                              );
                            },
                          ),
                        ),
                      ),
                    )),
                SliverToBoxAdapter(
                  child: SizedBox(
                    height: 20,
                  ),
                ),
                SliverToBoxAdapter(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 18,
                      vertical: 12,
                    ),
                    child: Container(
                      width: sizeAware.width,
                      padding: EdgeInsets.all(16),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(35),
                          boxShadow: [
                            BoxShadow(
                              blurRadius: 5,
                              color: Colors.grey[300],
                              offset: Offset(1, 2),
                              spreadRadius: 5,
                            )
                          ]),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Icon(
                                Icons.mail,
                                size: 30,
                              ),
                              Icon(
                                Icons.phone_android,
                                size: 30,
                              ),
                              GestureDetector(
                                onTap: () {
                                  map(mark['lan'], mark['lon']);
                                },
                                child: Icon(
                                  Icons.pin_drop,
                                  size: 30,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          getSettings(),
                          SizedBox(
                            height: 20,
                          ),
                          //map here
                          Container(
                            width: sizeAware.width,
                            height: 250,
                            child: GoogleMap(
                              initialCameraPosition: CameraPosition(
                                target: LatLng(mark['lan'], mark['lon']),
                                zoom: 12,
                              ),
                              gestureRecognizers: Set()
                                ..add(Factory<PanGestureRecognizer>(
                                        () => PanGestureRecognizer()))
                                ..add(
                                  Factory<VerticalDragGestureRecognizer>(
                                          () => VerticalDragGestureRecognizer()),
                                )
                                ..add(
                                  Factory<HorizontalDragGestureRecognizer>(
                                          () => HorizontalDragGestureRecognizer()),
                                )
                                ..add(
                                  Factory<ScaleGestureRecognizer>(
                                          () => ScaleGestureRecognizer()),
                                ),
                              circles: Set<Circle>()
                                ..add(Circle(
                                  circleId: CircleId('hi2'),
                                  center: LatLng(47.6, 8.8796),
                                  radius: 50,
                                  strokeWidth: 10,
                                  strokeColor: Colors.black,
                                )),
                              markers: markers,
                              onMapCreated: (mapController) {
                                _mapController = mapController;
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            );
          }
          return Container();
        },
      ),
    );
  }

  getSettings() {
    List<Widget> list = [];
    list = branches[choosen]
        .settings
        .map(
          (e) => Padding(
        padding: const EdgeInsets.all(4.0),
        child: Row(
          children: [
            Image.network(
              e.icon,
              width: 30,
              height: 30,
            ),
            Text(
              ' ${e.key}:  ',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Text(
              '${e.value}',
              style: TextStyle(color: Config.primaryColor),
              textDirection: TextDirection.ltr,
            )
          ],
        ),
      ),
    )
        .toList();

    return Column(
      children: list,
    );
  }
}
