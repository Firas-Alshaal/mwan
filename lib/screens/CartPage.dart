import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dtic/blocs/auth/auth_bloc.dart';
import 'package:flutter_dtic/blocs/auth/auth_event.dart';
import 'package:flutter_dtic/blocs/cart/cart_bloc.dart';
import 'package:flutter_dtic/blocs/cart/cart_event.dart';
import 'package:flutter_dtic/blocs/cart/cart_state.dart';
import 'package:flutter_dtic/config.dart';
import 'package:flutter_dtic/constants.dart';
import 'package:flutter_dtic/localization/localization_constants.dart';
import 'package:flutter_dtic/services/CartApi.dart';
import 'package:flutter_dtic/widgets/CartItem.dart';
import 'package:flutter_dtic/widgets/Loading.dart';
import 'package:flutter_dtic/widgets/MyAppBar.dart';
import 'package:flutter_dtic/widgets/MyBottomNavigation.dart';
import 'package:flutter_dtic/widgets/MyFloatingActionButton.dart';
import 'package:flutter_dtic/widgets/StepsWidget.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:hexcolor/hexcolor.dart';

class CartPage extends StatefulWidget {
  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  List products = [];
  CartApi cartApi = CartApi(httpClient: http.Client());
  CartBloc cartBloc;
  TextEditingController coupon = TextEditingController();
  bool is_valid = false;
  String discount = "";
  String limit;
  double tot_price = 0;

  @override
  void initState() {
    super.initState();
    cartBloc = BlocProvider.of<CartBloc>(context);
    cartBloc.add(CartPageRequested());
  }

  @override
  Widget build(BuildContext context) {
    final sizeAware = MediaQuery.of(context).size;

    return Scaffold(
      appBar: myAppBar(getTranslated(context, 'Cart'), null),
      body: BlocListener(
        cubit: cartBloc,
        listener: (context, state) {
          if (state is CodeInvalid) {
            Fluttertoast.showToast(toastLength: Toast.LENGTH_LONG,
                msg: getTranslated(context, 'couponsuccessdesc'));
          }
          if(state is CodeValid)
            {
              Fluttertoast.showToast(toastLength: Toast.LENGTH_LONG,
                  msg: getTranslated(context, 'coponSuccess'));
            }
        },
        child: BlocBuilder(
          cubit: cartBloc,
          builder: (context, state) {
            if (state is CartLoadInProgress) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
            if (state is CartLoadFailure) {
              return Scaffold(
                body: SafeArea(
                  child: Container(
                    width: sizeAware.width,
                    height: sizeAware.height,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          getTranslated(context, 'Connection Error'),
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold),
                        ),
                        SizedBox(height: 20),
                        FlatButton(
                          color: Config.primaryColor,
                          onPressed: () {
                            setState(() {
                              cartBloc.add(CartPageRequested());
                            });
                          },
                          child: Text(
                            getTranslated(context, 'Refresh'),
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              );
            }
            if (state is CartLoadSuccess) {
              products = state.items;
              limit = state.limit != null ? state.limit : limit;
            }
            if (state is OrderSuccess) {
              products = [];
            }
            tot_price = 0;
            for (int i = 0; i < products.length; i++) {
              tot_price += products[i].total_price;
            }

            if (state is CodeValid) {
              is_valid = true;
              discount = state.str;
            }
            return SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: sizeAware.height * 0.03,
                  ),
                  StepsWidget(
                    step: 1,
                  ),
                  SizedBox(
                    height: sizeAware.height * 0.03,
                  ),
                  getItems(),
                  SizedBox(height: 30),
                  Center(
                    child: Container(
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              getTranslated(context, 'Discount card'),
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            SizedBox(height: 5),
                            Text(
                              getTranslated(context, 'Enter your code'),
                              style: TextStyle(
                                fontSize: 16,
                              ),
                            ),
                            SizedBox(height: 20),
                            Container(
                              color: Colors.white,
                              width: sizeAware.width * 0.8,
                              height: 50,
                              child: Center(
                                child: TextFormField(
                                  controller: coupon,
                                  enabled: true,
                                  decoration: InputDecoration(
                                    hintText:
                                        '${getTranslated(context, 'Code')}',
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(25),
                                    ),
                                    suffixIcon: GestureDetector(
                                      onTap: () {
                                        if (ISAuth) {
                                          if (coupon.text != '') {
                                            cartBloc.add(
                                              CheckCoupon(coupon: coupon.text),
                                            );
                                          }
                                        } else {
                                          Fluttertoast.showToast(toastLength: Toast.LENGTH_LONG,
                                              msg: getTranslated(context,
                                                  'You have to login'));
                                        }
                                      },
                                      child: Container(
                                        width: 70,
                                        height: 50,
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(25),
                                          //color: Config.primaryColor,
                                            gradient: LinearGradient(
                                                begin: Alignment.topRight,
                                                end: Alignment.bottomLeft,
                                                colors: [HexColor('#F93E4F'), HexColor('#8D2CD3')])
                                        ),
                                        child: Center(
                                          child: is_valid
                                              ? Icon(
                                                  Icons.check,
                                                  color: Colors.white,
                                                )
                                              : Text(
                                                  getTranslated(
                                                      context, 'Confirm'),
                                                  style: TextStyle(
                                                      color: Colors.white),
                                                ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 30),
                  Center(
                    child: GestureDetector(
                      onTap: () {
                        if (ISAuth) {
                          if (products.length != 0 &&
                              double.parse(limit) <= tot_price) {
                            Navigator.pushNamed(context, '/delivery',
                                arguments: {
                                  'coupon': coupon.text,
                                  'discount': discount,
                                });
                          } else {
                            Fluttertoast.showToast(
                                msg: LANGUAGE == 'en'
                                    ? 'minimum checkout limit is $limit'
                                    : 'اقل سعر للشراء هو $limit',
                                toastLength: Toast.LENGTH_LONG);
                          }
                        } else {
                          Navigator.pushNamed(context, '/login');
                        }
                      },
                      child: Container(
                        width: sizeAware.width * 0.8,
                        height: 60,
                        decoration: BoxDecoration(
                          //color: Config.primaryColor,
                          borderRadius: BorderRadius.circular(30),
                            gradient: LinearGradient(
                                begin: Alignment.topRight,
                                end: Alignment.bottomLeft,
                                colors: [HexColor('#F93E4F'), HexColor('#8D2CD3')])
                        ),
                        child: Center(
                          child: Text(
                            getTranslated(context, 'Buy'),
                            style: TextStyle(color: Colors.white, fontSize: 18),
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 30),
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  Widget getItems() {
    List<CartItemCard> list = [];

    for (int i = 0; i < products.length; i++) {
      list.add(CartItemCard(
        cartItem: products[i],
      ));
    }
    return Column(
      children: list,
    );
  }
}
