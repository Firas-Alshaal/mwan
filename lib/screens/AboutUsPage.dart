import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dtic/blocs/home/home_bloc.dart';
import 'package:flutter_dtic/blocs/home/home_event.dart';
import 'package:flutter_dtic/blocs/home/home_state.dart';
import 'package:flutter_dtic/config.dart';
import 'package:flutter_dtic/localization/localization_constants.dart';
import 'package:flutter_dtic/services/HomeApi.dart';
import 'package:flutter_dtic/widgets/Loading.dart';
import 'package:flutter_dtic/widgets/MyAppBar.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:http/http.dart' as http;
import 'package:hexcolor/hexcolor.dart';

class AboutUsPage extends StatefulWidget {
  @override
  _AboutUsPageState createState() => _AboutUsPageState();
}

class _AboutUsPageState extends State<AboutUsPage> {
  String text;
  HomeBloc homeBloc;
  HomeApi homeApi = HomeApi(httpClient: http.Client());
  @override
  void initState() {
    super.initState();
    homeBloc = HomeBloc(homeApi: homeApi);
    homeBloc.add(AboutUsRequested());
  }

  @override
  Widget build(BuildContext context) {
    final sizeAware = MediaQuery.of(context).size;
    return Scaffold(
      appBar: myAppBar('', null),
      body: BlocBuilder(
        cubit: homeBloc,
        builder: (context, state) {
          if (state is HomeLoadInProgress) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          if (state is HomeLoadFailure) {
            return Scaffold(
              body: SafeArea(
                child: Container(
                  width: sizeAware.width,
                  height: sizeAware.height,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        getTranslated(context, 'Connection Error'),
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(height: 20),
                      FlatButton(
                        color: Config.primaryColor,
                        onPressed: () {
                          setState(() {
                            homeBloc.add(AboutUsRequested());
                          });
                        },
                        child: Text(
                          getTranslated(context, 'Refresh'),
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            );
          }
          if (state is AboutUsLoadSuccess) {
            text = state.text;
            return SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        // Image.asset(
                        //   'assets/about-us.jpg',
                        // ),
                        // Container(
                        //   width: sizeAware.width,
                        //   height: 50,
                        //   decoration: ecoration(
                        //     //color: Config.primaryColor,
                        //       gradient: LinearGradient(
                        //           begin: Alignment.topLeft,
                        //           end: Alignment.bottomRight,
                        //           colors: <Color>[ HexColor('#8D2CD3'),HexColor('#F93E4F'),]),
                        //     borderRadius: BorderRadius.only(
                        //       bottomLeft: Radius.circular(25),
                        //       bottomRight: Radius.circular(25),
                        //     ),
                        //   ),
                        //   child: Center(
                        //     child: Text(
                        //       getTranslated(context, 'About Us'),
                        //       style: TextStyle(
                        //         color: Colors.white,
                        //         fontSize: 18,
                        //       ),
                        //     ),
                        //   ),
                        // )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 12.0),
                    child: Container(
                      width: sizeAware.width,
                      child: Html(data: text),
                    ),
                  ),
                ],
              ),
            );
          }
          return Container();
        },
      ),
    );
  }
}
