import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dtic/blocs/job/job_bloc.dart';
import 'package:flutter_dtic/blocs/job/job_event.dart';
import 'package:flutter_dtic/blocs/job/job_state.dart';
import 'package:flutter_dtic/config.dart';
import 'package:flutter_dtic/localization/localization_constants.dart';
import 'package:flutter_dtic/models/Job.dart';
import 'package:flutter_dtic/services/JopApi.dart';
import 'package:flutter_dtic/widgets/JobCard.dart';
import 'package:flutter_dtic/widgets/Loading.dart';
import 'package:flutter_dtic/widgets/MyAppBar.dart';
import 'package:http/http.dart' as http;

class JobsPage extends StatefulWidget {
  @override
  _JobsPageState createState() => _JobsPageState();
}

class _JobsPageState extends State<JobsPage> {
  List<Job> jobs = [];
  JobBloc jobBloc;
  JobApi jobApi = JobApi(httpClient: http.Client());
  @override
  void initState() {
    super.initState();
    jobBloc = JobBloc(jobApi: jobApi);
    jobBloc.add(JobsRequested());
  }

  @override
  Widget build(BuildContext context) {
    final sizeAware = MediaQuery.of(context).size;

    return Scaffold(
      appBar: myAppBar(getTranslated(context, 'Avilable Jobs'), null),
      body: BlocBuilder(
        cubit: jobBloc,
        builder: (context, state) {
          if (state is JobLoadInProgress) {
            return Center(child: CircularProgressIndicator(),);
          }
          if (state is JobLoadFailure) {
            return Scaffold(
              body: SafeArea(
                child: Container(
                  width: sizeAware.width,
                  height: sizeAware.height,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        getTranslated(context, 'Connection Error'),
                        style:
                            TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(height: 20),
                      FlatButton(
                        color: Config.primaryColor,
                        onPressed: () {
                          setState(() {
                            jobBloc.add(JobsRequested());
                          });
                        },
                        child: Text(
                          getTranslated(context, 'Refresh'),
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            );
          }
          if (state is JobsLoadSuccess) {
            jobs = state.jobs;
            return  SingleChildScrollView(
                child: Column(
                  children: [
                    SizedBox(
                      height: sizeAware.height * 0.1,
                    ),
                    jobs.length == 0
                        ? Center(
                            child: Text('No jobs available right now'),
                          )
                        : getJobs(),
                  ],
                ),
              );

          }
          return Container();
        },
      ),
    );
  }

  getJobs() {
    return Column(
      children: jobs
          .map((e) => JobCard(
                job: e,
              ))
          .toList(),
    );
  }
}
