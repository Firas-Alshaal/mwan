import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dtic/blocs/home/home_bloc.dart';
import 'package:flutter_dtic/blocs/home/home_event.dart';
import 'package:flutter_dtic/blocs/home/home_state.dart';
import 'package:flutter_dtic/localization/localization_constants.dart';
import 'package:flutter_dtic/models/Blog.dart';
import 'package:flutter_dtic/services/HomeApi.dart';
import 'package:flutter_dtic/widgets/BlogCard.dart';
import 'package:flutter_dtic/widgets/Loading.dart';
import 'package:flutter_dtic/widgets/MyAppBar.dart';
import 'package:flutter_dtic/widgets/MyBottomNavigation.dart';
import 'package:flutter_dtic/widgets/MyFloatingActionButton.dart';
import 'package:http/http.dart' as http;

import '../config.dart';

class BlogsPage extends StatefulWidget {
  @override
  _BlogsPageState createState() => _BlogsPageState();
}

class _BlogsPageState extends State<BlogsPage> {
  List<Blog> blogs = [];
  HomeApi homeApi = HomeApi(httpClient: http.Client());
  HomeBloc homeBloc;

  @override
  void initState() {
    super.initState();
    homeBloc = HomeBloc(homeApi: homeApi);
    homeBloc.add(BlogsRequested());
  }

  @override
  Widget build(BuildContext context) {
    final sizeAware = MediaQuery.of(context).size;

    return Scaffold(
      appBar: myAppBar(getTranslated(context, 'Blogs'), null),
      bottomNavigationBar: MyBottomNavigation(),
      floatingActionButton: MyFloatingActionButton(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      body: BlocBuilder(
        cubit: homeBloc,
        builder: (context, state) {
          if (state is HomeLoadInProgress) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          if (state is HomeLoadFailure) {
            return Scaffold(
              body: SafeArea(
                child: Container(
                  width: sizeAware.width,
                  height: sizeAware.height,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        getTranslated(context, 'Connection Error'),
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(height: 20),
                      FlatButton(
                        color: Config.primaryColor,
                        onPressed: () {
                          setState(() {
                            homeBloc.add(BlogsRequested());
                          });
                        },
                        child: Text(
                          getTranslated(context, 'Refresh'),
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            );
          }
          if (state is BlogsLoadSuccess) {
            blogs = state.blogs;
            return GridView.count(
              crossAxisCount: 2,
              crossAxisSpacing: 3,
              childAspectRatio: 0.8,
              children: blogs
                  .map(
                    (e) => BLogCard(
                      blog: e,
                    ),
                  )
                  .toList(),
            );
          }
          return Container();
        },
      ),
    );
  }
}
