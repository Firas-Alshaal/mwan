import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dtic/blocs/product/product_bloc.dart';
import 'package:flutter_dtic/blocs/product/product_event.dart';
import 'package:flutter_dtic/blocs/product/product_state.dart';
import 'package:flutter_dtic/localization/localization_constants.dart';
import 'package:flutter_dtic/models/Brand.dart';
import 'package:flutter_dtic/models/Product.dart';
import 'package:flutter_dtic/services/ProductApi.dart';
import 'package:flutter_dtic/widgets/MyAppBar.dart';
import 'package:flutter_dtic/widgets/ProductCard.dart';
import 'package:http/http.dart' as http;

import '../config.dart';

class SingleBrandPage extends StatefulWidget {
  @override
  _SingleBrandPageState createState() => _SingleBrandPageState();
}

class _SingleBrandPageState extends State<SingleBrandPage> {
  List<Product> products = [];
  ProductApi productApi = ProductApi(httpClient: http.Client());
  ProductBloc productBloc;
  Brand brand;
  TextEditingController query = TextEditingController(text: "");

  @override
  void initState() {
    super.initState();
    productBloc = ProductBloc(productApi: productApi);
  }

  @override
  Widget build(BuildContext context) {
    final sizeAware = MediaQuery.of(context).size;
    Map args = ModalRoute.of(context).settings.arguments;
    brand = args['brand'];

    return Scaffold(
      appBar: myAppBar(brand.name, null),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(15),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: query,
                    decoration: InputDecoration(
                      prefixIcon: Icon(Icons.search),
                      border: InputBorder.none,
                      hintText: getTranslated(context, 'Search'),
                    ),
                    onChanged: (val) {
                      productBloc.add(
                        SearchByBrandRequested(
                          query: query.text,
                          brand: brand.name,
                        ),
                      );
                    },
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              BlocBuilder(
                cubit: productBloc,
                builder: (context, state) {
                  if (state is ProductsInitial) {
                    productBloc.add(
                        SearchByBrandRequested(brand: brand.name, query: null));
                  }
                  if (state is ProductsLoadInProgress) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                  if (state is ProductsLoadFailure) {
                    return Container(
                      width: sizeAware.width,
                      height: sizeAware.height,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            getTranslated(context, 'Connection Error'),
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                          ),
                          SizedBox(height: 20),
                          FlatButton(
                            color: Config.primaryColor,
                            onPressed: () {
                              setState(() {
                                productBloc.add(SearchByBrandRequested());
                              });
                            },
                            child: Text(
                              getTranslated(context, 'Refresh'),
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            ),
                          )
                        ],
                      ),
                    );
                  }
                  if (state is SearchResult) {
                    products = state.products;
                    return Container(
                      width: sizeAware.width,
                      child: ListView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: products.length,
                        itemBuilder: (context, index) {
                          return ProductCard(
                            product: products[index],
                          );
                        },
                      ),
                    );
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
