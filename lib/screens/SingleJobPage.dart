import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dtic/blocs/job/job_bloc.dart';
import 'package:flutter_dtic/blocs/job/job_event.dart';
import 'package:flutter_dtic/blocs/job/job_state.dart';
import 'package:flutter_dtic/config.dart';
import 'package:flutter_dtic/localization/localization_constants.dart';
import 'package:flutter_dtic/models/Job.dart';
import 'package:flutter_dtic/services/JopApi.dart';
import 'package:flutter_dtic/widgets/JobCard.dart';
import 'package:flutter_dtic/widgets/JobCardDetail.dart';
import 'package:flutter_dtic/widgets/Loading.dart';
import 'package:flutter_dtic/widgets/MyAppBar.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:hexcolor/hexcolor.dart';

class SingleJobPage extends StatefulWidget {
  @override
  _SingleJobPageState createState() => _SingleJobPageState();
}

class _SingleJobPageState extends State<SingleJobPage> {
  TextEditingController firstName = TextEditingController();
  TextEditingController lastName = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController details = TextEditingController();
  File file;
  Job job;
  JobApi jobApi = JobApi(httpClient: http.Client());
  JobBloc jobBloc;

  getFile() async {
    FilePickerResult result = await FilePicker.platform.pickFiles();

    if (result != null) {
      File f = File(result.files.single.path);
      setState(() {
        file = f;
      });
    } else {
      // User canceled the picker
    }
  }

  @override
  void initState() {
    super.initState();
    jobBloc = JobBloc(jobApi: jobApi);
  }

  @override
  Widget build(BuildContext context) {
    final sizeAware = MediaQuery.of(context).size;
    Map args = ModalRoute.of(context).settings.arguments;
    job = args['job'];
    return BlocListener(
      cubit: jobBloc,
      listener: (context, state) {
        if (state is JobApplied) {
          Fluttertoast.showToast(
              msg: getTranslated(context, 'Sent Successfuly'));
          Navigator.pop(context);
        }
      },
      child: Scaffold(
        appBar: myAppBar(getTranslated(context, 'Job Application'), null),
        body: BlocBuilder(
          cubit: jobBloc,
          builder: (context, state) {
            if (state is JobLoadInProgress) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
            if (state is JobLoadFailure) {
              return Scaffold(
                body: SafeArea(
                  child: Container(
                    width: sizeAware.width,
                    height: sizeAware.height,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          getTranslated(context, 'Connection Error'),
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold),
                        ),
                        SizedBox(height: 20),
                        FlatButton(
                          color: Config.primaryColor,
                          onPressed: () {
                            setState(() {
                              jobBloc = JobBloc(jobApi: jobApi);
                            });
                          },
                          child: Text(
                            getTranslated(context, 'Refresh'),
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              );
            }

            if (state is JobInitial ||
                state is JobApplied ||
                state is JobNotApplied) {
              return SingleChildScrollView(
                child: Column(
                  children: [
                    SizedBox(
                      height: 25,
                    ),
                    JobCardDetail(
                      job: job,
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: [
                          Container(
                            child: Row(
                              children: [
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                      horizontal: 4,
                                    ),
                                    child: Container(
                                      clipBehavior: Clip.antiAlias,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(25),
                                        border:
                                            Border.all(color: Colors.grey[400]),
                                        color: Colors.white,
                                      ),
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(
                                          horizontal: 12,
                                        ),
                                        child: Center(
                                          child: TextFormField(
                                            controller: firstName,
                                            decoration: InputDecoration(
                                              border: InputBorder.none,
                                              hintText: getTranslated(
                                                  context, 'First Name'),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                      horizontal: 4,
                                    ),
                                    child: Container(
                                      clipBehavior: Clip.antiAlias,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(25),
                                        border:
                                            Border.all(color: Colors.grey[400]),
                                        color: Colors.white,
                                      ),
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 12),
                                        child: Center(
                                            child: TextFormField(
                                          controller: lastName,
                                          decoration: InputDecoration(
                                            border: InputBorder.none,
                                            hintText: getTranslated(
                                                context, 'Last Name'),
                                          ),
                                        )),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(
                              horizontal: 4,
                            ),
                            child: Container(
                              clipBehavior: Clip.antiAlias,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(25),
                                border: Border.all(color: Colors.grey[400]),
                                color: Colors.white,
                              ),
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                  horizontal: 12,
                                ),
                                child: Center(
                                  child: TextFormField(
                                    controller: email,
                                    decoration: InputDecoration(
                                      border: InputBorder.none,
                                      hintText: getTranslated(context, 'Email'),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(
                              horizontal: 4,
                            ),
                            child: Container(
                              height: 200,
                              clipBehavior: Clip.antiAlias,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(25),
                                border: Border.all(color: Colors.grey[400]),
                                color: Colors.white,
                              ),
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                  horizontal: 12,
                                ),
                                child: Center(
                                  child: TextFormField(
                                    expands: true,
                                    keyboardType: TextInputType.multiline,
                                    maxLines: null,
                                    controller: details,
                                    decoration: InputDecoration(
                                      border: InputBorder.none,
                                      hintText:
                                          getTranslated(context, 'Details'),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(
                              horizontal: 4,
                            ),
                            child: GestureDetector(
                              onTap: () {
                                getFile();
                              },
                              child: Container(
                                clipBehavior: Clip.antiAlias,
                                height: 50,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(25),
                                  border: Border.all(
                                      color: Config.primaryColor, width: 2),
                                  color: Colors.white,
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 12,
                                  ),
                                  child: Center(
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        file == null
                                            ? Container(
                                                child: Text(
                                                  getTranslated(context,
                                                      'Upload your CV  '),
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              )
                                            : Expanded(
                                                child: Container(
                                                  width: sizeAware.width,
                                                  child: Text(
                                                    file.path,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                  ),
                                                ),
                                              ),
                                        Icon(
                                          Icons.upload_file,
                                          color: Config.primaryColor,
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Center(
                            child: GestureDetector(
                              onTap: () {
                                if (file != null &&
                                    email.text != '' &&
                                    firstName.text != '' &&
                                    details.text != '' &&
                                    lastName.text != '') {
                                  jobBloc.add(
                                    ApplyJobRequested(
                                        cv: file,
                                        email: email.text,
                                        first_name: firstName.text,
                                        id: job.id,
                                        details: details.text,
                                        last_name: lastName.text),
                                  );
                                }
                              },
                              child: Container(
                                width: sizeAware.width * 0.8,
                                height: 60,
                                decoration: BoxDecoration(
                                  //color: Config.primaryColor,
                                  gradient: LinearGradient(
                                      begin: Alignment.topRight,
                                      end: Alignment.bottomLeft,
                                      colors: [HexColor('#F93E4F'), HexColor('#8D2CD3')]),
                                  borderRadius: BorderRadius.circular(30),
                                ),
                                child: Center(
                                  child: Text(
                                    getTranslated(context, 'Apply'),
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 18),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              );
            }
            return Container();
          },
        ),
      ),
    );
  }
}
