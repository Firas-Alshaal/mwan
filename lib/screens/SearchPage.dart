import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dtic/blocs/home/home_bloc.dart';
import 'package:flutter_dtic/blocs/product/product_bloc.dart';
import 'package:flutter_dtic/blocs/product/product_event.dart';
import 'package:flutter_dtic/blocs/product/product_state.dart';
import 'package:flutter_dtic/localization/localization_constants.dart';
import 'package:flutter_dtic/models/Product.dart';
import 'package:flutter_dtic/services/ProductApi.dart';
import 'package:flutter_dtic/widgets/FeaturedProductCard.dart';
import 'package:flutter_dtic/widgets/MyAppBar.dart';
import 'package:flutter_dtic/widgets/MyBottomNavigation.dart';
import 'package:flutter_dtic/widgets/MyFloatingActionButton.dart';
import 'package:http/http.dart' as http;

import '../config.dart';

class SearchPage extends StatefulWidget {
  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  TextEditingController query = TextEditingController();
  List<Product> products = [];
  ProductApi productApi = ProductApi(httpClient: http.Client());
  ProductBloc productBloc;

  @override
  void initState() {
    super.initState();
    productBloc = ProductBloc(productApi: productApi);
  }

  @override
  Widget build(BuildContext context) {
    final sizeAware = MediaQuery.of(context).size;

    return Scaffold(
      bottomNavigationBar: MyBottomNavigation(
        index: 1,
      ),
      floatingActionButton: MyFloatingActionButton(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      appBar: myAppBar(getTranslated(context, 'Search'), null),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Column(
            children: [
              Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(15),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: query,
                    decoration: InputDecoration(
                      prefixIcon: Icon(Icons.search),
                      border: InputBorder.none,
                      hintText: getTranslated(context, 'Search'),
                    ),
                    onChanged: (val) {
                      productBloc.add(SearchRequested(query: query.text));
                    },
                  ),
                ),
              ),
              BlocBuilder(
                cubit: productBloc,
                buildWhen: (previous, current) {
                  if (current is SearchResult) {
                    return true;
                  } else
                    return false;
                },
                builder: (context, state) {
                  if (state is ProductsLoadInProgress) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                  if (state is ProductsLoadFailure) {
                    return Scaffold(
                      body: SafeArea(
                        child: Container(
                          width: sizeAware.width,
                          height: sizeAware.height,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                getTranslated(context, 'Connection Error'),
                                style: TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.bold),
                              ),
                              SizedBox(height: 20),
                              FlatButton(
                                color: Config.primaryColor,
                                onPressed: () {
                                  setState(() {
                                    productBloc =
                                        ProductBloc(productApi: productApi);
                                  });
                                },
                                child: Text(
                                  getTranslated(context, 'Refresh'),
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    );
                  }
                  if (state is SearchResult) {
                    products = state.products;
                    return GridView.count(
                      shrinkWrap: true,
                      crossAxisCount: 2,
                      crossAxisSpacing: 3,
                      childAspectRatio: 0.8,
                      physics: NeverScrollableScrollPhysics(),
                      children: products
                          .map(
                            (e) => FeaturedProductCard(
                              product: e,
                            ),
                          )
                          .toList(),
                    );
                  }
                  return Container();
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
