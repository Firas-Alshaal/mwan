import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dtic/blocs/profile/profile_bloc.dart';
import 'package:flutter_dtic/blocs/profile/profile_event.dart';
import 'package:flutter_dtic/blocs/profile/profile_state.dart';
import 'package:flutter_dtic/config.dart';
import 'package:flutter_dtic/localization/localization_constants.dart';
import 'package:flutter_dtic/models/City.dart';
import 'package:flutter_dtic/models/Order.dart';
import 'package:flutter_dtic/models/User.dart';
import 'package:flutter_dtic/services/ProfileApi.dart';
import 'package:flutter_dtic/widgets/Loading.dart';
import 'package:flutter_dtic/widgets/MyAppBar.dart';
import 'package:flutter_dtic/widgets/MyBottomNavigation.dart';
import 'package:flutter_dtic/widgets/MyFloatingActionButton.dart';
import 'package:flutter_dtic/widgets/OrderCard.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:http/http.dart' as http;
import '../constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  bool choice = true;
  ProfileBloc profileBloc;
  ProfileApi profileApi = ProfileApi(httpClient: http.Client());
  User user;
  List<Order> orders = [];
  TextEditingController fname = TextEditingController();
  TextEditingController lname = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController phone = TextEditingController();
  TextEditingController password = TextEditingController();
  TextEditingController city = TextEditingController();
  List<City> cities = [];
  bool isEdited = false;

  @override
  void initState() {
    super.initState();
    profileBloc = ProfileBloc(profileApi: profileApi);
    profileBloc.add(ProfileRequested());
  }

  @override
  Widget build(BuildContext context) {
    final sizeAware = MediaQuery.of(context).size;
    return BlocListener(
      cubit: profileBloc,
      listener: (context, state) {
        if (state is ResendCodeSuccess) {
          if (state.status) {
            print('hereee');
            Navigator.pushNamed(context, '/verify order', arguments: {
              'id': state.id,
            }).then((value) => profileBloc.add(ProfileRequested()));
          } else {
            Fluttertoast.showToast(
              toastLength: Toast.LENGTH_LONG,
              msg: 'Error',
            );
          }
        }
        if (state is UpdateProfileFailed) {
          Fluttertoast.showToast(
              toastLength: Toast.LENGTH_LONG, msg: state.msg);
        }
        if (state is UpdateProfileLoadSuccess) {
          Navigator.pushReplacementNamed(context, '/home');
          Fluttertoast.showToast(
            toastLength: Toast.LENGTH_LONG,
            msg: getTranslated(context, 'Update Success'),
          );
        }
      },
      child: BlocBuilder(
        cubit: profileBloc,
        builder: (context, state) {
          if (state is ProfileLoadInProgress) {
            return Loading();
          }
          if (state is ProfileLoadFailure) {
            return Scaffold(
              body: SafeArea(
                child: Container(
                  width: sizeAware.width,
                  height: sizeAware.height,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        getTranslated(context, 'Connection Error'),
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(height: 20),
                      FlatButton(
                        color: Config.primaryColor,
                        onPressed: () {
                          setState(() {
                            profileBloc.add(ProfileRequested());
                          });
                        },
                        child: Text(
                          getTranslated(context, 'Refresh'),
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            );
          }
          if (state is ProfileLoadSuccess ||
              state is ProfileNotUpdated ||
              state is ResendCodeSuccess ||
              state is UpdateProfileLoadSuccess ||
              state is UpdateProfileFailed) {
            if (state is ProfileLoadSuccess ||
                state is ProfileNotUpdated ||
                state is UpdateProfileLoadSuccess) {
              user = state.user;
            }
            if (state is UpdateProfileLoadSuccess) isEdited = false;

            if (state is ProfileLoadSuccess && state.orders != null) {
              orders = state.orders;
              cities = state.cities;
            }

            if (!isEdited) {
              fname.text = user.first_name;
              lname.text = user.last_name;
              email.text = user.email;
              phone.text = user.phone;
              city.text = user.city_id.toString();
              isEdited = true;
            }
            return Scaffold(
              bottomNavigationBar: MyBottomNavigation(
                index: 3,
              ),
              floatingActionButton: MyFloatingActionButton(),
              floatingActionButtonLocation:
                  FloatingActionButtonLocation.centerDocked,
              appBar: myAppBar(getTranslated(context, 'Profile'), null),
              body: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    Center(
                      child: Container(
                        height: 60,
                        width: sizeAware.width * 0.7,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(18),
                          boxShadow: [
                            BoxShadow(
                              blurRadius: 5,
                              color: Colors.grey[300],
                              offset: Offset(1, 2),
                              spreadRadius: 5,
                            ),
                          ],
                        ),
                        clipBehavior: Clip.hardEdge,
                        child: Row(
                          children: [
                            Expanded(
                              child: GestureDetector(
                                onTap: () {
                                  setState(() {
                                    choice = true;
                                  });
                                },
                                child: Container(
                                  //
                                  height: 60,
                                  color: choice
                                      ? Config.primaryColor
                                      : Colors.white,
                                  child: Center(
                                    child: Text(
                                      getTranslated(context, 'My Profile'),
                                      style: TextStyle(
                                        color: choice
                                            ? Colors.white
                                            : Colors.black,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              child: GestureDetector(
                                onTap: () {
                                  setState(() {
                                    choice = false;
                                  });
                                },
                                child: Container(
                                  height: 60,
                                  color: !choice
                                      ? Config.primaryColor
                                      : Colors.white,
                                  child: Center(
                                    child: Text(
                                      getTranslated(context, 'My Orders'),
                                      style: TextStyle(
                                        color: choice
                                            ? Colors.black
                                            : Colors.white,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    choice
                        ? Profile(
                            cities: cities,
                            city: city,
                            fname: fname,
                            lname: lname,
                            email: email,
                            phone: phone,
                            password: password,
                            profileBloc: profileBloc,
                            sizeAware: sizeAware,
                          )
                        : Column(
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Text('Order No.'),
                                  Text('Date'),
                                  Text('Status'),
                                ],
                              ),
                              getOrders(),
                            ],
                          ),
                  ],
                ),
              ),
            );
          }
          return Container();
        },
      ),
    );
  }

  getOrders() {
    List<Widget> list = [];
    list = orders
        .map((e) => OrderCard(
              order: e,
              profileBloc: profileBloc,
            ))
        .toList();

    return Column(
      children: list,
    );
  }
}

class Profile extends StatefulWidget {
  Profile({
    Key key,
    @required this.fname,
    @required this.lname,
    @required this.email,
    @required this.phone,
    @required this.password,
    @required this.profileBloc,
    @required this.sizeAware,
    @required this.cities,
    @required this.city,
  }) : super(key: key);

  final TextEditingController fname;
  final TextEditingController lname;
  final TextEditingController email;
  final TextEditingController phone;
  final TextEditingController password;
  final TextEditingController city;
  final ProfileBloc profileBloc;
  List<City> cities = [];

  final Size sizeAware;

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            getTranslated(context, 'First Name'),
            style: TextStyle(
              fontSize: 18,
              color: Config.primaryColor,
            ),
          ),
          SizedBox(
            height: 12,
          ),
          Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(18),
              boxShadow: [
                BoxShadow(
                  blurRadius: 5,
                  color: Colors.grey[300],
                  offset: Offset(1, 2),
                  spreadRadius: 5,
                ),
              ],
            ),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(8, 4, 8, 4),
              child: TextFormField(
                controller: widget.fname,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: getTranslated(context, 'First Name'),
                ),
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Text(
            getTranslated(context, 'Last Name'),
            style: TextStyle(
              fontSize: 18,
              color: Config.primaryColor,
            ),
          ),
          SizedBox(
            height: 12,
          ),
          Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(18),
              boxShadow: [
                BoxShadow(
                  blurRadius: 5,
                  color: Colors.grey[300],
                  offset: Offset(1, 2),
                  spreadRadius: 5,
                ),
              ],
            ),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(8, 4, 8, 4),
              child: TextFormField(
                controller: widget.lname,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: getTranslated(context, 'Last Name'),
                ),
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Text(
            getTranslated(context, 'Email'),
            style: TextStyle(
              fontSize: 18,
              color: Config.primaryColor,
            ),
          ),
          SizedBox(
            height: 12,
          ),
          Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(18),
              boxShadow: [
                BoxShadow(
                  blurRadius: 5,
                  color: Colors.grey[300],
                  offset: Offset(1, 2),
                  spreadRadius: 5,
                ),
              ],
            ),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(8, 4, 8, 4),
              child: TextFormField(
                controller: widget.email,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: getTranslated(context, 'Email'),
                ),
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Text(
            getTranslated(context, 'Phone'),
            style: TextStyle(
              fontSize: 18,
              color: Config.primaryColor,
            ),
          ),
          SizedBox(
            height: 12,
          ),
          Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(18),
              boxShadow: [
                BoxShadow(
                  blurRadius: 5,
                  color: Colors.grey[300],
                  offset: Offset(1, 2),
                  spreadRadius: 5,
                ),
              ],
            ),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(8, 4, 8, 4),
              child: TextFormField(
                controller: widget.phone,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: getTranslated(context, 'Phone'),
                ),
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Text(
            getTranslated(context, 'City'),
            style: TextStyle(
              fontSize: 18,
              color: Config.primaryColor,
            ),
          ),
          SizedBox(
            height: 12,
          ),
          Container(
            clipBehavior: Clip.antiAlias,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(2),
              border: Border.all(),
              color: Colors.white,
            ),
            child: Center(
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 8,
                ),
                child: DropdownButton(
                  value: widget.city.text == ''
                      ? null
                      : int.parse(widget.city.text),
                  hint: Text(
                    getTranslated(context, 'City'),
                  ),
                  underline: SizedBox(),
                  isExpanded: true,
                  onChanged: (val) {
                    setState(() {
                      widget.city.text = val.toString();
                    });

                    print(widget.city.text);
                  },
                  items: widget.cities
                      .map(
                        (e) => DropdownMenuItem<int>(
                          value: e.id,
                          child: Text(
                            e.name_en,
                            style: TextStyle(),
                          ),
                        ),
                      )
                      .toList(),
                ),
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Text(
            getTranslated(context, 'New Password'),
            style: TextStyle(
              fontSize: 18,
              color: Config.primaryColor,
            ),
          ),
          SizedBox(
            height: 12,
          ),
          Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(18),
              boxShadow: [
                BoxShadow(
                  blurRadius: 5,
                  color: Colors.grey[300],
                  offset: Offset(1, 2),
                  spreadRadius: 5,
                ),
              ],
            ),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(8, 4, 8, 4),
              child: TextFormField(
                obscureText: true,
                controller: widget.password,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: getTranslated(context, 'Password'),
                ),
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Center(
            child: GestureDetector(
              onTap: () {
                widget.profileBloc.add(
                  UpdateProfile(
                    email: widget.email.text,
                    first_name: widget.fname.text,
                    last_name: widget.lname.text,
                    phone: widget.phone.text,
                    password: widget.password.text,
                    city_id: widget.city.text,
                  ),
                );
              },
              child: Container(
                decoration: BoxDecoration(
                  //color: Config.primaryColor,
                  gradient: LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                      colors: <Color>[
                        HexColor('#8D2CD3'),
                        HexColor('#F93E4F'),
                      ]),
                  borderRadius: BorderRadius.circular(25),
                ),
                width: widget.sizeAware.width * 0.9,
                height: 50,
                child: Center(
                  child: Text(
                    getTranslated(context, 'Save'),
                    style: TextStyle(color: Colors.white, fontSize: 17),
                  ),
                ),
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
        ],
      ),
    );
  }
}
