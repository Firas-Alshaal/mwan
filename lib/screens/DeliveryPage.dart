import 'package:date_time_picker/date_time_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dtic/blocs/auth/auth_bloc.dart';
import 'package:flutter_dtic/blocs/auth/auth_state.dart';
import 'package:flutter_dtic/blocs/cart/cart_bloc.dart';
import 'package:flutter_dtic/blocs/cart/cart_event.dart';
import 'package:flutter_dtic/blocs/cart/cart_state.dart';
import 'package:flutter_dtic/config.dart';
import 'package:flutter_dtic/localization/localization_constants.dart';
import 'package:flutter_dtic/models/Address.dart';
import 'package:flutter_dtic/models/CartItem.dart';
import 'package:flutter_dtic/models/City.dart';
import 'package:flutter_dtic/widgets/CartItem.dart';
import 'package:flutter_dtic/widgets/DeliveryProduct.dart';
import 'package:flutter_dtic/widgets/Loading.dart';
import 'package:flutter_dtic/widgets/MyAppBar.dart';
import 'package:flutter_dtic/widgets/StepsWidget.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:flutter_svg/svg.dart';

class DeliveryPage extends StatefulWidget {
  @override
  _DeliveryPageState createState() => _DeliveryPageState();
}

class _DeliveryPageState extends State<DeliveryPage> {
  TextEditingController firstName = TextEditingController();
  TextEditingController lastName = TextEditingController();
  TextEditingController address = TextEditingController();
  TextEditingController phone = TextEditingController();
  TextEditingController deliveryTime = TextEditingController();
  TextEditingController city = TextEditingController();
  List<City> cities = [];
  List<Address> addresses = [];
  CartBloc cartBloc;
  List<CartItem> items = [];
  double tot_price = 0;
  double price_after_disc = 0;
  bool is_new_address = false;
  bool cashonDelIvery = true;
  bool creditCart = false;
  bool paypal = false;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController new_address = TextEditingController();

  @override
  void initState() {
    super.initState();
    cartBloc = BlocProvider.of<CartBloc>(context);
    cartBloc.add(DeliveryRequested());
  }

  @override
  Widget build(BuildContext context) {
    final sizeAware = MediaQuery.of(context).size;
    final authBloc = BlocProvider.of<AuthBloc>(context);
    Map args = ModalRoute.of(context).settings.arguments;
    final String coupon = args['coupon'] ?? '';
    final String discount = args['discount'] ?? "";

    return BlocListener(
      cubit: cartBloc,
      listener: (context, state) {
        if (state is OrderSuccess) {
          if (state.status == 0) {
            Navigator.pushReplacementNamed(context, '/verify order',
                arguments: {
                  'id': state.id,
                }).then((value) => Navigator.pop(context));
          } else {
            Fluttertoast.showToast(
              msg: getTranslated(context, 'Order Success'),
              toastLength: Toast.LENGTH_LONG,
            );
            Navigator.popUntil(context, (route) => route.isFirst);
          }
        }
        if (state is OrderFaield) {
          Fluttertoast.showToast(
              msg: state.msg, toastLength: Toast.LENGTH_LONG);
        }
      },
      child: Scaffold(
        appBar: myAppBar(getTranslated(context, 'Delivery Information'), null),
        body: BlocBuilder(
          cubit: authBloc,
          builder: (context, state) {
            if (state is Authenticated) {
              return BlocBuilder(
                cubit: cartBloc,
                builder: (context, state) {
                  if (state is CartLoadInProgress) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                  if (state is CartLoadFailure) {
                    return Scaffold(
                      body: SafeArea(
                        child: Container(
                          width: sizeAware.width,
                          height: sizeAware.height,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                getTranslated(context, 'Connection Error'),
                                style: TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.bold),
                              ),
                              SizedBox(height: 20),
                              FlatButton(
                                color: Config.primaryColor,
                                onPressed: () {
                                  setState(() {
                                    cartBloc.add(DeliveryRequested());
                                  });
                                },
                                child: Text(
                                  getTranslated(context, 'Refresh'),
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    );
                  }
                  if (state is DeliveryLoadSuccess ||
                      state is OrderFaield ||
                      state is OrderSuccess ||
                      state is CartLoadSuccess ||
                      state is StoreAddressSuccess) {
                    if (state is DeliveryLoadSuccess) {
                      items = state.items;
                      cities = state.cities;
                      addresses = state.addresses;
                    }
                    if (state is StoreAddressSuccess) {
                      addresses = state.addresses;
                    }

                    if (state is CartLoadSuccess) {
                      items = state.items;
                    }

                    if (state is OrderSuccess) {
                      items = [];
                    }

                    tot_price = 0;
                    for (int i = 0; i < items.length; i++) {
                      tot_price += items[i].total_price;
                    }
                    price_after_disc = discount != ''
                        ? tot_price - (tot_price * double.parse(discount) / 100)
                        : 0;
                    return SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: sizeAware.height * 0.03,
                          ),
                          StepsWidget(step: 2),
                          SizedBox(
                            height: sizeAware.height * 0.03,
                          ),
                          Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  getTranslated(context, 'Payment on delivery'),
                                  style: TextStyle(
                                    color: Config.primaryColor,
                                    fontSize: 22,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                SizedBox(
                                  height: sizeAware.height * 0.04,
                                ),
                                Row(
                                  children: [
                                    Expanded(
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            8, 8, 8, 8),
                                        child: Container(
                                          clipBehavior: Clip.antiAlias,
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(25),
                                            border: Border.all(
                                                color: Colors.grey[400]),
                                            color: Colors.white,
                                          ),
                                          child: Center(
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                horizontal: 8,
                                              ),
                                              child: DropdownButton(
                                                value: address.text == ''
                                                    ? null
                                                    : int.parse(address.text),
                                                hint: Text(
                                                  getTranslated(
                                                      context, 'Address'),
                                                ),
                                                underline: SizedBox(),
                                                isExpanded: true,
                                                onChanged: (val) {
                                                  setState(() {
                                                    address.text =
                                                        val.toString();
                                                  });
                                                  print(address.text);
                                                },
                                                items: addresses
                                                    .map(
                                                      (e) =>
                                                          DropdownMenuItem<int>(
                                                        value: e.id,
                                                        child: Text(
                                                          e.address_text ?? "",
                                                          style: TextStyle(),
                                                        ),
                                                      ),
                                                    )
                                                    .toList(),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Card(
                                      child: IconButton(
                                        icon: Icon(Icons.add),
                                        onPressed: () {
                                          showDialog(
                                              context: context,
                                              /*child: AlertDialog(
                                                title: Text(getTranslated(
                                                    context,
                                                    "Add new address")),
                                                content: Container(
                                                  height: 200,
                                                  child: SingleChildScrollView(
                                                    child: Form(
                                                      key: _formKey,
                                                      child: Column(
                                                        children: [
                                                          TextFormField(
                                                            decoration:
                                                                InputDecoration(
                                                              filled: true,
                                                              hintText:
                                                                  getTranslated(
                                                                      context,
                                                                      'Address'),
                                                              fillColor: Colors
                                                                  .grey[100],
                                                            ),
                                                            controller:
                                                                new_address,
                                                            validator: (value) =>
                                                                value.length ==
                                                                        0
                                                                    ? getTranslated(
                                                                        context,
                                                                        "Address is required")
                                                                    : null,
                                                          ),
                                                          SizedBox(
                                                            height: 30,
                                                          ),
                                                          RaisedButton(
                                                            color: Config
                                                                .primaryColor,
                                                            onPressed: () {
                                                              if (_formKey
                                                                  .currentState
                                                                  .validate()) {
                                                                cartBloc.add(
                                                                    StoreAddressRequested(
                                                                  address:
                                                                      new_address
                                                                          .text,
                                                                ));
                                                                setState(() {
                                                                  new_address
                                                                      .text = '';
                                                                });
                                                                Navigator.pop(
                                                                    context);
                                                              }
                                                            },
                                                            child: Text(
                                                              getTranslated(
                                                                  context,
                                                                  'ADD'),
                                                              style: TextStyle(
                                                                color: Colors
                                                                    .white,
                                                              ),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),*/
                                              builder: (BuildContext context) {
                                                return AlertDialog(
                                                  title: Text(getTranslated(
                                                      context,
                                                      "Add new address")),
                                                  content: Container(
                                                    height: 200,
                                                    child:
                                                        SingleChildScrollView(
                                                      child: Form(
                                                        key: _formKey,
                                                        child: Column(
                                                          children: [
                                                            TextFormField(
                                                              decoration:
                                                                  InputDecoration(
                                                                filled: true,
                                                                hintText:
                                                                    getTranslated(
                                                                        context,
                                                                        'Address'),
                                                                fillColor:
                                                                    Colors.grey[
                                                                        100],
                                                              ),
                                                              controller:
                                                                  new_address,
                                                              validator: (value) => value
                                                                          .length ==
                                                                      0
                                                                  ? getTranslated(
                                                                      context,
                                                                      "Address is required")
                                                                  : null,
                                                            ),
                                                            SizedBox(
                                                              height: 30,
                                                            ),
                                                            RaisedButton(
                                                              color: Config
                                                                  .primaryColor,
                                                              onPressed: () {
                                                                if (_formKey
                                                                    .currentState
                                                                    .validate()) {
                                                                  cartBloc.add(
                                                                      StoreAddressRequested(
                                                                    address:
                                                                        new_address
                                                                            .text,
                                                                  ));
                                                                  setState(() {
                                                                    new_address
                                                                        .text = '';
                                                                  });
                                                                  Navigator.pop(
                                                                      context);
                                                                }
                                                              },
                                                              child: Text(
                                                                getTranslated(
                                                                    context,
                                                                    'ADD'),
                                                                style:
                                                                    TextStyle(
                                                                  color: Colors
                                                                      .white,
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                );
                                              });
                                        },
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: sizeAware.height * 0.04,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Expanded(
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            8, 8, 8, 8),
                                        child: Container(
                                          clipBehavior: Clip.antiAlias,
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(25),
                                            border: Border.all(
                                              color: Colors.grey[400],
                                            ),
                                            color: Colors.white,
                                          ),
                                          child: Padding(
                                            padding: const EdgeInsets.symmetric(
                                              horizontal: 12,
                                            ),
                                            child: Container(
                                              height: 50,
                                              child: Center(
                                                  child: DateTimePicker(
                                                decoration: InputDecoration(
                                                    border: InputBorder.none,
                                                    hintText: getTranslated(
                                                      context,
                                                      'Date',
                                                    )),
                                                type: DateTimePickerType.date,
                                                dateMask: 'yyyy-MM-dd',
                                                controller: deliveryTime,
                                                firstDate: DateTime.now(),
                                                lastDate: DateTime(2100),
                                                onChanged: (val) => print(val),
                                                validator: (val) {
                                                  return null;
                                                },
                                                onSaved: (val) => print(val),
                                              )),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            8, 8, 8, 8),
                                        child: Container(
                                          clipBehavior: Clip.antiAlias,
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(25),
                                            border: Border.all(
                                                color: Colors.grey[400]),
                                            color: Colors.white,
                                          ),
                                          child: Center(
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                horizontal: 8,
                                              ),
                                              child: DropdownButton(
                                                value: city.text == ''
                                                    ? null
                                                    : int.parse(city.text),
                                                hint: Text(
                                                  getTranslated(
                                                      context, 'City'),
                                                ),
                                                underline: SizedBox(),
                                                isExpanded: true,
                                                onChanged: (val) {
                                                  setState(() {
                                                    city.text = val.toString();
                                                  });
                                                  print(city.text);
                                                },
                                                items: cities
                                                    .map(
                                                      (e) =>
                                                          DropdownMenuItem<int>(
                                                        value: e.id,
                                                        child: Text(
                                                          e.name_en ?? "",
                                                          style: TextStyle(),
                                                        ),
                                                      ),
                                                    )
                                                    .toList(),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: sizeAware.height * 0.03,
                                ),
                                SizedBox(
                                  height: sizeAware.height * 0.04,
                                ),
                                Text(
                                  getTranslated(context, 'payemnt Method'),
                                  style: TextStyle(
                                    color: Config.primaryColor,
                                    fontSize: 22,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),

                                SizedBox(
                                  height: sizeAware.height * 0.02,
                                ),
                                GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      cashonDelIvery = true;
                                      creditCart = false;
                                      paypal = false;
                                    });
                                  },
                                  child: Row(
                                    children: [
                                      SvgPicture.asset(
                                        'assets/cash-on-delivery.svg',
                                        width: sizeAware.width * 0.08,
                                      ),
                                      SizedBox(
                                        width: 4,
                                      ),
                                      Text(
                                        getTranslated(context, 'CashDel'),
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 16,
                                        ),
                                      ),
                                      Spacer(),
                                      Container(
                                        width: 20,
                                        height: 20,
                                        decoration: BoxDecoration(
                                            color: cashonDelIvery
                                                ? Config.primaryColor
                                                : Colors.white,
                                            borderRadius:
                                                BorderRadius.circular(100),
                                            border: Border.all(
                                              color: cashonDelIvery
                                                  ? Colors.grey
                                                  : Colors.grey,
                                            )),
                                      )
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: sizeAware.height * 0.02,
                                ),
                                GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      creditCart = true;
                                      cashonDelIvery = false;
                                      paypal = false;
                                    });
                                  },
                                  child: Row(
                                    children: [
                                      SvgPicture.asset(
                                        'assets/mastercard.svg',
                                        width: sizeAware.width * 0.08,
                                      ),
                                      SizedBox(
                                        width: 4,
                                      ),
                                      Text(
                                        getTranslated(context, 'Credit Card'),
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 16,
                                        ),
                                      ),
                                      Spacer(),
                                      Container(
                                        width: 20,
                                        height: 20,
                                        decoration: BoxDecoration(
                                            color: creditCart
                                                ? Config.primaryColor
                                                : Colors.white,
                                            borderRadius:
                                                BorderRadius.circular(100),
                                            border: Border.all(
                                              color: creditCart
                                                  ? Colors.grey
                                                  : Colors.grey,
                                            )),
                                      )
                                    ],
                                  ),
                                ),
                                Visibility(
                                  visible: creditCart,
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 20),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          getTranslated(
                                              context, 'Card Information'),
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Container(
                                            height: 50,
                                            child: TextField(
                                              decoration: new InputDecoration(
                                                  border:
                                                      new OutlineInputBorder(
                                                          borderRadius:
                                                              BorderRadius.all(
                                                                  Radius
                                                                      .circular(
                                                                          10)),
                                                          borderSide:
                                                              new BorderSide(
                                                                  color: Colors
                                                                      .teal)),

                                                  // helperText:
                                                  //     'Keep it short, this is just a demo.',
                                                  labelText: getTranslated(
                                                      context, 'CardNumber'),
                                                  // prefixIcon: const Icon(
                                                  //   Icons.person,
                                                  //   color: Colors.green,
                                                  // ),
                                                  // prefixText: ' ',
                                                  // suffixText: 'USD',
                                                  suffixStyle: const TextStyle(
                                                      color: Colors.green)),
                                            )),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        Row(
                                          children: [
                                            Flexible(
                                              flex: 2,
                                              child: Container(
                                                  height: 50,
                                                  child: TextField(
                                                    decoration:
                                                        new InputDecoration(
                                                            border: new OutlineInputBorder(
                                                                borderRadius: BorderRadius
                                                                    .all(Radius
                                                                        .circular(
                                                                            10)),
                                                                borderSide: new BorderSide(
                                                                    color: Colors
                                                                        .teal)),

                                                            // helperText:
                                                            //     'Keep it short, this is just a demo.',
                                                            labelText: 'MM/YY',
                                                            // prefixIcon: const Icon(
                                                            //   Icons.person,
                                                            //   color: Colors.green,
                                                            // ),
                                                            // prefixText: ' ',
                                                            // suffixText: 'USD',
                                                            suffixStyle:
                                                                const TextStyle(
                                                                    color: Colors
                                                                        .green)),
                                                  )),
                                            ),
                                            Spacer(),
                                            Flexible(
                                              flex: 2,
                                              child: Container(
                                                  height: 50,
                                                  child: TextField(
                                                    decoration:
                                                        new InputDecoration(
                                                            border: new OutlineInputBorder(
                                                                borderRadius: BorderRadius
                                                                    .all(Radius
                                                                        .circular(
                                                                            10)),
                                                                borderSide: new BorderSide(
                                                                    color: Colors
                                                                        .teal)),

                                                            // helperText:
                                                            //     'Keep it short, this is just a demo.',
                                                            labelText: 'CVC',
                                                            // prefixIcon: const Icon(
                                                            //   Icons.person,
                                                            //   color: Colors.green,
                                                            // ),
                                                            // prefixText: ' ',
                                                            // suffixText: 'USD',
                                                            suffixStyle:
                                                                const TextStyle(
                                                                    color: Colors
                                                                        .green)),
                                                  )),
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        Container(
                                            height: 50,
                                            child: TextField(
                                              decoration: new InputDecoration(
                                                  border:
                                                      new OutlineInputBorder(
                                                          borderRadius:
                                                              BorderRadius.all(
                                                                  Radius
                                                                      .circular(
                                                                          10)),
                                                          borderSide:
                                                              new BorderSide(
                                                                  color: Colors
                                                                      .teal)),

                                                  // helperText:
                                                  //     'Keep it short, this is just a demo.',
                                                  labelText: getTranslated(
                                                      context, 'CardName'),
                                                  // prefixIcon: const Icon(
                                                  //   Icons.person,
                                                  //   color: Colors.green,
                                                  // ),
                                                  // prefixText: ' ',
                                                  // suffixText: 'USD',
                                                  suffixStyle: const TextStyle(
                                                      color: Colors.green)),
                                            )),
                                      ],
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: sizeAware.height * 0.02,
                                ),
                                GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      creditCart = false;
                                      cashonDelIvery = false;
                                      paypal = true;
                                    });
                                  },
                                  child: Row(
                                    children: [
                                      SvgPicture.asset(
                                        'assets/paypal.svg',
                                        width: sizeAware.width * 0.06,
                                      ),
                                      SizedBox(
                                        width: 9,
                                      ),
                                      Text(
                                        "Paypal",
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 16,
                                        ),
                                      ),
                                      Spacer(),
                                      Container(
                                        width: 20,
                                        height: 20,
                                        decoration: BoxDecoration(
                                            color: paypal
                                                ? Config.primaryColor
                                                : Colors.white,
                                            borderRadius:
                                                BorderRadius.circular(100),
                                            border: Border.all(
                                              color: paypal
                                                  ? Colors.grey
                                                  : Colors.grey,
                                            )),
                                      )
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: sizeAware.height * 0.03,
                                ),

                                Visibility(
                                  visible: paypal,
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 20),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Center(
                                          child: Container(
                                            width: sizeAware.width * 0.4,
                                            height: 40,
                                            decoration: BoxDecoration(
                                              color: HexColor('#FFC439'),
                                              borderRadius:
                                                  BorderRadius.circular(30),
                                            ),
                                            child: Center(
                                                child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                RichText(
                                                  text: TextSpan(
                                                      text: "Pay",
                                                      style: TextStyle(
                                                          color: HexColor(
                                                              '#003087'),
                                                          fontSize: 18),
                                                      children: [
                                                        TextSpan(
                                                          text: "pal",
                                                          style: TextStyle(
                                                              color: HexColor(
                                                                  '#009CDE'),
                                                              fontSize: 18),
                                                        )
                                                      ]),
                                                ),
                                              ],
                                            )),
                                          ),
                                        ),
                                        // Text(
                                        //   "Paypal Information",
                                        //   style: TextStyle(
                                        //       color: Colors.black,
                                        //       fontSize: 16,
                                        //       fontWeight: FontWeight.bold),
                                        // ),
                                        // SizedBox(
                                        //   height: 10,
                                        // ),
                                        // Container(
                                        //     height: 50,
                                        //     child: TextField(
                                        //       decoration: new InputDecoration(
                                        //           border:
                                        //               new OutlineInputBorder(
                                        //                   borderRadius:
                                        //                       BorderRadius.all(
                                        //                           Radius
                                        //                               .circular(
                                        //                                   10)),
                                        //                   borderSide:
                                        //                       new BorderSide(
                                        //                           color: Colors
                                        //                               .teal)),

                                        //           // helperText:
                                        //           //     'Keep it short, this is just a demo.',
                                        //           labelText: getTranslated(
                                        //               context, 'Email'),
                                        //           // prefixIcon: const Icon(
                                        //           //   Icons.person,
                                        //           //   color: Colors.green,
                                        //           // ),
                                        //           // prefixText: ' ',
                                        //           // suffixText: 'USD',
                                        //           suffixStyle: const TextStyle(
                                        //               color: Colors.green)),
                                        //     )),
                                        // SizedBox(
                                        //   height: 20,
                                        // ),
                                        // Container(
                                        //     height: 50,
                                        //     child: TextField(
                                        //       decoration: new InputDecoration(
                                        //           border:
                                        //               new OutlineInputBorder(
                                        //                   borderRadius:
                                        //                       BorderRadius.all(
                                        //                           Radius
                                        //                               .circular(
                                        //                                   10)),
                                        //                   borderSide:
                                        //                       new BorderSide(
                                        //                           color: Colors
                                        //                               .teal)),

                                        //           // helperText:
                                        //           //     'Keep it short, this is just a demo.',
                                        //           labelText: getTranslated(
                                        //               context, 'Password'),
                                        //           // prefixIcon: const Icon(
                                        //           //   Icons.person,
                                        //           //   color: Colors.green,
                                        //           // ),
                                        //           // prefixText: ' ',
                                        //           // suffixText: 'USD',
                                        //           suffixStyle: const TextStyle(
                                        //               color: Colors.green)),
                                        //     )),
                                      ],
                                    ),
                                  ),
                                ),

                                SizedBox(
                                  height: sizeAware.height * 0.05,
                                ),
                                Text(
                                  getTranslated(context, 'Invoice details'),
                                  style: TextStyle(
                                    color: Config.primaryColor,
                                    fontSize: 22,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                getProducts(),
                                Container(
                                  height: 50,
                                  color: Colors.white,
                                  child: Row(
                                    children: [
                                      Text(
                                        getTranslated(context, 'Total: '),
                                      ),
                                      Spacer(),
                                      Center(
                                        child: Text('${tot_price}'),
                                      ),
                                    ],
                                  ),
                                ),
                                //
                                discount != ''
                                    ? Container(
                                        height: 50,
                                        color: Colors.white,
                                        child: Row(
                                          children: [
                                            Text(
                                              getTranslated(
                                                  context, 'Discount: '),
                                            ),
                                            Spacer(),
                                            Center(
                                              child: Text('${discount}%'),
                                            ),
                                          ],
                                        ),
                                      )
                                    : Container(),
                                discount != ''
                                    ? Container(
                                        height: 50,
                                        color: Colors.white,
                                        child: Row(
                                          children: [
                                            Text(
                                              getTranslated(context,
                                                  'Price after discount: '),
                                            ),
                                            Spacer(),
                                            Center(
                                              child:
                                                  Text('${price_after_disc}'),
                                            ),
                                          ],
                                        ),
                                      )
                                    : Container(),
                                Container(
                                  height: 50,
                                  color: Colors.white,
                                  child: Row(
                                    children: [
                                      Text(
                                        getTranslated(context, 'Delivery: '),
                                      ),
                                      Spacer(),
                                      Center(
                                        child: Text(
                                          getTranslated(context, 'Free'),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(height: 30),
                                Center(
                                  child: GestureDetector(
                                    onTap: () {
                                      if (address.text == '') {
                                        Fluttertoast.showToast(
                                          toastLength: Toast.LENGTH_LONG,
                                          msg: getTranslated(
                                              context, 'Please select address'),
                                        );
                                        return;
                                      }
                                      if (city.text == '') {
                                        Fluttertoast.showToast(
                                          toastLength: Toast.LENGTH_LONG,
                                          msg: getTranslated(
                                              context, 'Please select city'),
                                        );
                                        return;
                                      }
                                      if (deliveryTime.text == '') {
                                        Fluttertoast.showToast(
                                          toastLength: Toast.LENGTH_LONG,
                                          msg: getTranslated(
                                              context, 'Please select date'),
                                        );
                                        return;
                                      }
                                      if (address.text != '' &&
                                          city.text != '' &&
                                          deliveryTime.text != '') {
                                        cartBloc.add(
                                          AuthenticatedCheckoutRequested(
                                            cartItems: items,
                                            address: address.text,
                                            deliveryTime: deliveryTime.text,
                                            city: city.text,
                                          ),
                                        );
                                      }
                                    },
                                    child: Container(
                                      width: sizeAware.width * 0.8,
                                      height: 60,
                                      decoration: BoxDecoration(
                                        gradient: LinearGradient(
                                            begin: Alignment.topRight,
                                            end: Alignment.bottomLeft,
                                            colors: [
                                              HexColor('#F93E4F'),
                                              HexColor('#8D2CD3')
                                            ]),
                                        //color: Config.primaryColor,
                                        borderRadius: BorderRadius.circular(30),
                                      ),
                                      child: Center(
                                        child: Text(
                                          getTranslated(
                                              context, 'Confirm Delivery'),
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 18),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(height: 30),
                              ],
                            ),
                          ),
                        ],
                      ),
                    );
                  }
                  return Container();
                },
              );
            }
            if (state is NotAuthenticated) {
              return BlocBuilder(
                cubit: cartBloc,
                builder: (context, state) {
                  if (state is CartLoadInProgress) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                  if (state is CartLoadFailure) {
                    return Loading();
                  }
                  if (state is DeliveryLoadSuccess ||
                      state is OrderFaield ||
                      state is OrderSuccess ||
                      state is CartLoadSuccess) {
                    if (state is DeliveryLoadSuccess) {
                      items = state.items;
                      cities = state.cities;
                    }
                    if (state is CartLoadSuccess) {
                      items = state.items;
                    }
                    tot_price = 0;
                    for (int i = 0; i < items.length; i++) {
                      tot_price += items[i].total_price;
                    }
                    return SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: sizeAware.height * 0.03,
                          ),
                          StepsWidget(step: 2),
                          SizedBox(
                            height: sizeAware.height * 0.03,
                          ),
                          Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  getTranslated(context, 'Payment on delivery'),
                                  style: TextStyle(
                                    color: Config.primaryColor,
                                    fontSize: 22,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                SizedBox(
                                  height: sizeAware.height * 0.04,
                                ),
                                Container(
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: Padding(
                                          padding: const EdgeInsets.symmetric(
                                            horizontal: 4,
                                          ),
                                          child: Container(
                                            clipBehavior: Clip.antiAlias,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(25),
                                              border: Border.all(
                                                  color: Colors.grey[400]),
                                              color: Colors.white,
                                            ),
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                horizontal: 12,
                                              ),
                                              child: Center(
                                                child: TextFormField(
                                                  controller: firstName,
                                                  decoration: InputDecoration(
                                                    border: InputBorder.none,
                                                    hintText: getTranslated(
                                                        context, 'First Name'),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        child: Padding(
                                          padding: const EdgeInsets.symmetric(
                                            horizontal: 4,
                                          ),
                                          child: Container(
                                            clipBehavior: Clip.antiAlias,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(25),
                                              border: Border.all(
                                                  color: Colors.grey[400]),
                                              color: Colors.white,
                                            ),
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      horizontal: 12),
                                              child: Center(
                                                  child: TextFormField(
                                                controller: lastName,
                                                decoration: InputDecoration(
                                                  border: InputBorder.none,
                                                  hintText: getTranslated(
                                                      context, 'Last Name'),
                                                ),
                                              )),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: sizeAware.height * 0.03,
                                ),
                                Container(
                                  clipBehavior: Clip.antiAlias,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(25),
                                    border: Border.all(color: Colors.grey[400]),
                                    color: Colors.white,
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 12),
                                    child: Center(
                                      child: TextFormField(
                                        controller: address,
                                        decoration: InputDecoration(
                                          border: InputBorder.none,
                                          hintText:
                                              getTranslated(context, 'Address'),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: sizeAware.height * 0.04,
                                ),
                                Container(
                                  clipBehavior: Clip.antiAlias,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(25),
                                    border: Border.all(color: Colors.grey[400]),
                                    color: Colors.white,
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 12),
                                    child: Center(
                                      child: TextFormField(
                                        controller: phone,
                                        decoration: InputDecoration(
                                          border: InputBorder.none,
                                          hintText:
                                              getTranslated(context, 'Phone'),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: sizeAware.height * 0.04,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Expanded(
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            8, 8, 8, 8),
                                        child: Container(
                                          clipBehavior: Clip.antiAlias,
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(25),
                                            border: Border.all(
                                                color: Colors.grey[400]),
                                            color: Colors.white,
                                          ),
                                          child: Padding(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 12),
                                            child: Container(
                                              height: 50,
                                              child: Center(
                                                  child: DateTimePicker(
                                                type: DateTimePickerType.date,
                                                dateMask: 'yyyy-MM-dd',
                                                controller: deliveryTime,
                                                firstDate: DateTime.now(),
                                                lastDate: DateTime(2100),
                                                dateLabelText: 'Date',
                                                onChanged: (val) => print(val),
                                                validator: (val) {
                                                  return null;
                                                },
                                                onSaved: (val) => print(val),
                                              )),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            8, 8, 8, 8),
                                        child: Container(
                                          clipBehavior: Clip.antiAlias,
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(25),
                                            border: Border.all(
                                                color: Colors.grey[400]),
                                            color: Colors.white,
                                          ),
                                          child: Center(
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                horizontal: 8,
                                              ),
                                              child: DropdownButton(
                                                value: city.text == ''
                                                    ? null
                                                    : int.parse(city.text),
                                                hint: Text(
                                                  getTranslated(
                                                      context, 'City'),
                                                ),
                                                underline: SizedBox(),
                                                isExpanded: true,
                                                onChanged: (val) {
                                                  setState(() {
                                                    city.text = val.toString();
                                                  });
                                                  print(city.text);
                                                },
                                                items: cities
                                                    .map(
                                                      (e) =>
                                                          DropdownMenuItem<int>(
                                                        value: e.id,
                                                        child: Text(
                                                          e.name_en,
                                                          style: TextStyle(),
                                                        ),
                                                      ),
                                                    )
                                                    .toList(),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: sizeAware.height * 0.03,
                                ),
                                Text(
                                  getTranslated(context, 'Invoice details'),
                                  style: TextStyle(
                                    color: Config.primaryColor,
                                    fontSize: 22,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                getProducts(),
                                Container(
                                  height: 50,
                                  color: Colors.white,
                                  child: Row(
                                    children: [
                                      Text(getTranslated(context, 'Total: ')),
                                      Expanded(
                                        child: Center(
                                          child: Text('${tot_price}'),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  height: 50,
                                  color: Colors.white,
                                  child: Row(
                                    children: [
                                      Text(
                                          getTranslated(context, 'Delivery: ')),
                                      Expanded(
                                        child: Center(
                                          child: Text(
                                              getTranslated(context, 'Free')),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(height: 30),
                                Center(
                                  child: GestureDetector(
                                    onTap: () {
                                      if (address.text != '' &&
                                          city.text != '' &&
                                          deliveryTime.text != '' &&
                                          firstName.text != '' &&
                                          lastName.text != '' &&
                                          phone.text != '' &&
                                          items.length != 0) {
                                        cartBloc.add(
                                          CheckoutRequested(
                                            cartItems: items,
                                            firstName: firstName.text,
                                            lastName: lastName.text,
                                            address: address.text,
                                            deliveryTime: deliveryTime.text,
                                            city: city.text,
                                            phone: phone.text,
                                          ),
                                        );
                                      }
                                    },
                                    child: Container(
                                      width: sizeAware.width * 0.8,
                                      height: 60,
                                      decoration: BoxDecoration(
                                        color: Config.primaryColor,
                                        borderRadius: BorderRadius.circular(30),
                                      ),
                                      child: Center(
                                        child: Text(
                                          getTranslated(
                                              context, 'Confirm Delivery'),
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 18),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(height: 30),
                              ],
                            ),
                          ),
                        ],
                      ),
                    );
                  }
                  return Container();
                },
              );
            }
            return Container();
          },
        ),
      ),
    );
  }

  Widget getProducts() {
    List<DeliveryProduct> list = [];

    for (int i = 0; i < items.length; i++) {
      list.add(DeliveryProduct(
        cartItem: items[i],
      ));
    }
    return Column(
      children: list,
    );
  }
}
