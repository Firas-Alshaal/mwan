import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dtic/blocs/home/home_bloc.dart';
import 'package:flutter_dtic/blocs/home/home_event.dart';
import 'package:flutter_dtic/blocs/home/home_state.dart';
import 'package:flutter_dtic/config.dart';
import 'package:flutter_dtic/constants.dart';
import 'package:flutter_dtic/localization/localization_constants.dart';
import 'package:flutter_dtic/models/Gift.dart';
import 'package:flutter_dtic/models/Product.dart';
import 'package:flutter_dtic/services/HomeApi.dart';
import 'package:flutter_dtic/widgets/ImageCarusel.dart';
import 'package:flutter_dtic/widgets/Loading.dart';
import 'package:flutter_dtic/widgets/MyAppBar.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_dtic/models/CartItem.dart';
import 'package:flutter_dtic/blocs/cart/cart_bloc.dart';
import 'package:flutter_dtic/blocs/cart/cart_event.dart';
import 'package:flutter_dtic/blocs/cart/cart_state.dart';

class BarcodeSearchPage extends StatefulWidget {
  @override
  _BarcodeSearchPageState createState() => _BarcodeSearchPageState();
}

class _BarcodeSearchPageState extends State<BarcodeSearchPage> {
  HomeApi homeApi = HomeApi(httpClient: http.Client());
  CartBloc cartBloc;

  HomeBloc homeBloc;
  String code;
  Product product;

  @override
  void initState() {
    super.initState();
    homeBloc = HomeBloc(homeApi: homeApi);
  }

  @override
  Widget build(BuildContext context) {
    final sizeAware = MediaQuery.of(context).size;
    cartBloc = BlocProvider.of<CartBloc>(context);

    Map args = ModalRoute.of(context).settings.arguments;
    final String code = args['code'];
    homeBloc.add(BarcodeScan(code: code));
    return BlocListener(
      cubit: homeBloc,
      listener: (context, state) {
        if (state is BarcodeScanResult) {
          product = state.product;
          print("ss");
          cartBloc.add(
            AddToCartRequested(
              cartItem: CartItem(
                product: product,
                qty: 1,
                total_price: 1 * product.price,
              ),
            ),
          );

          Navigator.pushReplacementNamed(context, '/cart');
          Fluttertoast.showToast(
              msg: getTranslated(context, 'Item Added To Cart'));
        }
        if (state is BarcodeScanNotFound) {
          Fluttertoast.showToast(
              toastLength: Toast.LENGTH_LONG,
              msg: getTranslated(context, 'Gift Not Found Or Already Used !'));
          Navigator.pop(context);
        }
      },
      child: BlocBuilder(
        cubit: homeBloc,
        builder: (context, state) {
          if (state is HomeLoadInProgress || state is BarcodeScanNotFound) {
            return Loading();
          }
          if (state is HomeLoadFailure) {
            return Scaffold(
              body: SafeArea(
                child: Container(
                  width: sizeAware.width,
                  height: sizeAware.height,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        getTranslated(context, 'Connection Error'),
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(height: 20),
                      FlatButton(
                        color: Config.primaryColor,
                        onPressed: () {
                          setState(() {});
                        },
                        child: Text(
                          getTranslated(context, 'Refresh'),
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            );
          }
          if (state is BarcodeScanResult) {
            product = state.product;
            return Scaffold(
              appBar: myAppBar('Barcode', null),
              body: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        height: 350,
                        child: CachedNetworkImage(
                          imageUrl: product.images[0],
                          placeholder: (context, url) =>
                              Center(child: CircularProgressIndicator()),
                          errorWidget: (context, url, error) =>
                              Icon(Icons.error),
                          fit: BoxFit.fill,
//              color: Colors.grey[200],
                        ),
                      ),
                      SizedBox(
                        height: 40,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: Text(
                              LANGUAGE == 'en'
                                  ? product.name_en
                                  : product.name_ar,
                              style: TextStyle(
                                fontSize: 24,
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Html(
                          data: LANGUAGE == 'en'
                              ? product.overview_en
                              : product.overview_ar),
                      SizedBox(
                        height: 15,
                      ),
                    ],
                  ),
                ),
              ),
            );
          }
          return Container();
        },
      ),
    );
  }
}
