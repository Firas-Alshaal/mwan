import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dtic/blocs/auth/auth_event.dart';
import 'package:flutter_dtic/blocs/auth/auth_state.dart';
import 'package:flutter_dtic/services/AuthApi.dart';
import 'package:video_player/video_player.dart';
import 'package:http/http.dart' as http;
import '../constants.dart';
import 'package:flutter_dtic/blocs/auth/auth_bloc.dart';

class AssetVideo extends StatefulWidget {
  @override
  AssetVideoState createState() => AssetVideoState();
}

class AssetVideoState extends State<AssetVideo> {
  VideoPlayerController _controller;
  AuthBloc authBloc;

  @override
  void initState() {
    super.initState();
    authBloc = BlocProvider.of<AuthBloc>(context);
    authBloc.add(AuthRequested());
   _controller = VideoPlayerController.asset(
      'assets/intro1.mp4',
    );
    _controller.addListener(checkVideo);
    _controller.initialize().then((_) => setState(() {}));
    _controller.play();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final sizeAware = MediaQuery
        .of(context)
        .size;

    return SafeArea(
      child: Scaffold(
        body: Container(
          width: sizeAware.width,
          height: sizeAware.height,
          child: Image(image: AssetImage('assets/intro.png'),),
        ),
      ),
    );
  }

  void checkVideo() {
    if (_controller.value.position == _controller.value.duration) {
      Navigator.pushReplacementNamed(context, '/home');
    }
  }
}