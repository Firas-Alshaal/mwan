import 'package:flutter/material.dart';
import 'package:flutter_dtic/config.dart';
import 'package:flutter_dtic/constants.dart';
import 'package:flutter_dtic/localization/localization_constants.dart';
import 'package:flutter_dtic/models/event.dart';
import 'package:flutter_dtic/widgets/MyAppBar.dart';
import 'package:flutter_html/flutter_html.dart';

class EventDetailes extends StatefulWidget {
  @override
  _EventDetailesState createState() => _EventDetailesState();
}

class _EventDetailesState extends State<EventDetailes> {
  Event event;
  @override
  Widget build(BuildContext context) {
    final sizeAware = MediaQuery.of(context).size;
    final Map args = ModalRoute.of(context).settings.arguments;
    event = args['event'];
    return Scaffold(
      appBar: myAppBar(getTranslated(context, 'Event Details'), null),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: sizeAware.height * 0.4,
              width: sizeAware.width,
              child: Image.network(
                event.images[0],
                fit: BoxFit.fill,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    event.name,
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: Config.primaryColor,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(LANGUAGE == 'en'
                      ? 'From ${event.start_date} To ${event.end_date}'
                      : 'من ${event.start_date} الى ${event.end_date}'),
                  SizedBox(
                    height: 20,
                  ),
                  Html(data: event.description_en),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
