import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dtic/blocs/cart/cart_bloc.dart';
import 'package:flutter_dtic/blocs/cart/cart_event.dart';
import 'package:flutter_dtic/blocs/cart/cart_state.dart';
import 'package:flutter_dtic/blocs/product/product_bloc.dart';
import 'package:flutter_dtic/blocs/product/product_event.dart';
import 'package:flutter_dtic/blocs/product/product_state.dart';
import 'package:flutter_dtic/constants.dart';
import 'package:flutter_dtic/localization/localization_constants.dart';
import 'package:flutter_dtic/models/CartItem.dart';
import 'package:flutter_dtic/models/Product.dart';
import 'package:flutter_dtic/services/ProductApi.dart';
import 'package:flutter_dtic/widgets/FeaturedProductCard.dart';
import 'package:flutter_dtic/widgets/ImageCarusel.dart';
import 'package:flutter_dtic/widgets/MyAppBar.dart';
import 'package:flutter_dtic/widgets/MyBottomNavigation.dart';
import 'package:flutter_dtic/widgets/MyFloatingActionButton.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:hexcolor/hexcolor.dart';

import '../config.dart';

class ProductDetailes extends StatefulWidget {
  @override
  _ProductDetailesState createState() => _ProductDetailesState();
}

class _ProductDetailesState extends State<ProductDetailes> {
  Product product;
  List<String> images = [];
  List<Product> similar = [];
  int qty = 1;
  CartBloc cartBloc;
  ProductBloc productBloc;
  ProductApi productApi = ProductApi(httpClient: http.Client());
  int i = 0;
  @override
  void initState() {
    super.initState();
    productBloc = ProductBloc(productApi: productApi);
  }

  @override
  Widget build(BuildContext context) {
    final sizeAware = MediaQuery.of(context).size;
    cartBloc = BlocProvider.of<CartBloc>(context);

    Map args = ModalRoute.of(context).settings.arguments;
    product = args['product'];
    print(product.id);
    if (i == 0) {
      productBloc.add(SingleProductRequested(id: product.id));
      i++;
    }
    return BlocListener(
      cubit: cartBloc,
      listener: (context, state) {
        if (state is AddedToCart) {
          Fluttertoast.showToast(
              msg: getTranslated(context, 'Item Added To Cart'));
        }
        if (state is ProductsLoadInProgress) {}
      },
      child: BlocBuilder(
        cubit: productBloc,
        builder: (context, state) {
          if (state is ProductsLoadFailure) {
            return Scaffold(
              body: SafeArea(
                child: Container(
                  width: sizeAware.width,
                  height: sizeAware.height,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        getTranslated(context, 'Connection Error'),
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(height: 20),
                      FlatButton(
                        color: Config.primaryColor,
                        onPressed: () {
                          setState(() {
                            setState(() {
                              i = 0;
                            });
                          });
                        },
                        child: Text(
                          getTranslated(context, 'Refresh'),
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            );
          }
          if (state is ProductLoadSuccess || state is ProductsLoadInProgress) {
            if (state is ProductLoadSuccess) {
              product = state.product;
            }

            return Scaffold(
              appBar: myAppBar(
                  LANGUAGE == 'en' ? product.name_en : product.name_ar, null),
              bottomNavigationBar: MyBottomNavigation(),
              floatingActionButton: MyFloatingActionButton(),
              floatingActionButtonLocation:
                  FloatingActionButtonLocation.centerDocked,
              body: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        height: 350,
                        child: ImageCarusel(
                          images: product.images,
                          product_id: product.id,
                        ),
                      ),
                      SizedBox(
                        height: 40,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: Text(
                              LANGUAGE == 'en'
                                  ? product.name_en
                                  : product.name_ar,
                              style: TextStyle(
                                fontSize: 24,
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        '${product.price}',
                        style: TextStyle(
                          color: Colors.red,
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Html(data: product.overview_en),
                      SizedBox(
                        height: 5,
                      ),
                      // Html(data: product.specifications_en),
                      // SizedBox(
                      //   height: 15,
                      // ),
                      Center(
                        child: Container(
                          width: 120,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(25),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              GestureDetector(
                                onTap: () {
                                  if (qty > 1) {
                                    setState(() {
                                      qty--;
                                    });
                                  }
                                },
                                child: Container(
                                  width: 28,
                                  height: 28,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(100),
                                    color: Colors.red,
                                  ),
                                  child: Icon(
                                    Icons.remove,
                                    color: Colors.white,
                                    size: 28,
                                  ),
                                ),
                              ),
                              Expanded(
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      getTranslated(context, 'Quantity'),
                                    ),
                                    Text('$qty'),
                                  ],
                                ),
                              ),
                              GestureDetector(
                                onTap: () {
                                  setState(() {
                                    qty++;
                                  });
                                },
                                child: Container(
                                  width: 28,
                                  height: 28,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(100),
                                    color: Config.primaryColor,
                                  ),
                                  child: Icon(
                                    Icons.add,
                                    color: Colors.white,
                                    size: 28,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Center(
                        child: GestureDetector(
                          onTap: () {
                            cartBloc.add(
                              AddToCartRequested(
                                cartItem: CartItem(
                                  product: product,
                                  qty: qty,
                                  total_price: qty * product.price,
                                ),
                              ),
                            );
                          },
                          child: Container(
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                  begin: Alignment.topRight,
                                  end: Alignment.bottomLeft,
                                  colors: [HexColor('#F93E4F'), HexColor('#8D2CD3')]),
                              borderRadius: BorderRadius.circular(25),
                            ),
                            width: sizeAware.width * 0.9,
                            height: 50,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  Icons.shopping_cart,
                                  color: Colors.white,
                                ),
                                SizedBox(
                                  width: 15,
                                ),
                                Text(
                                  getTranslated(context, 'Add To Cart'),
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 17),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      product.similar.length != 0
                          ? Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  getTranslated(context, 'Similar Products'),
                                  style: TextStyle(fontSize: 18),
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                getSimilar(sizeAware),
                              ],
                            )
                          : Container(),
                      SizedBox(
                        height: 15,
                      ),
                    ],
                  ),
                ),
              ),
            );
          }
          return Container();
        },
      ),
    );
  }

  Widget getSimilar(sizeAware) {
    List<Widget> products = product.similar
        .map(
          (e) => FeaturedProductCard(
            product: e,
          ),
        )
        .toList();
    return Container(
      height: 250,
      width: sizeAware.width,
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          children: products,
        ),
      ),
    );
  }
}
