import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dtic/blocs/cart/cart_bloc.dart';
import 'package:flutter_dtic/blocs/cart/cart_event.dart';
import 'package:flutter_dtic/blocs/cart/cart_state.dart';
import 'package:flutter_dtic/config.dart';
import 'package:flutter_dtic/localization/localization_constants.dart';
import 'package:flutter_dtic/widgets/Loading.dart';
import 'package:flutter_dtic/widgets/MyAppBar.dart';
import 'package:flutter_dtic/widgets/StepsWidget.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hexcolor/hexcolor.dart';

class ConfirmOrderPage extends StatefulWidget {
  @override
  _ConfirmOrderPageState createState() => _ConfirmOrderPageState();
}

class _ConfirmOrderPageState extends State<ConfirmOrderPage> {
  TextEditingController code = TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    final sizeAware = MediaQuery.of(context).size;
    final cartBloc = BlocProvider.of<CartBloc>(context);

    Map args = ModalRoute.of(context).settings.arguments;
    final int id = args['id'];
    print(id);
    return BlocListener(
      cubit: cartBloc,
      listener: (context, state) {
        if (state is OrderVerified) {
          Navigator.popUntil(context, (route) => route.isFirst);
        }
        if (state is OrderFaield) {
          Fluttertoast.showToast(toastLength: Toast.LENGTH_LONG,msg: getTranslated(context, 'Wrong Code'));
        }
      },
      child: BlocBuilder(
        cubit: cartBloc,
        builder: (context, state) {
          if (state is CartLoadInProgress) {
            return Loading();
          }
          if (state is CartLoadFailure) {
            return Scaffold(
              body: SafeArea(
                child: Container(
                  width: sizeAware.width,
                  height: sizeAware.height,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        getTranslated(context, 'Connection Error'),
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(height: 20),
                      FlatButton(
                        color: Config.primaryColor,
                        onPressed: () {
                          setState(() {
                            cartBloc.add(CartRequested());
                          });
                        },
                        child: Text(
                          getTranslated(context, 'Refresh'),
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            );
          }
          if (state is OrderSuccess ||
              state is CartLoadSuccess ||
              state is OrderFaield ||
              state is OrderVerified) {
            return Scaffold(
              key: _scaffoldKey,
              appBar: myAppBar(getTranslated(context, 'Confirm Order'), null),
              body: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: sizeAware.height * 0.03,
                    ),
                    StepsWidget(step: 3),
                    SizedBox(
                      height: sizeAware.height * 0.03,
                    ),
                    Text(
                      getTranslated(context, 'Enter confirmation code'),
                      style: TextStyle(
                        fontSize: 20,
                        color: Config.primaryColor,
                      ),
                    ),
                    SizedBox(
                      height: sizeAware.height * 0.03,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: TextFormField(
                        controller: code,
                        decoration: InputDecoration(
                          labelText: getTranslated(context, 'Code'),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: sizeAware.height * 0.03,
                    ),
                    GestureDetector(
                      onTap: () {
                        if(code.text.isEmpty){
                          Fluttertoast.showToast(msg: 'please Enter Code',toastLength: Toast.LENGTH_LONG);
                        }
                        if (code.text != '') {
                          cartBloc.add(VerifyOrder(code: code.text, id: id));
                        }
                      },
                      child: Container(
                        width: sizeAware.width * 0.85,
                        height: 50.0,
                        decoration: BoxDecoration(
                          //color: Config.primaryColor,
                            gradient: LinearGradient(
                                begin: Alignment.topLeft,
                                end: Alignment.bottomRight,
                                colors: <Color>[ HexColor('#8D2CD3'),HexColor('#F93E4F'),]),
                          borderRadius: BorderRadius.horizontal(
                            left: Radius.circular(40),
                            right: Radius.circular(40),
                          ),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                getTranslated(context, 'Send'),
                                style: TextStyle(
                                  fontSize: 20,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          }
          return Container();
        },
      ),
    );
  }
}
