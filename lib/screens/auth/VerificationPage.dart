import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dtic/blocs/auth/auth_bloc.dart';
import 'package:flutter_dtic/blocs/auth/auth_event.dart';
import 'package:flutter_dtic/blocs/auth/auth_state.dart';
import 'package:flutter_dtic/constants.dart';
import 'package:flutter_dtic/localization/localization_constants.dart';
import 'package:flutter_dtic/services/AuthApi.dart';
import 'package:flutter_dtic/widgets/Loading.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;

import '../../config.dart';

class VerificationPage extends StatefulWidget {
  @override
  _VerificationPageState createState() => _VerificationPageState();
}

class _VerificationPageState extends State<VerificationPage> {
  TextEditingController code = TextEditingController();
  AuthBloc authBloc;
  AuthApi authApi = AuthApi(httpClient: http.Client());

  @override
  void initState() {
    super.initState();
    authBloc = BlocProvider.of<AuthBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    final sizeAware = MediaQuery.of(context).size;
    print(EMAIL);
    return BlocListener(
      cubit: authBloc,
      listener: (context, state) {
        if (state is Authenticated) {
          Navigator.pop(context);
        }
        if (state is NotAuthenticated) {
          Fluttertoast.showToast(toastLength: Toast.LENGTH_LONG,msg: getTranslated(context, "Wrong Code"));
        }
      },
      child: BlocBuilder(
        cubit: authBloc,
        builder: (context, state) {
          if (state is AuthLoadInProgress) {
            return Loading();
          }
          if (state is AuthLoadFailure) {
            return Scaffold(
              body: SafeArea(
                child: Container(
                  width: sizeAware.width,
                  height: sizeAware.height,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        getTranslated(context, 'Connection Error'),
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(height: 20),
                      FlatButton(
                        color: Config.primaryColor,
                        onPressed: () {
                          setState(() {
                            authBloc.add(AuthRequested());
                          });
                        },
                        child: Text(
                          getTranslated(context, 'Refresh'),
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            );
          }
          if (state is NotAuthenticated || state is Authenticated) {
            return SafeArea(
              child: Scaffold(
                body: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Column(
                      children: [
                        SizedBox(
                          height: sizeAware.height * 0.1,
                        ),
                        Text(
                         getTranslated(context, 'Enter Verification Code'),
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(
                          height: sizeAware.height * 0.1,
                        ),
                        TextFormField(
                          controller: code,
                          decoration: InputDecoration(
                            labelText: getTranslated(context,'Code'),
                          ),
                        ),
                        SizedBox(
                          height: sizeAware.height * 0.03,
                        ),
                        GestureDetector(
                          onTap: () {
                            if (code.text != '') {
                              authBloc.add(
                                SendCodeRequested(
                                  code: code.text,
                                  email: EMAIL,
                                ),
                              );
                            }
                          },
                          child: Container(
                            width: sizeAware.width * 0.85,
                            height: 50.0,
                            decoration: BoxDecoration(
                              color: Config.primaryColor,
                              borderRadius: BorderRadius.horizontal(
                                left: Radius.circular(40),
                                right: Radius.circular(40),
                              ),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    getTranslated(context, 'Send'),
                                    style: TextStyle(
                                      fontSize: 20,
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            );
          }
        },
      ),
    );
  }
}
