import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dtic/blocs/auth/auth_bloc.dart';
import 'package:flutter_dtic/blocs/auth/auth_event.dart';
import 'package:flutter_dtic/blocs/auth/auth_state.dart';
import 'package:flutter_dtic/config.dart';
import 'package:flutter_dtic/localization/localization_constants.dart';
import 'package:flutter_dtic/services/AuthApi.dart';
import 'package:flutter_dtic/widgets/Loading.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;

class ForgotPasswordPage extends StatefulWidget {
  @override
  _ForgotPasswordPageState createState() => _ForgotPasswordPageState();
}

class _ForgotPasswordPageState extends State<ForgotPasswordPage> {
  AuthBloc authBloc;

  AuthApi authApi = AuthApi(httpClient: http.Client());
  TextEditingController phoneTextEditingController = TextEditingController();
  String countryCode = "+971";

  @override
  void initState() {
    super.initState();
    authBloc = AuthBloc(authApi: authApi);
  }

  @override
  Widget build(BuildContext context) {
    final sizeAware = MediaQuery.of(context).size;
    return BlocListener(
      cubit: authBloc,
      listener: (context, state) {
        if (state is CodeSent) {
          Fluttertoast.showToast(toastLength: Toast.LENGTH_LONG,
              msg: getTranslated(context, 'new password sent to you'));
          Navigator.pop(context);
        }
      },
      child: BlocBuilder(
        cubit: authBloc,
        builder: (context, state) {
          if (state is AuthInitial ||
              state is NotAuthenticated ||
              state is Authenticated ||
              state is CodeSent ||
              state is CodeNotSent) {
            return SafeArea(
              child: Scaffold(
                backgroundColor: Colors.white,
                body: SingleChildScrollView(
                  child: Container(
                    width: sizeAware.width,
                    child: Column(
                      children: [
                        SizedBox(
                          height: sizeAware.height * 0.1,
                        ),
                        Text(
                          getTranslated(context, 'Forgot your password'),
                          style: TextStyle(
                            fontSize: 22,
                            fontWeight: FontWeight.bold,
                            color: Config.primaryColor,
                          ),
                        ),
                        SizedBox(
                          height: sizeAware.height * 0.1,
                        ),
                        Text(
                          getTranslated(context, 'Enter your phone number'),
                        ),
                        SizedBox(
                          height: sizeAware.height * 0.01,
                        ),
                        Container(
                          width: sizeAware.width * 0.8,
                          child: Row(
                            children: [
                              CountryCodePicker(
                                onChanged: (val) {
                                  setState(() {
                                    countryCode = val.dialCode;
                                  });
                                },
                                // Initial selection and favorite can be one of code ('IT') OR dial_code('+39')
                                initialSelection: 'AE',
                                favorite: ['+971', 'AE'],
                                // optional. Shows only country name and flag
                                showCountryOnly: false,
                                // optional. Shows only country name and flag when popup is closed.
                                showOnlyCountryWhenClosed: false,
                                // optional. aligns the flag and the Text left
                                alignLeft: false,
                              ),
                              Expanded(
                                child: TextFormField(
                                  controller: phoneTextEditingController,
                                  decoration: InputDecoration(
                                    labelText:
                                        getTranslated(context, 'Mobile number'),
                                  ),
                                  validator: (String arg) {
                                    if (arg.length <= 0)
                                      return 'Mobile number is required';
                                    else
                                      return null;
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: sizeAware.height * 0.05,
                        ),
                        GestureDetector(
                          onTap: () {
                            if (phoneTextEditingController.text
                                .startsWith('0')) {
                              phoneTextEditingController.text =
                                  phoneTextEditingController.text.substring(1);
                            }
                            if (phoneTextEditingController.text.trim() != '') {
                              authBloc.add(
                                ResetPassword(
                                  phone: countryCode +
                                      phoneTextEditingController.text,
                                ),
                              );
                            }
                          },
                          child: Container(
                            width: sizeAware.width * 0.85,
                            height: 50.0,
                            decoration: BoxDecoration(
                              color: Config.primaryColor,
                              borderRadius: BorderRadius.horizontal(
                                left: Radius.circular(40),
                                right: Radius.circular(40),
                              ),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    getTranslated(context, 'Send'),
                                    style: TextStyle(
                                      fontSize: 20,
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            );
          }
          if (state is AuthLoadFailure) {
            return Scaffold(
              body: SafeArea(
                child: Container(
                  width: sizeAware.width,
                  height: sizeAware.height,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        getTranslated(context, 'Connection Error'),
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(height: 20),
                      FlatButton(
                        color: Config.primaryColor,
                        onPressed: () {
                          setState(() {
                            authBloc.add(AuthRequested());
                          });
                        },
                        child: Text(
                          'Refresh',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            );
          }
          if (state is AuthLoadInProgress) {
            return Loading();
          }
          return Container();
        },
      ),
    );
  }
}
