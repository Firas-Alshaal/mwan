import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dtic/blocs/auth/auth_bloc.dart';
import 'package:flutter_dtic/blocs/auth/auth_event.dart';
import 'package:flutter_dtic/blocs/auth/auth_state.dart';
import 'package:flutter_dtic/blocs/cart/cart_bloc.dart';
import 'package:flutter_dtic/blocs/cart/cart_event.dart';
import 'package:flutter_dtic/blocs/cart/cart_state.dart';
import 'package:flutter_dtic/config.dart';
import 'package:flutter_dtic/localization/localization_constants.dart';
import 'package:flutter_dtic/models/City.dart';
import 'package:flutter_dtic/services/AuthApi.dart';
import 'package:flutter_dtic/widgets/Loading.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:http/http.dart' as http;
import 'package:hexcolor/hexcolor.dart';

import 'VerificationPage.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  bool isVis = true;
  AuthBloc authBloc;
  AuthApi authApi = AuthApi(httpClient: http.Client());
  TextEditingController emailTextEditingController = TextEditingController();
  TextEditingController firstNameTextEditingController =
      TextEditingController();
  TextEditingController lastNameTextEditingController = TextEditingController();

  TextEditingController numberTextEditingController = TextEditingController();
  TextEditingController address = TextEditingController();
  TextEditingController passwordTextEditingController = TextEditingController();
  TextEditingController confirmPasswordTextEditingController =
      TextEditingController();
  TextEditingController city = TextEditingController();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  List<City> cities = [];
  CartBloc cartBloc;

  String countryCode = "+971";

  @override
  void initState() {
    super.initState();
    authBloc = BlocProvider.of<AuthBloc>(context);
    cartBloc = BlocProvider.of<CartBloc>(context);
    cartBloc.add(GetCitiesRequested());
  }

  @override
  Widget build(BuildContext context) {
    final sizeAware = MediaQuery.of(context).size;
    return BlocListener(
      cubit: authBloc,
      listener: (context, state) {
        if (state is NotAuthenticated) {
          if (state.verified) {
            Navigator.pushReplacementNamed(context, '/verification');
          } else {
            Fluttertoast.showToast(
                toastLength: Toast.LENGTH_LONG, msg: state.msg);
          }
        }
      },
      child: BlocBuilder(
        cubit: cartBloc,
        builder: (context, state) {
          if (state is CitiesLoadSuccess) {
            cities = state.cities;
          }
          return Scaffold(
            appBar: AppBar(
                flexibleSpace: Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                      colors: <Color>[
                    HexColor('#8D2CD3'),
                    HexColor('#F93E4F'),
                  ])),
            )),
            backgroundColor: Colors.white,
            body: BlocBuilder(
              cubit: authBloc,
              builder: (context, state) {
                if (state is AuthInitial ||
                    state is Authenticated ||
                    state is NotAuthenticated) {
                  return SingleChildScrollView(
                    child: Container(
                      width: sizeAware.width,
                      padding: EdgeInsets.symmetric(horizontal: 24),
                      child: Form(
                        key: _formKey,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: sizeAware.height * 0.03,
                            ),
                            Text(
                              getTranslated(context, 'Register'),
                              style: TextStyle(
                                  color: Config.primaryColor,
                                  fontSize: 30,
                                  fontWeight: FontWeight.bold),
                            ),
                            SizedBox(
                              height: sizeAware.height * 0.02,
                            ),
                            TextFormField(
                              controller: firstNameTextEditingController,
                              decoration: InputDecoration(
                                labelText: getTranslated(context, 'First Name'),
                              ),
                              validator: (String arg) {
                                if (arg.length <= 0)
                                  return getTranslated(
                                      context, 'First Name is required');
                                else
                                  return null;
                              },
                            ),
                            TextFormField(
                              controller: lastNameTextEditingController,
                              decoration: InputDecoration(
                                labelText: getTranslated(context, 'Last Name'),
                              ),
                              validator: (String arg) {
                                if (arg.length <= 0)
                                  return getTranslated(
                                      context, 'Last Name is required');
                                else
                                  return null;
                              },
                            ),
                            TextFormField(
                              controller: emailTextEditingController,
                              keyboardType: TextInputType.emailAddress,
                              decoration: InputDecoration(
                                labelText: getTranslated(context, 'Email'),
                              ),
                              validator: (String arg) {
                                if (arg.length <= 0)
                                  return getTranslated(
                                      context, 'Email is required');
                                else
                                  return null;
                              },
                            ),
                            TextFormField(
                              controller: numberTextEditingController,
                              keyboardType: TextInputType.number,
                              decoration: InputDecoration(
                                  prefixIcon: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 8),
                                    child: Container(
                                      width: 40,
                                      height: 40,
                                      child: SvgPicture.asset(
                                        'assets/saudi-arabia.svg',
                                        width: 18.0,
                                        height: 18.0,
                                      ),
                                    ),
                                  ),
                                  labelText:
                                      getTranslated(context, 'Mobile number'),
                                  hintText: "+966"),
                              validator: (String arg) {
                                if (arg.length <= 0)
                                  return getTranslated(
                                      context, 'Mobile number is required');
                                else
                                  return null;
                              },
                            ),
                            // Container(
                            //   width: sizeAware.width * 0.8,
                            //   child: Row(
                            //     textDirection: TextDirection.ltr,
                            //     children: [
                            //       CountryCodePicker(
                            //         onChanged: (val) {
                            //           setState(() {
                            //             countryCode = val.dialCode;
                            //           });
                            //         },
                            //         // Initial selection and favorite can add one of code ('IT') OR dial_code('+39')
                            //         initialSelection: 'SA',
                            //         favorite: ['+966', 'SA'],
                            //         // optional. Shows only country name and flag
                            //         showCountryOnly: false,
                            //         // optional. Shows only country name and flag when popup is closed.
                            //         showOnlyCountryWhenClosed: false,
                            //         // optional. aligns the flag and the Text left
                            //         alignLeft: false,
                            //       ),
                            //       Expanded(
                            //         child: TextFormField(
                            //           textDirection: TextDirection.ltr,
                            //           controller: numberTextEditingController,
                            //           decoration: InputDecoration(
                            //             labelText: getTranslated(
                            //                 context, 'Mobile number'),
                            //           ),
                            //           validator: (String arg) {
                            //             if (arg.length <= 0)
                            //               return getTranslated(context,
                            //                   'Mobile number is required');
                            //             else
                            //               return null;
                            //           },
                            //         ),
                            //       ),
                            //     ],
                            //   ),
                            // ),
                            SizedBox(
                              height: 20,
                            ),
                            Container(
                              clipBehavior: Clip.antiAlias,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(2),
                                border: Border.all(),
                                color: Colors.white,
                              ),
                              child: Center(
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 8,
                                  ),
                                  child: DropdownButton(
                                    value: city.text == ''
                                        ? null
                                        : int.parse(city.text),
                                    hint: Text(
                                      getTranslated(context, 'City'),
                                    ),
                                    underline: SizedBox(),
                                    isExpanded: true,
                                    onChanged: (val) {
                                      setState(() {
                                        city.text = val.toString();
                                      });
                                      print(city.text);
                                    },
                                    items: cities
                                        .map(
                                          (e) => DropdownMenuItem<int>(
                                            value: e.id,
                                            child: Text(
                                              e.name_en,
                                              style: TextStyle(),
                                            ),
                                          ),
                                        )
                                        .toList(),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: sizeAware.height * 0.03,
                            ),
                            TextFormField(
                              controller: address,
                              decoration: InputDecoration(
                                labelText: getTranslated(context, 'Address'),
                              ),
                              validator: (String arg) {
                                if (arg.length <= 0)
                                  return getTranslated(
                                      context, 'Address is required');
                                else
                                  return null;
                              },
                            ),
                            SizedBox(
                              height: sizeAware.height * 0.02,
                            ),
                            TextFormField(
                              obscureText: isVis,
                              controller: passwordTextEditingController,
                              validator: (String arg) {
                                if (arg.length <= 0)
                                  return getTranslated(
                                      context, 'Password is required');
                                else
                                  return null;
                              },
                              decoration: InputDecoration(
                                labelText: getTranslated(context, 'Password'),
                                suffixIcon: IconButton(
                                  icon: isVis
                                      ? Icon(Icons.visibility_off)
                                      : Icon(Icons.remove_red_eye),
                                  onPressed: () {
                                    setState(() {
                                      isVis = !isVis;
                                    });
                                  },
                                ),
                              ),
                            ),
                            TextFormField(
                              obscureText: isVis,
                              controller: confirmPasswordTextEditingController,
                              validator: (String arg) {
                                if (arg.length <= 0)
                                  return getTranslated(context,
                                      'Password Confirmation is required');
                                if (arg != passwordTextEditingController.text)
                                  return getTranslated(context,
                                      'Password confirmation does not match');
                                else
                                  return null;
                              },
                              decoration: InputDecoration(
                                labelText:
                                    getTranslated(context, 'Confirm Password'),
                                suffixIcon: IconButton(
                                  icon: isVis
                                      ? Icon(Icons.visibility_off)
                                      : Icon(Icons.remove_red_eye),
                                  onPressed: () {
                                    setState(() {
                                      isVis = !isVis;
                                    });
                                  },
                                ),
                              ),
                            ),
                            SizedBox(
                              height: sizeAware.height * 0.03,
                            ),
                            Container(
                              width: sizeAware.width,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  GestureDetector(
                                    onTap: () {
                                      if (city.text == '') {
                                        Fluttertoast.showToast(
                                            toastLength: Toast.LENGTH_LONG,
                                            msg: getTranslated(
                                                context, 'Please Select City'));
                                        // return;
                                      }
                                      if (_formKey.currentState.validate() &&
                                          city.text != '') {
                                        if (numberTextEditingController.text
                                            .startsWith('0')) {
                                          numberTextEditingController.text =
                                              numberTextEditingController.text
                                                  .substring(1);
                                        }
                                        authBloc.add(RegisterRequested(
                                          email:
                                              emailTextEditingController.text,
                                          password:
                                              passwordTextEditingController
                                                  .text,
                                          fname: firstNameTextEditingController
                                              .text,
                                          lname: lastNameTextEditingController
                                              .text,
                                          phone: '+966' +
                                              numberTextEditingController.text,
                                          city_id: city.text,
                                          addresses: [address.text],
                                        ));
                                      }
                                    },
                                    child: Container(
                                      width: sizeAware.width * 0.85,
                                      height: 50.0,
                                      decoration: BoxDecoration(
                                        gradient: LinearGradient(
                                            begin: Alignment.topRight,
                                            end: Alignment.bottomLeft,
                                            colors: [
                                              HexColor('#F93E4F'),
                                              HexColor('#8D2CD3')
                                            ]),
                                        borderRadius: BorderRadius.horizontal(
                                          left: Radius.circular(40),
                                          right: Radius.circular(40),
                                        ),
                                      ),
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              getTranslated(
                                                  context, 'Register'),
                                              style: TextStyle(
                                                fontSize: 20,
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: sizeAware.height * 0.03,
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      Navigator.pushReplacementNamed(
                                          context, '/login');
                                    },
                                    child: Container(
                                      width: sizeAware.width,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Text(
                                            getTranslated(context,
                                                'Already have an account?'),
                                            style: TextStyle(
                                              fontSize: 17,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.grey[600],
                                            ),
                                          ),
                                          Text(
                                            getTranslated(context, 'Login'),
                                            style: TextStyle(
                                              fontSize: 17,
                                              color: Color.fromRGBO(
                                                  53, 9, 100, 1.0),
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: sizeAware.height * 0.1,
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                }
                if (state is AuthLoadInProgress) {
                  return Loading();
                }
                if (state is AuthLoadFailure) {
                  return Scaffold(
                    body: SafeArea(
                      child: Container(
                        width: sizeAware.width,
                        height: sizeAware.height,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              getTranslated(context, 'Connection Error'),
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                            SizedBox(height: 20),
                            FlatButton(
                              color: Config.primaryColor,
                              onPressed: () {
                                setState(() {
                                  authBloc.add(AuthRequested());
                                });
                              },
                              child: Text(
                                getTranslated(context, 'Refresh'),
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  );
                }
                return Container();
              },
            ),
          );
        },
      ),
    );
  }
}
