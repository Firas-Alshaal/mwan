import 'package:auth_buttons/res/buttons/facebook_auth_button.dart';
import 'package:auth_buttons/res/shared/auth_style.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dtic/blocs/auth/auth_bloc.dart';
import 'package:flutter_dtic/blocs/auth/auth_event.dart';
import 'package:flutter_dtic/blocs/auth/auth_state.dart';
import 'package:flutter_dtic/config.dart';
import 'package:flutter_dtic/localization/localization_constants.dart';
import 'package:flutter_dtic/services/AuthApi.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:http/http.dart' as http;
import 'package:auth_buttons/auth_buttons.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import '../../constants.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  AuthBloc authBloc;
  bool isVis = true;
  bool darkMode = false;
  AuthButtonStyle authButtonStyle = AuthButtonStyle.secondary;
  AuthApi authApi = AuthApi(httpClient: http.Client());
  TextEditingController emailTextEditingController = TextEditingController();
  TextEditingController passwordTextEditingController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool isLogin = false;

  //dynamic _userData;
  UserCredential userCredential;
  GoogleSignIn _googleSignIn = GoogleSignIn();
  FirebaseAuth _auth = FirebaseAuth.instance;

  // facebook login
  Map<String, dynamic> _userData;
  AccessToken _accessToken;
  bool _checking = true;

  Future<String> googleSignIn() async {
    GoogleSignInAccount googleSignInAccount = await _googleSignIn.signIn();
    GoogleSignInAuthentication googleSignInAuthentication =
        await googleSignInAccount.authentication;
    AuthCredential authCredential = GoogleAuthProvider.credential(
        accessToken: googleSignInAuthentication.accessToken,
        idToken: googleSignInAuthentication.idToken);
    UserCredential userCredential =
        await _auth.signInWithCredential(authCredential);
    User user = userCredential.user;
    assert(user.displayName != null);
    assert(user.email != null);
    User currentUser = _auth.currentUser;
    if (currentUser.email != null) {
      authBloc.add(RegisterByGoogle(
          access_token: googleSignInAuthentication.accessToken,
          social_provider: 'google'));
    }
    assert(currentUser.uid == user.uid);
    return ("Error");
  }

  Future<void> _checkIfIsLogged() async {
    final AccessToken accessToken = await FacebookAuth.instance.isLogged;
    setState(() {
      _checking = false;
    });
    if (accessToken != null) {
      final userData = await FacebookAuth.instance.getUserData();
      _accessToken = accessToken;
      setState(() {
        _userData = userData;
      });
    }
  }

  Future<void> _logOut() async {
    await FacebookAuth.instance.logOut();
    _accessToken = null;
    _userData = null;
    setState(() {});
  }

  Future<void> _login() async {
    try {
      setState(() {
        _checking = true;
      });
      _accessToken = await FacebookAuth.instance.login();
      if (_accessToken != null) {
        authBloc.add(RegisterByFaceBook(
            access_token: _accessToken.token, social_provider: 'facebook'));
      }
      final facebookAuthCredential =
          FacebookAuthProvider.credential(_accessToken.token);
      return await FirebaseAuth.instance
          .signInWithCredential(facebookAuthCredential);
    } on FacebookAuthException catch (e) {
      print(e.message);

      print(_accessToken);
      switch (e.errorCode) {
        case FacebookAuthErrorCode.OPERATION_IN_PROGRESS:
          print("You have a previous login operation in progress");
          break;
        case FacebookAuthErrorCode.CANCELLED:
          print("login cancelled");
          break;
        case FacebookAuthErrorCode.FAILED:
          print("login failed");
          break;
      }
    } catch (e, s) {
      print(e);
      print(s);
    } finally {
      setState(() {
        _checking = false;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    authBloc = BlocProvider.of<AuthBloc>(context);
    _checkIfIsLogged();
  }

  @override
  Widget build(BuildContext context) {
    final sizeAware = MediaQuery.of(context).size;
    return BlocListener(
      cubit: authBloc,
      listener: (context, state) {
        if (state is Authenticated) {

          if (ResgisterOk) {
            Navigator.of(context)
                .pushNamedAndRemoveUntil('/profile', (Route<dynamic> route) => false);          }
          else
            {
              Navigator.pop(context);
            }
        }
        if (state is NotAuthenticated) {
          if (state.verified) {
            Navigator.pushReplacementNamed(context, '/verification');
          } else {
            Fluttertoast.showToast(toastLength: Toast.LENGTH_LONG,
                msg: getTranslated(context, "Wrong Email or Password"));
          }
        }
        if (state is NoAuthenticated) {
          Fluttertoast.showToast(toastLength: Toast.LENGTH_LONG,msg: getTranslated(context, "Try Aagin"));
        }
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          flexibleSpace: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: <Color>[ HexColor('#8D2CD3'),HexColor('#F93E4F'),])),
          )
        ),
        body: BlocBuilder(
          cubit: authBloc,
          builder: (context, state) {
            if (state is AuthInitial ||
                state is NotAuthenticated ||
                state is Authenticated ||
                state is NoAuthenticated) {
              return SingleChildScrollView(
                child: Form(
                  key: _formKey,
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: sizeAware.height*0.15,),
                        Text(
                          getTranslated(context, 'Welcome'),
                          style: TextStyle(
                              color: Config.primaryColor,
                              fontSize: 30,
                              fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          height: sizeAware.height * 0.01,
                        ),
                        Text(
                          getTranslated(context, 'Sign in to continue'),
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          height: sizeAware.height * 0.01,
                        ),
                        TextFormField(
                          controller: emailTextEditingController,
                          decoration: InputDecoration(
                            labelText: getTranslated(context, 'Email'),
                          ),
                          validator: (String arg) {
                            if (arg.length <= 0)
                              return getTranslated(
                                  context, 'Email is required');
                            else
                              return null;
                          },
                        ),
                        TextFormField(
                          controller: passwordTextEditingController,
                          obscureText: isVis,
                          validator: (String arg) {
                            if (arg.length <= 0)
                              return getTranslated(
                                  context, 'Password is required');
                            else
                              return null;
                          },
                          decoration: InputDecoration(
                            labelText: getTranslated(context, 'Password'),
                            suffixIcon: IconButton(
                              icon: isVis ? Icon(Icons.visibility_off) :Icon(Icons.remove_red_eye),
                              onPressed: () {
                                setState(() {
                                  isVis = !isVis;
                                });
                              },
                            ),
                          ),
                        ),
                        SizedBox(
                          height: sizeAware.height * 0.03,
                        ),
                        Container(
                          width: sizeAware.width,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              GestureDetector(
                                onTap: () {
                                  if (_formKey.currentState.validate()) {
                                    authBloc.add(
                                      LoginRequested(
                                        email: emailTextEditingController.text
                                            .trim(),
                                        password: passwordTextEditingController
                                            .text
                                            .trim(),
                                      ),
                                    );
                                  }
                                },
                                child: Container(
                                  width: sizeAware.width * 0.85,
                                  height: 50.0,
                                  decoration: BoxDecoration(
                                    gradient: LinearGradient(
                                        begin: Alignment.topRight,
                                        end: Alignment.bottomLeft,
                                        colors: [HexColor('#F93E4F'), HexColor('#8D2CD3')]),
                                    borderRadius: BorderRadius.horizontal(
                                      left: Radius.circular(40),
                                      right: Radius.circular(40),
                                    ),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          getTranslated(context, 'Login'),
                                          style: TextStyle(
                                            fontSize: 20,
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              // facebook and google
                              /*Container(
                                width: sizeAware.width * 0.85,
                                height: 50.0,
                                child: FacebookAuthButton(
                                  onPressed:
                                      _userData != null ? _logOut : _login,
                                  borderRadius: 40,
                                ),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Container(
                                width: sizeAware.width * 0.85,
                                height: 50.0,
                                child: GoogleAuthButton(
                                  onPressed: () async {
                                    googleSignIn();
                                  },
                                  borderRadius: 40,
                                ),
                              ),*/
                              SizedBox(
                                height: sizeAware.height * 0.02,
                              ),
                              // Text(
                              //   getTranslated(context,'or'),
                              //   style: TextStyle(
                              //     fontSize: 18,
                              //     fontWeight: FontWeight.bold,
                              //     color: Colors.grey[600],
                              //   ),
                              // ),
                              // SizedBox(
                              //   height: sizeAware.height * 0.02,
                              // ),
                              // Container(
                              //   width: sizeAware.width * 0.85,
                              //   height: 50.0,
                              //   decoration: BoxDecoration(
                              //     border: Border.all(),
                              //     borderRadius: BorderRadius.horizontal(
                              //       left: Radius.circular(40),
                              //       right: Radius.circular(40),
                              //     ),
                              //   ),
                              //   child: Padding(
                              //     padding: const EdgeInsets.all(8.0),
                              //     child: Row(
                              //       mainAxisAlignment: MainAxisAlignment.center,
                              //       children: [
                              //         Text(
                              //          getTranslated(context, 'Login with Facebook'),
                              //           style: TextStyle(
                              //             fontSize: 17,
                              //             color: Colors.grey[600],
                              //             fontWeight: FontWeight.bold,
                              //           ),
                              //         ),
                              //       ],
                              //     ),
                              //   ),
                              // ),
                              // SizedBox(
                              //   height: sizeAware.height * 0.03,
                              // ),
                              GestureDetector(
                                onTap: () {
                                  Navigator.pushNamed(
                                      context, '/forgot password');
                                },
                                child: Text(
                                  getTranslated(
                                      context, 'Forgot your password'),
                                  style: TextStyle(
                                    fontSize: 17,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.grey[600],
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: sizeAware.height * 0.02,
                              ),
                              GestureDetector(
                                onTap: () {
                                  Navigator.pushReplacementNamed(
                                      context, '/register');
                                },
                                child: Container(
                                  width: sizeAware.width,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        getTranslated(
                                            context, 'Dont have an account?'),
                                        style: TextStyle(
                                          fontSize: 17,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.grey[600],
                                        ),
                                      ),
                                      Text(
                                        getTranslated(context, 'Sign Up'),
                                        style: TextStyle(
                                          fontSize: 17,
                                          color: Config.primaryColor,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        SizedBox(
                          height: sizeAware.height * 0.2,
                        ),
                      ],
                    ),
                  ),
                ),
              );
            }
            if (state is AuthLoadFailure) {
              return Scaffold(
                body: SafeArea(
                  child: Container(
                    width: sizeAware.width,
                    height: sizeAware.height,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          getTranslated(context, 'Connection Error'),
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold),
                        ),
                        SizedBox(height: 20),
                        FlatButton(
                          color: Config.primaryColor,
                          onPressed: () {
                            setState(() {
                              authBloc.add(AuthRequested());
                            });
                          },
                          child: Text(
                            getTranslated(context, 'Refresh'),
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            }
            if (state is AuthLoadInProgress) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
            return Container();
          },
        ),
      ),
    );
  }
}
