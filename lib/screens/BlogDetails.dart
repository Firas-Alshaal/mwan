import 'package:flutter/material.dart';
import 'package:flutter_dtic/config.dart';
import 'package:flutter_dtic/localization/localization_constants.dart';
import 'package:flutter_dtic/models/Blog.dart';
import 'package:flutter_dtic/widgets/MyAppBar.dart';
import 'package:flutter_html/flutter_html.dart';

class BlogDetailes extends StatefulWidget {
  @override
  _BlogDetailesState createState() => _BlogDetailesState();
}

class _BlogDetailesState extends State<BlogDetailes> {
  @override
  Widget build(BuildContext context) {
    final sizeAware = MediaQuery.of(context).size;
    Map args = ModalRoute.of(context).settings.arguments;
    final Blog blog = args['blog'];

    return Scaffold(
      appBar: myAppBar(getTranslated(context, 'Blog Details'), null),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: sizeAware.height * 0.4,
              width: sizeAware.width,
              child: Image.network(
                blog.image_path,
                fit: BoxFit.fill,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    blog.article_title_en,
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: Config.primaryColor,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Html(data: blog.article_body_en),
                  SizedBox(
                    height: 20,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
