import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dtic/blocs/home/home_bloc.dart';
import 'package:flutter_dtic/blocs/home/home_event.dart';
import 'package:flutter_dtic/blocs/home/home_state.dart';
import 'package:flutter_dtic/localization/localization_constants.dart';
import 'package:flutter_dtic/services/HomeApi.dart';
import 'package:flutter_dtic/widgets/Loading.dart';
import 'package:flutter_dtic/widgets/MyAppBar.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:hexcolor/hexcolor.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:url_launcher/url_launcher.dart';

import '../config.dart';

class ContactUs extends StatefulWidget {
  @override
  _ContactUsState createState() => _ContactUsState();
}

class _ContactUsState extends State<ContactUs> {
  TextEditingController email = TextEditingController();
  TextEditingController name = TextEditingController();
  TextEditingController subject = TextEditingController();
  TextEditingController msg = TextEditingController();
  var formKey = GlobalKey<FormState>();

  HomeApi homeApi = HomeApi(httpClient: http.Client());
  HomeBloc homeBloc;

  @override
  void initState() {
    super.initState();
    homeBloc = HomeBloc(homeApi: homeApi);
  }

  @override
  Widget build(BuildContext context) {
    final sizeAware = MediaQuery.of(context).size;
    return BlocListener(
      cubit: homeBloc,
      listener: (context, state) {
        if (state is ContactUsSuccess) {
          Fluttertoast.showToast(
              toastLength: Toast.LENGTH_LONG,
              msg: getTranslated(context, 'Message sent successfuly'));
          Navigator.pop(context);
        }
      },
      child: Scaffold(
        appBar: myAppBar(getTranslated(context, 'ContactUs'), null),
        body: BlocBuilder(
          cubit: homeBloc,
          builder: (context, state) {
            if (state is HomeInitial || state is ContactUsSuccess) {
              return SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Form(
                    key: formKey,
                    child: Column(
                      children: [
                        SizedBox(
                          height: 10,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 4,
                          ),
                          child: Container(
                            clipBehavior: Clip.antiAlias,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(25),
                              border: Border.all(color: Colors.grey[400]),
                              color: Colors.white,
                            ),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 12,
                              ),
                              child: Center(
                                child: TextFormField(
                                  controller: name,
                                  keyboardType: TextInputType.name,
                                  validator: (String value) {
                                    if (value.isEmpty) {
                                      return 'Input User Name';
                                    }
                                    return '';
                                  },
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText:
                                        getTranslated(context, 'Full Name'),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 4,
                          ),
                          child: Container(
                            clipBehavior: Clip.antiAlias,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(25),
                              border: Border.all(color: Colors.grey[400]),
                              color: Colors.white,
                            ),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 12,
                              ),
                              child: Center(
                                child: TextFormField(
                                  keyboardType: TextInputType.emailAddress,
                                  controller: email,
                                  validator: (String value) {
                                    if (value.isEmpty) {
                                      return 'Input your Email';
                                    }
                                    return '';
                                  },
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: getTranslated(context, 'Email'),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 4,
                          ),
                          child: Container(
                            clipBehavior: Clip.antiAlias,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(25),
                              border: Border.all(color: Colors.grey[400]),
                              color: Colors.white,
                            ),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 12,
                              ),
                              child: Center(
                                child: TextFormField(
                                  controller: subject,
                                  keyboardType: TextInputType.text,
                                  validator: (String value) {
                                    if (value.isEmpty) {
                                      return 'Input your subject';
                                    }
                                    return '';
                                  },
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: getTranslated(context, 'Subject'),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 4,
                          ),
                          child: Container(
                            clipBehavior: Clip.antiAlias,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(25),
                              border: Border.all(color: Colors.grey[400]),
                              color: Colors.white,
                            ),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 12,
                              ),
                              child: Center(
                                child: TextFormField(
                                  controller: msg,
                                  maxLines: 5,
                                  keyboardType: TextInputType.multiline,
                                  validator: (String value) {
                                    if (value.isEmpty) {
                                      return 'Input your Message';
                                    }
                                    return '';
                                  },
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: getTranslated(context, 'Message'),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        GestureDetector(
                          onTap: () {
                            if (email.text.trim() != '' &&
                                name.text.trim() != '' &&
                                msg.text.trim() != '' &&
                                subject.text.trim() != '') {
                              homeBloc.add(ContactRequested(
                                email: email.text,
                                msg: msg.text,
                                name: name.text,
                                subject: subject.text,
                              ));
                            } else {
                              Fluttertoast.showToast(
                                msg: getTranslated(context, 'Contacte'),
                                toastLength: Toast.LENGTH_LONG,
                                backgroundColor: Colors.red,
                              );
                            }
                          },
                          child: Container(
                            width: sizeAware.width * 0.85,
                            height: 50.0,
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                  begin: Alignment.bottomLeft,
                                  end: Alignment.topRight,
                                  colors: [
                                    HexColor('#8D2CD3'),
                                    HexColor('#F93E4F')
                                  ]),
                              borderRadius: BorderRadius.horizontal(
                                left: Radius.circular(40),
                                right: Radius.circular(40),
                              ),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    getTranslated(context, 'Send'),
                                    style: TextStyle(
                                      fontSize: 20,
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        GestureDetector(
                          onTap: () {
                            _launchURL();
                          },
                          child: Container(
                            width: sizeAware.width * 0.85,
                            height: 50.0,
                            decoration: BoxDecoration(
                              color: Colors.green,
                              borderRadius: BorderRadius.horizontal(
                                left: Radius.circular(40),
                                right: Radius.circular(40),
                              ),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  SvgPicture.asset('assets/whatsapp2.svg',
                                      width: sizeAware.width * 0.08,
                                      color: Colors.white),
                                ],
                              ),
                            ),
                          ),
                        ),
                        // Padding(
                        //   padding: const EdgeInsets.all(8.0),
                        //   child: GestureDetector(
                        //     onTap: () {
                        //       _launchURL();
                        //     },
                        //     child: Container(
                        //       child: Align(
                        //         alignment: Alignment.topLeft,
                        //         child: Row(
                        //           mainAxisAlignment: MainAxisAlignment.center,
                        //           children: [
                        //             SizedBox(
                        //               width: 10,
                        //             ),
                        //             SvgPicture.asset(
                        //               'assets/whatsapp2.svg',
                        //               width: sizeAware.width * 0.08,
                        //             ),
                        //             SizedBox(
                        //               width: 20,
                        //             ),
                        //             Text(
                        //               "+9668003040500",
                        //               style: TextStyle(
                        //                   fontSize: 15,
                        //                   fontWeight: FontWeight.bold,
                        //                   color: Colors.black26),
                        //             )
                        //           ],
                        //         ),
                        //       ),
                        //     ),
                        //   ),
                        // )
                      ],
                    ),
                  ),
                ),
              );
            }
            if (state is HomeLoadInProgress) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
            if (state is HomeLoadFailure) {
              return Scaffold(
                body: SafeArea(
                  child: Container(
                    width: sizeAware.width,
                    height: sizeAware.height,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          getTranslated(context, 'Connection Error'),
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold),
                        ),
                        SizedBox(height: 20),
                        FlatButton(
                          color: Config.primaryColor,
                          onPressed: () {
                            setState(() {
                              homeBloc = HomeBloc(homeApi: homeApi);
                            });
                          },
                          child: Text(
                            getTranslated(context, 'Refresh'),
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              );
            }
            return Container();
          },
        ),
      ),
    );
  }

  var _url = "https://api.whatsapp.com/send?phone=+9668003040500";
  void _launchURL() async => await canLaunch(_url)
      ? await launch(_url)
      : throw 'Could not launch $_url';
}
