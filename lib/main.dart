import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dtic/routes.dart';
import 'package:flutter_dtic/services/AuthApi.dart';
import 'package:flutter_dtic/services/CartApi.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'blocs/auth/auth_bloc.dart';
import 'blocs/cart/cart_bloc.dart';
import 'blocs/simple_bloc_observer.dart';
import 'package:http/http.dart' as http;

import 'localization/localization.dart';
import 'localization/localization_constants.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  Bloc.observer = SimpleBlocObserver();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  static void setLocale(BuildContext context, Locale locale) {
    _MyAppState state = context.findAncestorStateOfType<_MyAppState>();
    state.setLocale(locale);
  }

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  CartApi cartApi = CartApi(httpClient: http.Client());
  AuthApi authApi = AuthApi(httpClient: http.Client());

  Locale _locale;

  void setLocale(locale) {
    setState(() {
      _locale = locale;
    });
  }

  @override
  void didChangeDependencies() {
    getLocale().then((locale) {
      setState(() {
        this._locale = locale;
      });
    });
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    if (_locale == null) {
      return Container();
    } else {
      return MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (context) => CartBloc(cartApi: cartApi),
          ),
          BlocProvider(
            create: (context) => AuthBloc(authApi: authApi),
          ),
        ],
        child: MaterialApp(
          title: 'MWAN',
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            primarySwatch: Colors.pink,
            visualDensity: VisualDensity.adaptivePlatformDensity,
          ),
          routes: Routes.getAll(),
          initialRoute: '/splash',
          locale: _locale,
          localizationsDelegates: [
            Localization.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
          ],
          localeResolutionCallback: (deviceLocale, supportedLocales) {
            for (var locale in supportedLocales) {
              if (locale.languageCode == deviceLocale.languageCode &&
                  locale.countryCode == deviceLocale.countryCode) {
                return deviceLocale;
              }
            }
            return supportedLocales.first;
          },
          supportedLocales: [
            const Locale('en', 'US'),
            const Locale('ar', 'SA'),
          ],
        ),
      );
    }
  }
}
