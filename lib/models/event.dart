import 'package:flutter_dtic/constants.dart';

class Event {
  final int id;
  final String start_date;
  final String end_date;
  final String description_en;
  final String description_ar;
  final String name;
  final List images;

  Event({
    this.id,
    this.start_date,
    this.end_date,
    this.description_en,
    this.description_ar,
    this.images,
    this.name,
  });

  static Event fromJson(json) {
    List images = json['event_images'].map((e) => e['path']).toList();
    return Event(
      id: json['id'],
      start_date: json['start_date'],
      end_date: json['end_date'],
      description_en: json['description:$LANGUAGE'],
      description_ar: json['description:ar'],
      name: json['name:$LANGUAGE'],
      images: images,
    );
  }
}
