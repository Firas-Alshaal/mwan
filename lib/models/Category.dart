import 'package:flutter_dtic/constants.dart';

class Category {
  int id;
  String name_en;
  String name_ar;
  String image;
  String image2;
  List subCategories;

  Category(
      {this.id,
      this.name_en,
      this.name_ar,
      this.image,
      this.image2,
      this.subCategories});

  static Category fromJson(json) {
    List sub = json['sub_categories'] != null
        ? json['sub_categories'].map((e) => Category.fromJson(e)).toList()
        : [];

    return Category(
      id: json['id'],
      name_ar: json['name:ar'],
      name_en: json['name:$LANGUAGE'],
      image: json['image_path'],
      image2: json['image_path2'],
      subCategories: sub,
    );
  }
}
