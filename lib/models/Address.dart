class Address {
  final int id;
  final String address_text;

  Address({this.id, this.address_text});

  static Address fromJson(json) {
    Address b = Address(
      id: json['id'],
      address_text: json['address_text'],
    );
    return b;
  }
}
