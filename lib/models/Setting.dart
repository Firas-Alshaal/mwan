import 'package:flutter_dtic/constants.dart';

class Setting {
  final int id;
  final String key;
  final String value;
  final String icon;

  Setting({this.id, this.key, this.value, this.icon});

  static Setting fromJson(json) {
    return Setting(
      id: json['id'],
      key: json['key:$LANGUAGE'],
      value: json['value:$LANGUAGE'],
      icon: json['icon'],
    );
  }
}
