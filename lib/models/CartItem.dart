import 'Product.dart';

class CartItem {
  Product product;
  int qty;
  double total_price;

  CartItem({this.product, this.qty, this.total_price});

  static CartItem fromJson(dynamic json) {
    return CartItem(
      product: Product.fromMyJson(json['product']),
      qty: json['qty'],
      total_price: json['total_price'],
    );
  }

  static Map<String, dynamic> toMapSend(CartItem c) {
    var d = {
      'product_id': c.product.id,
      'qty': c.qty,
    };

    return d;
  }

  static List<Map> encodeCartItemsSend(List<CartItem> items) => items
      .map<Map<String, dynamic>>((item) => CartItem.toMapSend(item))
      .toList();

  static Map<String, dynamic> toMap(CartItem c) {
    var d = {
      'product': Product.toMap(c.product),
      'qty': c.qty,
      'total_price': c.total_price,
    };

    return d;
  }

  static List<Map> encodeCartItems(List<CartItem> items) =>
      items.map<Map<String, dynamic>>((item) => CartItem.toMap(item)).toList();
}
