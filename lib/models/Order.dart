class Order {
  final int id;
  final int status;
  final String requested_delivery_date;
  final String order_no;
  final String total;

  Order({
    this.id,
    this.status,
    this.requested_delivery_date,
    this.order_no,
    this.total,
  });

  static Order fromMyJson(json) {
    return Order(
      id: json['id'],
      status: json['status'],
      requested_delivery_date: json['requested_delivery_date'],
      order_no: json['order_no'],
      total: json['total'].toString(),
    );
  }

  static Order fromJson(json) {
    return Order(
      status: json['order_status'],
      total: json['total'],
    );
  }
}
