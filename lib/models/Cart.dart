import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

import 'CartItem.dart';

class Cart {
  static Future<List<CartItem>> getItems() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();

    if (!localStorage.containsKey('cart')) {
      return [];
    }

    var cart = jsonDecode(localStorage.get('cart')) as List;

    List<CartItem> items = cart.map((e) => CartItem.fromJson(e)).toList();

    return items;
  }

  static Future<int> getItemsNumber() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    if (!localStorage.containsKey('cart')) {
      return 0;
    }

    var cart = jsonDecode(localStorage.get('cart')) as List;

    List<CartItem> items = cart.map((e) => CartItem.fromJson(e)).toList();

    return items.length;
  }

  static Future emptyCart() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    await localStorage.remove('cart');
  }

  static Future<List<CartItem>> addItem(CartItem item) async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    if (!localStorage.containsKey('cart')) {
      localStorage.setString('cart', jsonEncode([]));
    }

    var cart = jsonDecode(localStorage.get('cart')) as List;

    List<CartItem> products = cart.map((e) => CartItem.fromJson(e)).toList();
    bool t = true;
    for (int i = 0; i < products.length; i++) {
      if (products[i].product.id == item.product.id) {
        products[i].qty += item.qty;
        t = false;
      }
    }
    if (t) products.add(item);

    List<Map> list = CartItem.encodeCartItems(products);

    localStorage.setString('cart', jsonEncode(list));
    return products;
  }

  static Future<List<CartItem>> deleteItem(id) async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    if (!localStorage.containsKey('cart')) {
      localStorage.setString('cart', jsonEncode([]));
    }

    var mds = jsonDecode(localStorage.get('cart')) as List;

    List<CartItem> products = mds.map((e) => CartItem.fromJson(e)).toList();

    for (int i = 0; i < products.length; i++) {
      if (products[i].product.id == id) {
        products.removeAt(i);
      }
    }

    List<Map> list = CartItem.encodeCartItems(products);
    localStorage.setString('cart', jsonEncode(list));
    return products;
  }

  static Future<List<CartItem>> increase(id) async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    if (!localStorage.containsKey('cart')) {
      return [];
    }
    var cart = jsonDecode(localStorage.get('cart')) as List;
    List<CartItem> products = cart.map((e) => CartItem.fromJson(e)).toList();
    for (int i = 0; i < products.length; i++) {
      if (products[i].product.id == id) {
        products[i].qty++;
        products[i].total_price = products[i].qty * products[i].product.price;
        break;
      }
    }
    List<Map> list = CartItem.encodeCartItems(products);
    localStorage.setString('cart', jsonEncode(list));
    return products;
  }

  static Future<List<CartItem>> decrease(id) async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    if (!localStorage.containsKey('cart')) {
      return [];
    }
    var cart = jsonDecode(localStorage.get('cart')) as List;
    List<CartItem> products = cart.map((e) => CartItem.fromJson(e)).toList();
    for (int i = 0; i < products.length; i++) {
      if (products[i].product.id == id) {
        if (products[i].qty > 1) products[i].qty--;
        products[i].total_price = products[i].qty * products[i].product.price;
        break;
      }
    }
    List<Map> list = CartItem.encodeCartItems(products);
    localStorage.setString('cart', jsonEncode(list));
    return products;
  }
}
