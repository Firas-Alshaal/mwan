import '../constants.dart';

class Product {
  int id;
  String category_id;
  String brand_id;
  double price;
  String overview_ar;
  String overview_en;
  String specifications_ar;
  String specifications_en;
  String name_ar;
  String name_en;
  List images;
  List similar;

  Product({
    this.id,
    this.name_ar,
    this.name_en,
    this.overview_ar,
    this.overview_en,
    this.price,
    this.specifications_ar,
    this.specifications_en,
    this.images,
    this.similar,
  });

  static Product fromJson(json) {
    List images = json['product_images'].map((e) => e['image_path']).toList();
    List similar = json['similar_products'] != null
        ? json['similar_products'].map((e) => Product.fromJson(e)).toList()
        : [];
    return Product(
      id: json['id'],
      price: double.parse(json['price'].toString()),
      overview_ar: json['overview:ar'],
      overview_en: json['overview:$LANGUAGE'],
      specifications_ar: json['specifications:ar'],
      specifications_en: json['specifications:$LANGUAGE'],
      name_ar: json['name:ar'],
      name_en: json['name:$LANGUAGE'],
      images: images,
      similar: similar,
    );
  }

  static Product fromMyJson(json) {
    List images = json['images'].map((e) => e).toList();
    return Product(
      id: json['id'],
      price: json['price'] ?? 0,
      overview_ar: json['overview:ar'],
      overview_en: json['overview:$LANGUAGE'],
      specifications_ar: json['specifications:ar'],
      specifications_en: json['specifications:$LANGUAGE'],
      name_ar: json['name:ar'],
      name_en: json['name:en'],
      images: images,
    );
  }

  static Map<String, dynamic> toMap(Product c) {
    var d = {
      'id': c.id,
      'name:en': c.name_en,
      'name:ar': c.name_ar,
      'overview:ar': c.overview_ar,
      'overview:en': c.overview_en,
      'price': c.price,
      'specifications:ar': c.specifications_ar,
      'specifications:en': c.specifications_en,
      'images': c.images,
    };

    return d;
  }

  static List<Map> encodeProducts(List<Product> items) =>
      items.map<Map<String, dynamic>>((item) => Product.toMap(item)).toList();
}
