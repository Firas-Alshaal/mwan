class User {
  final int id;
  final String email;
  final String first_name;
  final String last_name;
  final String phone;
  final int city_id;
  final bool missingParams;
  User(
      {this.id,
      this.email,
      this.first_name,
      this.last_name,
      this.phone,
      this.city_id,
      this.missingParams});

  static User fromJson(json) {
    return User(
        id: json['id'],
        email: json['email'],
        first_name: json['first_name'],
        last_name: json['last_name'],
        phone: json['phone'],
        city_id: json['city_id'],
        missingParams: json['missing_params']);
  }
}
