class Job {
  final int id;
  final String image;
  final String description_en;
  final String description_ar;
  final String title_en;
  final String title_ar;

  Job({
    this.id,
    this.description_en,
    this.description_ar,
    this.title_en,
    this.title_ar,
    this.image,
  });

  static Job fromJson(json) {
    return Job(
      id: json['id'],
      description_en: json['description:en'],
      description_ar: json['description:ar'],
      title_ar: json['title:ar'],
      title_en: json['title:en'],
      image: json['image_path'],
    );
  }
}
