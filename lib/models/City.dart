import '../constants.dart';

class City {
  final int id;
  final String name_ar;
  final String name_en;

  City({this.id, this.name_ar, this.name_en});

  static City fromJson(json) {
    return City(
      id: json['id'],
      name_en: json['name:$LANGUAGE'],
      name_ar: json['name:ar'],
    );
  }
}
