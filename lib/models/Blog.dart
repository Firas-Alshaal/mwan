import '../constants.dart';

class Blog {
  final int id;
  final String image_path;
  final String article_title_ar;
  final String article_title_en;
  final String article_body_ar;
  final String article_body_en;

  Blog({
    this.id,
    this.image_path,
    this.article_title_ar,
    this.article_title_en,
    this.article_body_ar,
    this.article_body_en,
  });

  static Blog fromJson(json) {
    Blog b = Blog(
      id: json['id'],
      image_path: json['image_path'],
      article_title_ar: json['article_title:ar'],
      article_title_en: json['article_title:$LANGUAGE'],
      article_body_ar: json['article_body:ar'],
      article_body_en: json['article_body:$LANGUAGE'],
    );
    return b;
  }
}
