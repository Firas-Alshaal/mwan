class Brand {
  final int id;
  final String name;
  final String logo;

  Brand({this.id, this.name, this.logo});

  static Brand fromJson(json) {
    Brand b = Brand(
      id: json['id'],
      name: json['name'],
      logo: json['logo'],
    );
    return b;
  }
}
