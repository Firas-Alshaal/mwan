import 'package:flutter_dtic/models/Setting.dart';

import '../constants.dart';

class Branche {
  final int id;
  final String coordinates;
  final String region;
  final List settings;

  Branche({this.id, this.coordinates, this.region, this.settings});

  static Branche fromJson(json) {
    List settings = json['settings'].map((e) => Setting.fromJson(e)).toList();
    return Branche(
      id: json['id'],
      coordinates: json['coordinates'],
      region: json['region:$LANGUAGE'],
      settings: settings,
    );
  }
}
