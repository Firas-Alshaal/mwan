class Gift {
  int id;
  String image;
  String nameEn;
  String descriptionEn;
  String nameAr;
  String descriptionAr;

  Gift(
      {this.id,
      this.image,
      this.nameEn,
      this.descriptionEn,
      this.nameAr,
      this.descriptionAr});

  Gift.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    image = json['image'];
    nameEn = json['name:en'];
    descriptionEn = json['description:en'];
    nameAr = json['name:ar'];
    descriptionAr = json['description:ar'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['image'] = this.image;
    data['name:en'] = this.nameEn;
    data['description:en'] = this.descriptionEn;
    data['name:ar'] = this.nameAr;
    data['description:ar'] = this.descriptionAr;
    return data;
  }
}
