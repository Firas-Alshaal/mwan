import 'dart:convert';

import 'package:flutter_dtic/models/Cart.dart';
import 'package:flutter_dtic/models/CartItem.dart';
import 'package:flutter_dtic/models/Product.dart';
import 'package:flutter_dtic/services/AuthApi.dart';
import 'package:flutter_dtic/services/CartApi.dart';
import 'package:flutter_dtic/services/HomeApi.dart';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../constants.dart';
import 'auth_event.dart';
import 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final AuthApi authApi;

  AuthBloc({@required this.authApi})
      : assert(authApi != null),
        super(AuthInitial());

  @override
  Stream<AuthState> mapEventToState(AuthEvent event) async* {
    if (event is AuthRequested) {
      yield AuthLoadInProgress();
      try {
        await authApi.geturl();
        SharedPreferences sharedPreferences =
            await SharedPreferences.getInstance();
        bool isAuth = false;
        bool isPhoneRequired = false;
        if (sharedPreferences.containsKey('isAuth') &&
            sharedPreferences.containsKey('token')) {
          isAuth = await sharedPreferences.getBool('isAuth');
          ID = jsonDecode(await sharedPreferences.getString('user'))['id'];
          NAME = jsonDecode(
              await sharedPreferences.getString('user'))['first_name'];
          TOKEN = await sharedPreferences.getString('token');
          ISAuth = isAuth;
//          ResgisterOk = isPhoneRequired;
        }

        if (isAuth) {
          yield Authenticated();
        } else {
          yield NotAuthenticated();
        }
      } catch (_) {
        yield AuthLoadFailure();
      }
    } else if (event is LoginRequested) {
      yield AuthLoadInProgress();
      try {
        var data = {
          'email': event.email,
          'password': event.password,
        };
        int ans = await authApi.login(data);
        if (ans == 1) {
          yield Authenticated();
        } else if (ans == 0) {
          yield NotAuthenticated(verified: false);
        } else {
          yield NotAuthenticated(verified: true);
        }
      } catch (_) {
        yield AuthLoadFailure();
      }
    } else if (event is RegisterRequested) {
      yield AuthLoadInProgress();
      try {
        var data = {
          'email': event.email,
          'password': event.password,
          'phone': event.phone,
          'first_name': event.fname,
          'last_name': event.lname,
          'city_id': event.city_id,
          'addresses': event.addresses,
        };
        Map ans = await authApi.register(data);
        if (ans['status']) {
          yield NotAuthenticated(verified: true);
        } else {
          yield NotAuthenticated(verified: false, msg: ans['msg']);
        }
      } catch (_) {
        yield AuthLoadFailure();
      }
    } else if (event is RegisterByGoogle) {
      yield AuthLoadInProgress();
      try {
        var data = {
          'access-token': event.access_token,
          'social-provider': event.social_provider
        };
        Map ans = await authApi.googleRegister(data);
        if (ans['status']) {
          yield Authenticated();
        } else {
          yield NoAuthenticated();
        }
      } catch (_) {
        yield AuthLoadFailure();
      }
    } else if (event is RegisterByFaceBook) {
      yield AuthLoadInProgress();
      try {
        var data = {
          'access-token': event.access_token,
          'social-provider': event.social_provider
        };
        Map ans = await authApi.facebookRegister(data);
        if (ans['status']) {
          yield Authenticated();
        } else {
          yield NoAuthenticated();
        }
      } catch (_) {
        yield AuthLoadFailure();
      }
    } else if (event is LogoutRequested) {
      yield AuthLoadInProgress();
      try {
        SharedPreferences sharedPreferences =
            await SharedPreferences.getInstance();
        sharedPreferences.remove('token');
        sharedPreferences.setBool('isAuth', false);
        sharedPreferences.remove('user');
        bool ans = false;
        ISAuth = false;

        if (ans) {
          yield Authenticated();
        } else {
          yield NotAuthenticated();
        }
      } catch (_) {
        yield AuthLoadFailure();
      }
    } else if (event is ResetPassword) {
      yield AuthLoadInProgress();
      try {
        var data = {
          'phone': event.phone,
        };
        bool ans = await authApi.resetPassword(data);

        if (ans) {
          yield CodeSent();
        } else {
          yield CodeNotSent();
        }
      } catch (_) {
        yield AuthLoadFailure();
      }
    } else if (event is SendCodeRequested) {
      yield AuthLoadInProgress();
      try {
        var data = {
          'otp': event.code,
          'email': event.email,
        };
        print(data);
        bool ans = await authApi.sendCode(data);

        if (ans) {
          yield Authenticated();
        } else {
          yield NotAuthenticated();
        }
      } catch (_) {
        yield AuthLoadFailure();
      }
    }
  }
}
