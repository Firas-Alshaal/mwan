import 'package:flutter_dtic/models/CartItem.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

abstract class AuthEvent extends Equatable {
  const AuthEvent();
}

class AuthRequested extends AuthEvent {
  const AuthRequested();

  @override
  List<Object> get props => [];
}

class LoginRequested extends AuthEvent {
  final String email;
  final String password;
  const LoginRequested({this.email, this.password});

  @override
  List<Object> get props => [];
}

class SendCodeRequested extends AuthEvent {
  final String code;
  final String email;
  const SendCodeRequested({
    this.code,
    this.email,
  });

  @override
  List<Object> get props => [];
}

class RegisterRequested extends AuthEvent {
  final String email;
  final String password;
  final String fname;
  final String lname;
  final String phone;
  final String city_id;
  final List<String> addresses;
  const RegisterRequested(
      {this.email,
      this.password,
      this.fname,
      this.lname,
      this.phone,
      this.addresses,
      this.city_id});

  @override
  List<Object> get props => [];
}

class RegisterByGoogle extends AuthEvent {
  final String access_token;
  final String social_provider;
  const RegisterByGoogle({this.access_token, this.social_provider});
  @override
  List<Object> get props => [];
}

class RegisterByFaceBook extends AuthEvent {
  final String access_token;
  final String social_provider;
  const RegisterByFaceBook({this.access_token, this.social_provider});
  @override
  List<Object> get props => [];
}

class ResetPassword extends AuthEvent {
  final String phone;
  const ResetPassword({this.phone});

  @override
  List<Object> get props => [];
}

class LogoutRequested extends AuthEvent {
  @override
  List<Object> get props => [];
}
