import 'package:flutter_dtic/models/CartItem.dart';
import 'package:flutter_dtic/models/Product.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

abstract class AuthState extends Equatable {
  const AuthState();

  @override
  List<Object> get props => [];
}

class AuthInitial extends AuthState {}

class AuthLoadInProgress extends AuthState {}

class Authenticated extends AuthState {}

class NoAuthenticated extends AuthState {}

class CodeSent extends AuthState {}

class CodeNotSent extends AuthState {}

class NotAuthenticated extends AuthState {
  final bool verified;
  final String msg;

  NotAuthenticated({this.verified, this.msg});
}

class AuthLoadFailure extends AuthState {}
