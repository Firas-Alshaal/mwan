import 'package:flutter_dtic/models/CartItem.dart';
import 'package:flutter_dtic/models/City.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

abstract class CartEvent extends Equatable {
  const CartEvent();
}

class CartRequested extends CartEvent {
  const CartRequested();

  @override
  List<Object> get props => [];
}

class CartPageRequested extends CartEvent {
  const CartPageRequested();

  @override
  List<Object> get props => [];
}

class AddToCartRequested extends CartEvent {
  final CartItem cartItem;
  const AddToCartRequested({this.cartItem});

  @override
  List<Object> get props => [];
}

class DeleteItem extends CartEvent {
  final CartItem cartItem;
  const DeleteItem({this.cartItem});

  @override
  List<Object> get props => [];
}

class Increase extends CartEvent {
  final CartItem cartItem;
  const Increase({this.cartItem});

  @override
  List<Object> get props => [];
}

class Decrease extends CartEvent {
  final CartItem cartItem;
  const Decrease({this.cartItem});

  @override
  List<Object> get props => [];
}

class CheckoutRequested extends CartEvent {
  final List<CartItem> cartItems;
  final String firstName;
  final String lastName;
  final String address;
  final String deliveryTime;
  final String phone;
  final String city;

  const CheckoutRequested({
    this.cartItems,
    this.firstName,
    this.lastName,
    this.address,
    this.phone,
    this.deliveryTime,
    this.city,
  });

  @override
  List<Object> get props => [];
}

class AuthenticatedCheckoutRequested extends CartEvent {
  final List<CartItem> cartItems;
  final String address;
  final String deliveryTime;
  final String city;

  const AuthenticatedCheckoutRequested({
    this.cartItems,
    this.address,
    this.deliveryTime,
    this.city,
  });

  @override
  List<Object> get props => [];
}

class StoreAddressRequested extends CartEvent {
  final String address;

  const StoreAddressRequested({
    this.address,
  });

  @override
  List<Object> get props => [];
}

class DeliveryRequested extends CartEvent {
  final List<CartItem> cartItems;
  final List<City> cities;
  const DeliveryRequested({this.cartItems, this.cities});

  @override
  List<Object> get props => [];
}

class GetCitiesRequested extends CartEvent {
 
  @override
  List<Object> get props => [];
}

class CheckCoupon extends CartEvent {
  final String coupon;
  const CheckCoupon({
    this.coupon,
  });

  @override
  List<Object> get props => [];
}

class VerifyOrder extends CartEvent {
  final int id;
  final String code;

  const VerifyOrder({
    this.code,
    this.id,
  });

  @override
  List<Object> get props => [];
}
