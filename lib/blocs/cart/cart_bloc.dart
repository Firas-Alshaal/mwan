import 'package:flutter_dtic/models/Address.dart';
import 'package:flutter_dtic/models/Cart.dart';
import 'package:flutter_dtic/models/CartItem.dart';
import 'package:flutter_dtic/models/City.dart';
import 'package:flutter_dtic/models/Product.dart';
import 'package:flutter_dtic/services/CartApi.dart';
import 'package:flutter_dtic/services/HomeApi.dart';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';

import 'cart_event.dart';
import 'cart_state.dart';

class CartBloc extends Bloc<CartEvent, CartState> {
  final CartApi cartApi;

  CartBloc({@required this.cartApi})
      : assert(cartApi != null),
        super(CartInitial());

  @override
  Stream<CartState> mapEventToState(CartEvent event) async* {
    if (event is CartRequested) {
      yield CartLoadInProgress();
      try {
        List<CartItem> items = await Cart.getItems();

        yield CartLoadSuccess(items: items);
      } catch (_) {
        yield CartLoadFailure();
      }
    } else if (event is CartPageRequested) {
      yield CartLoadInProgress();
      // try {
      List<CartItem> items = await Cart.getItems();
      String limit = await cartApi.getLimit();

      yield CartLoadSuccess(items: items, limit: limit);
      // } catch (_) {
      //   yield CartLoadFailure();
      // }
    } else if (event is DeliveryRequested) {
      // yield CartLoadInProgress();
      try {
        List<CartItem> items = await Cart.getItems();
        List<City> cities = await cartApi.getCities();
        List<Address> addresses = await cartApi.getAddresses();

        yield DeliveryLoadSuccess(
            items: items, cities: cities, addresses: addresses);
      } catch (_) {
        yield CartLoadFailure();
      }
    } else if (event is GetCitiesRequested) {
      // yield CartLoadInProgress();
      try {
        List<City> cities = await cartApi.getCities();

        yield CitiesLoadSuccess(cities: cities);
      } catch (_) {
        yield CartLoadFailure();
      }
    } else if (event is AddToCartRequested) {
      yield CartLoadInProgress();
      try {
        List<CartItem> items = await Cart.addItem(event.cartItem);

        yield AddedToCart(items: items);
      } catch (_) {
        yield CartLoadFailure();
      }
    } else if (event is Increase) {
      yield CartLoadInProgress();
      try {
        List<CartItem> items = await Cart.increase(event.cartItem.product.id);

        yield CartLoadSuccess(items: items);
      } catch (_) {
        yield CartLoadFailure();
      }
    } else if (event is Decrease) {
      yield CartLoadInProgress();
      try {
        List<CartItem> items = await Cart.decrease(event.cartItem.product.id);

        yield CartLoadSuccess(items: items);
      } catch (_) {
        yield CartLoadFailure();
      }
    } else if (event is DeleteItem) {
      yield CartLoadInProgress();
      try {
        print('here');
        print(event.cartItem.product.id);
        List<CartItem> items = await Cart.deleteItem(event.cartItem.product.id);

        yield CartLoadSuccess(items: items);
      } catch (_) {
        yield CartLoadFailure();
      }
    } else if (event is CheckoutRequested) {
      yield CartLoadInProgress();
      try {
        var data = {
          'product': CartItem.encodeCartItemsSend(event.cartItems),
          'city_id': event.city,
          'address': event.address,
          'requested_delivery_date': event.deliveryTime,
          'first_name': event.firstName,
          'last_name': event.lastName,
          'phone': event.phone
        };
        Map t = await cartApi.checkout(data);
        if (t['status'] != -1) {
          Cart.emptyCart();
          yield OrderSuccess(id: t['id'], status: t['status']);
        } else {
          yield OrderFaield();
        }
      } catch (_) {
        yield CartLoadFailure();
      }
    } else if (event is AuthenticatedCheckoutRequested) {
      yield CartLoadInProgress();
      try {
        var data = {
          'product': CartItem.encodeCartItemsSend(event.cartItems),
          'city_id': event.city,
          'address': event.address,
          'requested_delivery_date': event.deliveryTime,
        };
        Map t = await cartApi.checkout(data);
        if (t['status'] != -1) {
          Cart.emptyCart();
          yield OrderSuccess(id: t['id'], status: t['status']);
        } else {
          yield OrderFaield(msg: t['msg']);
        }
      } catch (_) {
        yield CartLoadFailure();
      }
    } else if (event is CheckCoupon) {
      yield CartLoadInProgress();
      try {
        var data = {
          'coupon': event.coupon,
        };
        String res = await cartApi.checkCoupon(data);
        if (res != "") {
          yield CodeValid(str: res);
        } else {
          yield CodeInvalid(str: res);
        }
      } catch (_) {
        yield CartLoadFailure();
      }
    } else if (event is VerifyOrder) {
      yield CartLoadInProgress();
      try {
        var data = {
          'otp': event.code,
          'order_id': event.id,
        };
        bool res = await cartApi.verifyOrder(data, event.id);
        if (res) {
          yield OrderVerified();
        } else {
          yield OrderFaield();
        }
      } catch (_) {
        yield CartLoadFailure();
      }
    } else if (event is StoreAddressRequested) {
      yield CartLoadInProgress();
      try {
        var data = {
          'address_text': event.address,
        };
        Address address = await cartApi.storeAddress(data);
        List<Address> addresses = await cartApi.getAddresses();

        yield StoreAddressSuccess(addresses: addresses);
      } catch (_) {
        yield CartLoadFailure();
      }
    }
  }
}
