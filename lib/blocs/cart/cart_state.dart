import 'package:flutter_dtic/models/Address.dart';
import 'package:flutter_dtic/models/CartItem.dart';
import 'package:flutter_dtic/models/City.dart';
import 'package:flutter_dtic/models/Product.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

abstract class CartState extends Equatable {
  const CartState();

  @override
  List<Object> get props => [];
}

class CartInitial extends CartState {}

class CartLoadInProgress extends CartState {}

class OrderSuccess extends CartState {
  final int id;
  final int status;

  const OrderSuccess({
    @required this.id,
    @required this.status,
  }) : assert(id != null);

  @override
  List<Object> get props => [id];
}

class CodeValid extends CartState {
  final String str;

  const CodeValid({
    @required this.str,
  }) : assert(str != null);

  @override
  List<Object> get props => [str];
}

class CodeInvalid extends CartState {
  final String str;

  const CodeInvalid({
    @required this.str,
  }) : assert(str != null);

  @override
  List<Object> get props => [str];
}

class OrderFaield extends CartState {
  final String msg;

  OrderFaield({this.msg});
}

class AddedToCart extends CartState {
  final List<CartItem> items;

  const AddedToCart({
    @required this.items,
  }) : assert(items != null);

  @override
  List<Object> get props => [items];
}

class CartLoadSuccess extends CartState {
  final List<CartItem> items;
  final String limit;

  const CartLoadSuccess({
    @required this.items,
    this.limit,
  }) : assert(items != null);

  @override
  List<Object> get props => [items, limit];
}

class OrderVerified extends CartState {
  const OrderVerified();
}

class DeliveryLoadSuccess extends CartState {
  final List<CartItem> items;
  final List<City> cities;
  final List<Address> addresses;

  const DeliveryLoadSuccess({
    @required this.items,
    @required this.cities,
    @required this.addresses,
  }) : assert(items != null);

  @override
  List<Object> get props => [items, cities];
}

class CitiesLoadSuccess extends CartState {
  final List<City> cities;

  const CitiesLoadSuccess({
    @required this.cities,
  }) : assert(cities != null);

  @override
  List<Object> get props => [cities];
}

class StoreAddressSuccess extends CartState {
  final List<Address> addresses;

  const StoreAddressSuccess({
    @required this.addresses,
  }) : assert(addresses != null);

  @override
  List<Object> get props => [addresses];
}

class CartLoadFailure extends CartState {}
