import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

abstract class HomeEvent extends Equatable {
  const HomeEvent();
}

class HomeProductsRequested extends HomeEvent {
  final int page;

  const HomeProductsRequested({this.page});

  @override
  List<Object> get props => [];
}

class HomeGetMoreProductsRequested extends HomeEvent {
  final int page;
  const HomeGetMoreProductsRequested({this.page});

  @override
  List<Object> get props => [];
}

class HomeEventsRequested extends HomeEvent {
  const HomeEventsRequested();

  @override
  List<Object> get props => [];
}

class HomeCaruselRequested extends HomeEvent {
  const HomeCaruselRequested();

  @override
  List<Object> get props => [];
}

class RefreshHomeRequested extends HomeEvent {
  final int page;
  const RefreshHomeRequested({this.page});

  @override
  List<Object> get props => [];
}

class EventsRequested extends HomeEvent {
  const EventsRequested();

  @override
  List<Object> get props => [];
}

class BlogsRequested extends HomeEvent {
  const BlogsRequested();

  @override
  List<Object> get props => [];
}

class AboutUsRequested extends HomeEvent {
  const AboutUsRequested();

  @override
  List<Object> get props => [];
}

class ContactUsRequested extends HomeEvent {
  const ContactUsRequested();

  @override
  List<Object> get props => [];
}

class CheckOrder extends HomeEvent {
  final String id;
  const CheckOrder({this.id});

  @override
  List<Object> get props => [];
}

class BarcodeScan extends HomeEvent {
  final String code;
  const BarcodeScan({this.code});

  @override
  List<Object> get props => [];
}

class ContactRequested extends HomeEvent {
  final String name;
  final String email;
  final String subject;
  final String msg;
  const ContactRequested({this.name, this.email, this.subject, this.msg});

  @override
  List<Object> get props => [];
}
