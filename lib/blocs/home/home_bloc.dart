import 'package:flutter_dtic/models/Blog.dart';
import 'package:flutter_dtic/models/Branche.dart';
import 'package:flutter_dtic/models/Brand.dart';
import 'package:flutter_dtic/models/Gift.dart';
import 'package:flutter_dtic/models/Order.dart';
import 'package:flutter_dtic/models/Product.dart';
import 'package:flutter_dtic/models/event.dart';
import 'package:flutter_dtic/services/HomeApi.dart';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';

import 'home_event.dart';
import 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final HomeApi homeApi;
  List<Product> prods = [];
  HomeBloc({@required this.homeApi})
      : assert(homeApi != null),
        super(HomeInitial());

  @override
  Stream<HomeState> mapEventToState(HomeEvent event) async* {
    if (event is HomeProductsRequested) {
      yield HomeProductsLoadInProgress();
      // try {
      final List<Product> products = await homeApi.getProducts(event.page);
      yield HomeProductsLoadSuccess(
        products: products,
        isMore: true,
      );
      // } catch (_) {
      //   yield HomeLoadFailure();
      // }
    }
    if (event is HomeGetMoreProductsRequested) {
      yield HomeProductsLoadInProgress();
      // try {
      final List<Product> products = await homeApi.getProducts(event.page);
      if (products.length == 0) {
        prods = prods;
        yield HomeProductsLoadSuccess(isMore: false, products: prods);
      } else {
        print(prods.length);
        prods += products;
        yield HomeProductsLoadSuccess(isMore: true, products: prods);
      }
      // } catch (_) {
      //   yield HomeLoadFailure();
      // }
    } else if (event is HomeEventsRequested) {
      yield HomeEventsLoadInProgress();
      try {
        final List<Event> events = await homeApi.getEvents();

        yield HomeEventsLoadSuccess(
          events: events,
        );
      } catch (_) {
        yield HomeLoadFailure();
      }
    } else if (event is HomeCaruselRequested) {
      yield HomeCaruselLoadInProgress();
      try {
        final List<String> images = await homeApi.getSliderImages();

        yield HomeCaruselLoadSuccess(
          images: images,
        );
      } catch (_) {
        yield HomeLoadFailure();
      }
    } else if (event is RefreshHomeRequested) {
      try {
        final List<Product> products = await homeApi.getProducts(0);
        final List<Event> events = await homeApi.getEvents();
        final List<String> images = await homeApi.getSliderImages();

        yield HomeLoadSuccess(
          products: products,
          events: events,
          images: images,
        );
      } catch (_) {
        yield HomeLoadFailure();
      }
    } else if (event is EventsRequested) {
      yield HomeLoadInProgress();
      try {
        final List<Event> events = await homeApi.getEvents();

        yield EventsLoadSuccess(events: events);
      } catch (_) {
        yield HomeLoadFailure();
      }
    } else if (event is BlogsRequested) {
      yield HomeLoadInProgress();
      try {
        final List<Blog> blogs = await homeApi.getBlogs();

        yield BlogsLoadSuccess(blogs: blogs);
      } catch (_) {
        yield HomeLoadFailure();
      }
    } else if (event is AboutUsRequested) {
      yield HomeLoadInProgress();
      try {
        final String text = await homeApi.aboutus();

        yield AboutUsLoadSuccess(text: text);
      } catch (_) {
        yield HomeLoadFailure();
      }
    } else if (event is ContactUsRequested) {
      yield HomeLoadInProgress();
      try {
        final List<Branche> branches = await homeApi.getBranches();

        yield ContactUsLoadSuccess(branches: branches);
      } catch (_) {
        yield HomeLoadFailure();
      }
    } else if (event is CheckOrder) {
      yield HomeLoadInProgress();
      // try {
      final Order order = await homeApi.checkOrder(event.id);
      if (order != null) {
        yield CheckOrderLoadSuccess(order: order);
      } else {
        yield CheckOrderNotFound();
      }
      // } catch (_) {
      //   yield HomeLoadFailure();
      // }
    } else if (event is BarcodeScan) {
      yield HomeLoadInProgress();
      try {
        var code = {'barcode': event.code};
        final Product product = await homeApi.scanBarcode(code);
        if (product != null) {
          yield BarcodeScanResult(product: product);
        } else {
          yield BarcodeScanNotFound();
        }
      } catch (_) {
        yield HomeLoadFailure();
      }
    } else if (event is ContactRequested) {
      yield HomeLoadInProgress();
      try {
        var data = {
          'email': event.email,
          'full_name': event.name,
          'subject': event.subject,
          'msg': event.msg,
        };
        bool x = await homeApi.contactUs(data);

        yield ContactUsSuccess();
      } catch (_) {
        yield HomeLoadFailure();
      }
    }
  }
}
