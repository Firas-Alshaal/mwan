import 'package:flutter_dtic/models/Blog.dart';
import 'package:flutter_dtic/models/Branche.dart';
import 'package:flutter_dtic/models/Brand.dart';
import 'package:flutter_dtic/models/Gift.dart';
import 'package:flutter_dtic/models/Order.dart';
import 'package:flutter_dtic/models/Product.dart';
import 'package:flutter_dtic/models/event.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

abstract class HomeState extends Equatable {
  const HomeState();

  @override
  List<Object> get props => [];
}

class HomeInitial extends HomeState {}

class HomeLoadInProgress extends HomeState {}

class HomeProductsLoadInProgress extends HomeState {}

class HomeEventsLoadInProgress extends HomeState {}

class HomeCaruselLoadInProgress extends HomeState {}

class CheckOrderNotFound extends HomeState {}

class BarcodeScanNotFound extends HomeState {}

class ContactUsSuccess extends HomeState {}

class HomeLoadSuccess extends HomeState {
  final List<Product> products;
  final List<Event> events;
  final List<String> images;

  const HomeLoadSuccess({
    @required this.products,
    @required this.events,
    @required this.images,
  }) : assert(products != null);

  @override
  List<Object> get props => [products, events];
}

class HomeProductsLoadSuccess extends HomeState {
  final List<Product> products;
  final isMore;
  const HomeProductsLoadSuccess({
    @required this.products,
    @required this.isMore,
  }) : assert(products != null);

  @override
  List<Object> get props => [
        products,
      ];
}

class HomeGetMoreProductsLoadSuccess extends HomeState {
  final List<Product> products;
  final isMore = false;
  const HomeGetMoreProductsLoadSuccess({
    @required this.products,
  }) : assert(products != null);

  @override
  List<Object> get props => [
        products,
      ];
}

class HomeCaruselLoadSuccess extends HomeState {
  final List<String> images;

  const HomeCaruselLoadSuccess({
    @required this.images,
  }) : assert(images != null);

  @override
  List<Object> get props => [images];
}

class HomeEventsLoadSuccess extends HomeState {
  final List<Event> events;

  const HomeEventsLoadSuccess({
    @required this.events,
  }) : assert(events != null);

  @override
  List<Object> get props => [events];
}

class EventsLoadSuccess extends HomeState {
  final List<Event> events;

  const EventsLoadSuccess({
    @required this.events,
  }) : assert(events != null);

  @override
  List<Object> get props => [events];
}

class CheckOrderLoadSuccess extends HomeState {
  final Order order;

  const CheckOrderLoadSuccess({
    @required this.order,
  }) : assert(order != null);

  @override
  List<Object> get props => [order];
}

class ContactUsLoadSuccess extends HomeState {
  final List<Branche> branches;

  const ContactUsLoadSuccess({
    @required this.branches,
  }) : assert(branches != null);

  @override
  List<Object> get props => [branches];
}

class BarcodeScanResult extends HomeState {
  final Product product;

  const BarcodeScanResult({
    @required this.product,
  }) : assert(product != null);

  @override
  List<Object> get props => [product];
}

class AboutUsLoadSuccess extends HomeState {
  final String text;

  const AboutUsLoadSuccess({
    @required this.text,
  }) : assert(text != null);

  @override
  List<Object> get props => [text];
}

class BlogsLoadSuccess extends HomeState {
  final List<Blog> blogs;

  const BlogsLoadSuccess({
    @required this.blogs,
  }) : assert(blogs != null);

  @override
  List<Object> get props => [blogs];
}

class HomeLoadFailure extends HomeState {}
