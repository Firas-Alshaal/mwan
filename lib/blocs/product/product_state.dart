import 'package:flutter_dtic/models/Brand.dart';
import 'package:flutter_dtic/models/Category.dart';
import 'package:flutter_dtic/models/Product.dart';
import 'package:flutter_dtic/models/event.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

abstract class ProductState extends Equatable {
  const ProductState();

  @override
  List<Object> get props => [];
}

class ProductsInitial extends ProductState {}

class ProductsLoadInProgress extends ProductState {}

class CategoryProductsLoadSuccess extends ProductState {
  final List<Product> products;

  const CategoryProductsLoadSuccess({
    @required this.products,
  }) : assert(products != null);

  @override
  List<Object> get props => [products];
}

class ProductsLoadSuccess extends ProductState {
  final List<Product> products;
  final List<Category> categories;
  final List<Brand> brands;

  const ProductsLoadSuccess({
    @required this.products,
    @required this.categories,
    @required this.brands,
  }) : assert(products != null);

  @override
  List<Object> get props => [products, categories, brands];
}

class ProductLoadSuccess extends ProductState {
  final Product product;

  const ProductLoadSuccess({
    @required this.product,
  }) : assert(product != null);

  @override
  List<Object> get props => [product];
}

class SearchResult extends ProductState {
  final List<Product> products;

  const SearchResult({
    @required this.products,
  }) : assert(products != null);

  @override
  List<Object> get props => [products];
}

class ProductsLoadFailure extends ProductState {}
