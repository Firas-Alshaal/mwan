import 'package:flutter_dtic/models/Brand.dart';
import 'package:flutter_dtic/models/Category.dart';
import 'package:flutter_dtic/models/Product.dart';
import 'package:flutter_dtic/services/ProductApi.dart';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';

import 'product_event.dart';
import 'product_state.dart';

class ProductBloc extends Bloc<ProductEvent, ProductState> {
  final ProductApi productApi;

  ProductBloc({@required this.productApi})
      : assert(productApi != null),
        super(ProductsInitial());

  @override
  Stream<ProductState> mapEventToState(ProductEvent event) async* {
    if (event is ProductsRequested) {
      yield ProductsLoadInProgress();
      try {
        final List<Category> categories = await productApi.getCategories();
        List<Product> products = [];
        if (categories.length > 0) {
          products = await productApi.getCategoryProducts(categories[0].id);
        }
        final List<Brand> brands = await productApi.getBrands();

        yield ProductsLoadSuccess(
          categories: categories,
          brands: brands,
          products: products,
        );
      } catch (_) {
        yield ProductsLoadFailure();
      }
    } else if (event is CategoryProductsRequested) {
      yield ProductsLoadInProgress();
      try {
        final List<Product> products =
            await productApi.getCategoryProducts(event.id);

        yield CategoryProductsLoadSuccess(products: products);
      } catch (_) {
        yield ProductsLoadFailure();
      }
    } else if (event is SingleProductRequested) {
      yield ProductsLoadInProgress();
      try {
        final Product product = await productApi.getSingleProduct(event.id);

        yield ProductLoadSuccess(product: product);
      } catch (_) {
        yield ProductsLoadFailure();
      }
    } else if (event is SearchRequested) {
      yield ProductsLoadInProgress();
      try {
        var data = {
          'tags': [event.query],
        };
        final List<Product> products = await productApi.search(data);

        yield SearchResult(products: products);
      } catch (_) {
        yield ProductsLoadFailure();
      }
    } else if (event is SearchByBrandRequested) {
      yield ProductsLoadInProgress();
      try {
        var data;
        if (event.query != null) {
          data = {
            'tags': [event.query],
            'brand': event.brand,
          };
        } else
          data = {
            'brand': event.brand,
          };
        final List<Product> products = await productApi.search(data);

        yield SearchResult(products: products);
      } catch (_) {
        yield ProductsLoadFailure();
      }
    }
  }
}
