import 'package:flutter_dtic/models/Brand.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

abstract class ProductEvent extends Equatable {
  const ProductEvent();
}

class ProductsRequested extends ProductEvent {
  const ProductsRequested();

  @override
  List<Object> get props => [];
}

class SingleProductRequested extends ProductEvent {
  final int id;
  const SingleProductRequested({this.id});

  @override
  List<Object> get props => [];
}

class CategoryProductsRequested extends ProductEvent {
  final int id;
  const CategoryProductsRequested({this.id});

  @override
  List<Object> get props => [];
}

class SearchRequested extends ProductEvent {
  final String query;
  const SearchRequested({this.query});

  @override
  List<Object> get props => [];
}

class SearchByBrandRequested extends ProductEvent {
  final String query;
  final String brand;

  const SearchByBrandRequested({this.query, this.brand});

  @override
  List<Object> get props => [];
}
