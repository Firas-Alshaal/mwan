import 'package:flutter_dtic/models/Blog.dart';
import 'package:flutter_dtic/models/Brand.dart';
import 'package:flutter_dtic/models/Job.dart';
import 'package:flutter_dtic/models/Product.dart';
import 'package:flutter_dtic/models/event.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

abstract class JobState extends Equatable {
  const JobState();

  @override
  List<Object> get props => [];
}

class JobInitial extends JobState {}

class JobLoadInProgress extends JobState {}

class JobApplied extends JobState {}

class JobNotApplied extends JobState {}

class JobsLoadSuccess extends JobState {
  final List<Job> jobs;

  const JobsLoadSuccess({
    @required this.jobs,
  }) : assert(jobs != null);

  @override
  List<Object> get props => [jobs];
}

class JobLoadFailure extends JobState {}
