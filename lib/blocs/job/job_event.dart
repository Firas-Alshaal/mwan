import 'dart:io';

import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

abstract class JobEvent extends Equatable {
  const JobEvent();
}

class JobsRequested extends JobEvent {
  const JobsRequested();

  @override
  List<Object> get props => [];
}

class ApplyJobRequested extends JobEvent {
  final int id;
  final String first_name;
  final String last_name;
  final String email;
  final String details;
  final File cv;
  const ApplyJobRequested({
    this.id,
    this.first_name,
    this.last_name,
    this.email,
    this.details,
    this.cv,
  });

  @override
  List<Object> get props => [];
}
