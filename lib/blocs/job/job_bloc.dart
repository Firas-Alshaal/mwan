import 'package:flutter_dtic/models/Blog.dart';
import 'package:flutter_dtic/models/Brand.dart';
import 'package:flutter_dtic/models/Job.dart';
import 'package:flutter_dtic/models/Product.dart';
import 'package:flutter_dtic/models/event.dart';
import 'package:flutter_dtic/services/HomeApi.dart';
import 'package:flutter_dtic/services/JopApi.dart';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';

import 'job_event.dart';
import 'job_state.dart';

class JobBloc extends Bloc<JobEvent, JobState> {
  final JobApi jobApi;

  JobBloc({@required this.jobApi})
      : assert(jobApi != null),
        super(JobInitial());

  @override
  Stream<JobState> mapEventToState(JobEvent event) async* {
    if (event is JobsRequested) {
      yield JobLoadInProgress();
      try {
        final List<Job> jobs = await jobApi.getJobs();

        yield JobsLoadSuccess(jobs: jobs);
      } catch (_) {
        yield JobLoadFailure();
      }
    } else if (event is ApplyJobRequested) {
      yield JobLoadInProgress();
      try {
        var data = {
          'first_name': event.first_name,
          'last_name': event.last_name,
          'email': event.email,
          'description': event.details,
        };
        bool x = await jobApi.applyJob(event.id, data, event.cv);

        if (x) {
          yield JobApplied();
        } else {
          yield JobNotApplied();
        }
      } catch (_) {
        yield JobLoadFailure();
      }
    }
  }
}
