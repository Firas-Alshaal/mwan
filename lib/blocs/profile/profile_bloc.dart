import 'package:flutter_dtic/models/Blog.dart';
import 'package:flutter_dtic/models/Brand.dart';
import 'package:flutter_dtic/models/City.dart';
import 'package:flutter_dtic/models/Job.dart';
import 'package:flutter_dtic/models/Order.dart';
import 'package:flutter_dtic/models/Product.dart';
import 'package:flutter_dtic/models/User.dart';
import 'package:flutter_dtic/models/event.dart';
import 'package:flutter_dtic/services/HomeApi.dart';
import 'package:flutter_dtic/services/JopApi.dart';
import 'package:flutter_dtic/services/ProfileApi.dart';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../constants.dart';
import 'profile_event.dart';
import 'profile_state.dart';

class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  final ProfileApi profileApi;

  ProfileBloc({@required this.profileApi})
      : assert(profileApi != null),
        super(ProfileInitial());

  @override
  Stream<ProfileState> mapEventToState(ProfileEvent event) async* {
    if (event is ProfileRequested) {
      yield ProfileLoadInProgress();
      // try {
      final User user = await profileApi.getProfile();
      final List<Order> orders = await profileApi.getOrders();
      List<City> cities = await profileApi.getCities();

      yield ProfileLoadSuccess(
        user: user,
        orders: orders,
        cities: cities,
      );
      // } catch (_) {
      //   yield ProfileLoadFailure();
      // }
    } else if (event is UpdateProfile) {
      yield ProfileLoadInProgress();
      // try {
      var data = {
        'first_name': event.first_name,
        'last_name': event.last_name,
        'email': event.email,
        'phone': event.phone,
        'password': event.password,
        'city_id': event.city_id,
      };
      Map res = await profileApi.updateProfile(data);
      if (res['status']) {
        SharedPreferences sharedPreferences =
            await SharedPreferences.getInstance();
        sharedPreferences.setBool('ResgisterOk', false);
//        ResgisterOk = false;
        final User user = res['user'];
        yield UpdateProfileLoadSuccess(user: user, msg: res['msg']);
      } else {
        yield UpdateProfileFailed(msg: res['msg']);
      }
      // } catch (_) {
      //   yield ProfileLoadFailure();
      // }
    } else if (event is ResendCodeRequested) {
      yield ProfileLoadInProgress();
      try {
//        var data = {
//          'order_id': event.order_id,
//        };
        final bool user = await profileApi.resendCodeOrder(event.order_id);

        yield ResendCodeSuccess(status: user, id: event.order_id);
      } catch (_) {
        yield ProfileLoadFailure();
      }
    }
  }
}
