import 'package:flutter_dtic/models/Blog.dart';
import 'package:flutter_dtic/models/Brand.dart';
import 'package:flutter_dtic/models/City.dart';
import 'package:flutter_dtic/models/Job.dart';
import 'package:flutter_dtic/models/Order.dart';
import 'package:flutter_dtic/models/Product.dart';
import 'package:flutter_dtic/models/User.dart';
import 'package:flutter_dtic/models/event.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

abstract class ProfileState extends Equatable {
  const ProfileState();

  @override
  List<Object> get props => [];
}

class ProfileInitial extends ProfileState {}

class ProfileLoadInProgress extends ProfileState {}

class ProfileNotUpdated extends ProfileState {}

class UpdateProfileLoadSuccess extends ProfileState {
  final User user;
  final String msg;

  const UpdateProfileLoadSuccess({@required this.user, @required this.msg})
      : assert(user != null);

  @override
  List<Object> get props => [user];
}

class UpdateProfileFailed extends ProfileState {
  final String msg;

  const UpdateProfileFailed({@required this.msg}) : assert(msg != null);

  @override
  List<Object> get props => [msg];
}

class ResendCodeSuccess extends ProfileState {
  final bool status;
  final int id;

  const ResendCodeSuccess({
    @required this.status,
    @required this.id,
  }) : assert(status != null);

  @override
  List<Object> get props => [status];
}

class ProfileLoadSuccess extends ProfileState {
  final User user;
  final List<Order> orders;
  final List<City> cities;

  const ProfileLoadSuccess({
    @required this.user,
    @required this.cities,
    this.orders,
  }) : assert(user != null && orders != null);

  @override
  List<Object> get props => [user, orders];
}

class ProfileLoadFailure extends ProfileState {}
