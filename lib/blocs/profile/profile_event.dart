import 'dart:io';

import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

abstract class ProfileEvent extends Equatable {
  const ProfileEvent();
}

class ProfileRequested extends ProfileEvent {
  const ProfileRequested();

  @override
  List<Object> get props => [];
}

class ResendCodeRequested extends ProfileEvent {
  final int order_id;
  const ResendCodeRequested({this.order_id});

  @override
  List<Object> get props => [];
}

class UpdateProfile extends ProfileEvent {
  final String first_name;
  final String last_name;
  final String email;
  final String phone;
  final String password;
  final String city_id;
  const UpdateProfile({
    this.first_name,
    this.last_name,
    this.city_id,
    this.email,
    this.phone,
    this.password,
  });

  @override
  List<Object> get props => [];
}
