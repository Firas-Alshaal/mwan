import 'dart:convert';

import 'package:flutter_dtic/models/Address.dart';
import 'package:flutter_dtic/models/City.dart';
import 'package:flutter_dtic/models/Product.dart';
import 'package:flutter_dtic/models/event.dart';

import 'Api.dart';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

class CartApi extends Api {
  final http.Client httpClient;

  CartApi({
    @required this.httpClient,
  }) : assert(httpClient != null);

  Future<List<City>> getCities() async {
    final url = '${Api.baseUrl}/cities';

    final response = await http.get(url, headers: await getHeaders());

    var res = jsonDecode(response.body)['data'] as List;
    List<City> cities = res.map((e) => City.fromJson(e)).toList();

    return cities;
  }

  Future<List<Address>> getAddresses() async {
    final url = '${Api.baseUrl}/addresses';

    final response = await http.get(url, headers: await getHeaders());

    var res = jsonDecode(response.body)['data'] as List;
    List<Address> cities = res.map((e) => Address.fromJson(e)).toList();

    return cities;
  }

  Future<String> getLimit() async {
    final url = '${Api.baseUrl}/orders/limit';

    final response = await http.get(url, headers: await getHeaders());

    var res = jsonDecode(response.body)['data'];
    String s = res['min_checkout_limit'].toString();

    return s;
  }

  Future<Address> storeAddress(data) async {
    final url = '${Api.baseUrl}/addresses';

    final response = await http.post(url,
        body: jsonEncode(data), headers: await getHeaders());

    var res = jsonDecode(response.body)['data'];

    Address address = Address.fromJson(res);
    return address;
  }

  Future<Map> checkout(data) async {
    final url = '${Api.baseUrl}/orders';
    final response = await http.post(url,
        body: jsonEncode(data), headers: await getHeaders());

    var res = jsonDecode(response.body);
    print(res);

    if (res['status']) {
      Map m = {
        'status': res['data']['status'],
        'id': res['data']['id'],
      };
      return m;
    }
    String msg = "";
    if (res['status']) {
      msg = res['message'];
    } else {
      if (res['message']['errors'] is String) {
        msg = res['message']['errors'];
      } else {
        msg = res['message']['errors'].values.elementAt(0)[0];
      }
    }
    return {'status': -1, 'msg': msg};
  }

  Future<bool> verifyOrder(data, id) async {
    final url = '${Api.baseUrl}/verify-order/$id';
    final response = await http.post(url,
        body: jsonEncode(data), headers: await getHeaders());
    var res = jsonDecode(response.body);
    print(res);
    return res['status'];
  }

  Future<String> checkCoupon(data) async {
    final url = '${Api.baseUrl}/check_coupon';
    final response = await http.post(url,
        body: jsonEncode(data), headers: await getHeaders());

    var res = jsonDecode(response.body);
    if (res['status']) {
      return res['data'];
    } else
      return "";
  }
}
