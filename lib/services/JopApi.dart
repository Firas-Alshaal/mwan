import 'dart:convert';

import 'package:flutter_dtic/models/Blog.dart';
import 'package:flutter_dtic/models/Brand.dart';
import 'package:flutter_dtic/models/Job.dart';
import 'package:flutter_dtic/models/Product.dart';
import 'package:flutter_dtic/models/event.dart';

import 'Api.dart';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

class JobApi extends Api {
  final http.Client httpClient;

  JobApi({
    @required this.httpClient,
  }) : assert(httpClient != null);

  Future<List<Job>> getJobs() async {
    final url = '${Api.baseUrl}/jobs';

    final response = await http.get(url, headers: await getHeaders());

    var res = jsonDecode(response.body)['data'] as List;
    print(res);
    List<Job> jobs = res.map((e) => Job.fromJson(e)).toList();

    return jobs;
  }

  Future<bool> applyJob(id, data, file) async {
    final url = '${Api.baseUrl}/jobs/$id/careers';

    Uri uri = Uri.parse(url);

    http.MultipartRequest request = http.MultipartRequest("POST", uri);

    http.MultipartFile multipartFile = http.MultipartFile(
      'cv_path',
      file.readAsBytes().asStream(),
      file.lengthSync(),
      filename: file.path,
    );

    request.files.add(multipartFile);
    request.fields.addAll(data);
    var res = await request.send();
    var response = await http.Response.fromStream(res);
    print(response.body);
    var resp = jsonDecode(response.body);
    return resp['status'];
  }
}
