import 'dart:convert';

import 'package:flutter_dtic/models/Brand.dart';
import 'package:flutter_dtic/models/Category.dart';
import 'package:flutter_dtic/models/Product.dart';
import 'package:flutter_dtic/models/event.dart';

import 'Api.dart';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

class ProductApi extends Api {
  final http.Client httpClient;

  ProductApi({
    @required this.httpClient,
  }) : assert(httpClient != null);

  Future<List<Product>> getCategoryProducts(id) async {
    final url = '${Api.baseUrl}/categories/$id/products';

    final response = await http.get(url, headers: await getHeaders());

    var res = jsonDecode(response.body)['data'] as List;
    List<Product> products = res.map((e) => Product.fromJson(e)).toList();

    return products;
  }

  Future<List<Brand>> getBrands() async {
    final url = '${Api.baseUrl}/brands';

    final response = await http.get(url, headers: await getHeaders());

    var res = jsonDecode(response.body)['data'] as List;
    List<Brand> brands = res.map((e) => Brand.fromJson(e)).toList();

    return brands;
  }

  Future<List<Category>> getCategories() async {
    final url = '${Api.baseUrl}/categories';

    final response = await http.get(url, headers: await getHeaders());

    var res = jsonDecode(response.body)['data'] as List;
    List<Category> categories = res.map((e) => Category.fromJson(e)).toList();

    return categories;
  }

  Future<List<Product>> search(data) async {
    final url = '${Api.baseUrl}/product/search';
    print(data);
    final response = await http.post(url,
        body: jsonEncode(data), headers: await getHeaders());

    var res = jsonDecode(response.body)['data'] as List;
    List<Product> products = res.map((e) => Product.fromJson(e)).toList();

    return products;
  }

  Future<Product> getSingleProduct(id) async {
    final url = '${Api.baseUrl}/products/$id';
    print('id $id');
    final response = await http.get(url, headers: await getHeaders());
    print('ress ${response.body}');
    var res = jsonDecode(response.body)['data'];

    Product product = Product.fromJson(res);

    return product;
  }

// Future<List<Product>> search(query) async {
//   final url = '${Api.baseUrl}/products/$id';

//   final response = await http.get(url, headers: await getHeaders());

//   var res = jsonDecode(response.body)['data'];
//   Product product = Product.fromJson(res);

//   return product;
// }
}
