import 'dart:convert';

import 'package:flutter_dtic/constants.dart';
import 'package:flutter_dtic/models/Blog.dart';
import 'package:flutter_dtic/models/Branche.dart';
import 'package:flutter_dtic/models/Brand.dart';
import 'package:flutter_dtic/models/Gift.dart';
import 'package:flutter_dtic/models/Order.dart';
import 'package:flutter_dtic/models/Product.dart';
import 'package:flutter_dtic/models/event.dart';

import 'Api.dart';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

class HomeApi extends Api {
  final http.Client httpClient;

  HomeApi({
    @required this.httpClient,
  }) : assert(httpClient != null);

  Future<List<Product>> getProducts(page) async {
    final url =
        'https://demo.mawenapp.com/laravel_backend/public/api/products?page=$page';

    final response = await http.get(url, headers: await getHeaders());
    var number = jsonDecode(response.body)['meta']['total_pages'];
    print(number);
    var res = jsonDecode(response.body)['data'] as List;
    List<Product> products = res.map((e) => Product.fromJson(e)).toList();
    return products;
  }

  Future<List<Event>> getEvents() async {
    final url = '${Api.baseUrl}/events';

    final response = await http.get(url, headers: await getHeaders());

    var res = jsonDecode(response.body)['data'] as List;
    List<Event> events = res.map((e) => Event.fromJson(e)).toList();

    return events;
  }

//
  Future<List<Blog>> getBlogs() async {
    final url = '${Api.baseUrl}/articles';

    final response = await http.get(url, headers: await getHeaders());

    var res = jsonDecode(response.body)['data'] as List;
    List<Blog> blogs = res.map((e) => Blog.fromJson(e)).toList();
    return blogs;
  }

  Future<List<Event>> getLatestEvents() async {
    final url = '${Api.baseUrl}/events';

    final response = await http.get(url, headers: await getHeaders());

    var res = jsonDecode(response.body)['data'] as List;
    List<Event> events = res.map((e) => Event.fromJson(e)).toList();

    return events;
  }

  Future<List<Branche>> getBranches() async {
    final url = '${Api.baseUrl}/branches';

    final response = await http.get(url, headers: await getHeaders());
    print(response.body);
    var res = jsonDecode(response.body)['data'] as List;
    List<Branche> events = res.map((e) => Branche.fromJson(e)).toList();

    return events;
  }

  Future<List<Brand>> getBrands() async {
    final url = '${Api.baseUrl}/brands';

    final response = await http.get(url, headers: await getHeaders());

    var res = jsonDecode(response.body)['data'] as List;
    List<Brand> brands = res.map((e) => Brand.fromJson(e)).toList();

    return brands;
  }

  Future<List<String>> getSliderImages() async {
    final url = '${Api.baseUrl}/sliders';

    final response = await http.get(url, headers: await getHeaders());

    var res = jsonDecode(response.body)['data'] as List;
    List<String> brands = res.map((e) => e['image_path'].toString()).toList();

    return brands;
  }

  Future<String> aboutus() async {
    final url = '${Api.baseUrl}/about-us';

    final response = await http.get(url, headers: await getHeaders());

    var res = jsonDecode(response.body)['data'];
    String txt = res['content:$LANGUAGE'];

    return txt;
  }

  Future<Order> checkOrder(id) async {
    final url = '${Api.baseUrl}/orders/$id/status';

    final response = await http.get(url, headers: await getHeaders());

    var res = jsonDecode(response.body);
    Order order;
    if (res['status']) {
      order = Order.fromJson(res['data']);
    }

    return order;
  }

  Future<Product> scanBarcode(code) async {
    final url =
        'https://demo.mawenapp.com/laravel_backend/public/api/add-by-barcode';

    final response = await http.post(url,
        body: jsonEncode(code), headers: await getHeaders());

    var res = jsonDecode(response.body);
    print(res);
    Product product;
    if (response.statusCode == 200) {
      product = Product.fromJson(res['data']);
    }

    return product;
  }

  Future<bool> contactUs(data) async {
    final url = '${Api.baseUrl}/contact-us';

    final response = await http.post(url,
        body: jsonEncode(data), headers: await getHeaders());

    var res = jsonDecode(response.body);

    return res['status'];
  }
}
