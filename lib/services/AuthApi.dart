import 'dart:convert';

import 'package:flutter_dtic/models/City.dart';
import 'package:flutter_dtic/models/Product.dart';
import 'package:flutter_dtic/models/event.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../constants.dart';
import 'Api.dart';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

class AuthApi extends Api {
  final http.Client httpClient;

  AuthApi({
    @required this.httpClient,
  }) : assert(httpClient != null);

  Future<int> login(data) async {
    final url = '${Api.baseUrl}/login';

    final response = await http.post(url,
        body: jsonEncode(data), headers: await getHeaders());
    print(url);
    var res = jsonDecode(response.body);
    print(res);
    bool x = res['status'];
    if (x) {
      var user = res['data']['user'];
      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();

      sharedPreferences.setString('user', jsonEncode(user));
      sharedPreferences.setString('token', res['data']['token']);
      sharedPreferences.setBool('isAuth', true);
      ID = user['id'];
      NAME = user['first_name'];
      TOKEN = res['data']['token'];
      EMAIL = user['email'];

      ISAuth = true;
      return 1;
    } else {
      if (res['message'] == "Account Not Verified") {
        return 2;
      }
    }
    return 0;
  }

  Future<Map> register(data) async {
    final url = 'https://demo.mawenapp.com/laravel_backend/public/api/register';

    final response = await http.post(url,
        body: jsonEncode(data), headers: await getHeaders());

    var res = jsonDecode(response.body);
    bool x = res['status'];
    if (x) {
      var user = res['data']['user'];

      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();

      sharedPreferences.setString('user', jsonEncode(user));
      sharedPreferences.setString('token', res['data']['token']);
      sharedPreferences.setBool('isAuth', true);
      ID = user['id'];
      NAME = user['first_name'];
      EMAIL = user['email'];
      TOKEN = res['data']['token'];
      ISAuth = true;
    }
    String msg = "";
    if (res['status']) {
      msg = res['message'];
    } else {
      if (res['message']['errors'] is String) {
        msg = res['message']['errors'];
      } else {
        msg = res['message']['errors'].values.elementAt(0)[0];
      }
    }
    Map m = {
      'status': x,
      'msg': msg,
    };

    return m;
  }

  Future<List<City>> getCities() async {
    final url = '${Api.baseUrl}/cities';

    final response = await http.get(url, headers: await getHeaders());

    var res = jsonDecode(response.body)['data'] as List;
    List<City> cities = res.map((e) => City.fromJson(e)).toList();

    return cities;
  }

  Future<bool> resetPassword(data) async {
    final url = '${Api.baseUrl}/reset-password';

    final response = await http.post(url,
        body: jsonEncode(data), headers: await getHeaders());

    var res = jsonDecode(response.body);

    bool x = res['status'];
    return x;
  }

  Future<void> geturl() async {
    Api.baseUrl = "https://demo.mawenapp.com/laravel_backend/public/api";
  }

  Future<bool> sendCode(data) async {
    final url = '${Api.baseUrl}/verify-email';

    final response = await http.post(url,
        body: jsonEncode(data), headers: await getHeaders());

    var res = jsonDecode(response.body);

    bool x = res['status'];
    if (x) {
      var user = res['data']['user'];

      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();

      sharedPreferences.setString('user', jsonEncode(user));
      sharedPreferences.setString('token', res['data']['token']);
      sharedPreferences.setBool('isAuth', true);
      ID = user['id'];
      NAME = user['first_name'];
      TOKEN = res['data']['token'];
      ISAuth = true;
    }
    return x;
  }

  Future<Map> googleRegister(data) async {
    final url = '${Api.baseUrl}/social/login';

    final response = await http.post(url,
        body: jsonEncode(data), headers: await getHeaders());
    print(response.body);
    var res = jsonDecode(response.body);
    bool x = res['status'];
    if (x) {
      var user = res['data']['user'];

      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();

      sharedPreferences.setString('user', jsonEncode(user));
      sharedPreferences.setString('token', res['data']['token']);
      sharedPreferences.setBool('isAuth', true);
      sharedPreferences.setBool('ResgisterOk', true);
      ID = user['id'];
      NAME = user['first_name'];
      EMAIL = user['email'];
      TOKEN = res['data']['token'];
      ISAuth = true;
      ResgisterOk = true;
      // ResgisterOk = user['missingParams'];
    }
    String msg = "";
    if (res['status']) {
      msg = res['message'];
    } else {
      if (res['message']['errors'] is String) {
        msg = res['message']['errors'];
      } else {
        msg = res['message']['errors'].values.elementAt(0)[0];
      }
    }
    Map m = {
      'status': x,
      'msg': msg,
    };

    return m;
  }

  Future<Map> facebookRegister(data) async {
    final url = '${Api.baseUrl}/social/login';

    final response = await http.post(url,
        body: jsonEncode(data), headers: await getHeaders());
    print(response.body);
    var res = jsonDecode(response.body);
    bool x = res['status'];
    if (x) {
      var user = res['data']['user'];

      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();

      sharedPreferences.setString('user', jsonEncode(user));
      sharedPreferences.setString('token', res['data']['token']);
      sharedPreferences.setBool('isAuth', true);
      sharedPreferences.setBool('ResgisterOk', true);
      ID = user['id'];
      NAME = user['first_name'];
      EMAIL = user['email'];
      TOKEN = res['data']['token'];
      ISAuth = true;
      ResgisterOk = true;
    }
    String msg = "";
    if (res['status']) {
      msg = res['message'];
    } else {
      if (res['message']['errors'] is String) {
        msg = res['message']['errors'];
      } else {
        msg = res['message']['errors'].values.elementAt(0)[0];
      }
    }
    Map m = {
      'status': x,
      'msg': msg,
    };

    return m;
  }
}
