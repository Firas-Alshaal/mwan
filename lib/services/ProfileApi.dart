import 'dart:convert';

import 'package:flutter_dtic/models/Blog.dart';
import 'package:flutter_dtic/models/Brand.dart';
import 'package:flutter_dtic/models/City.dart';
import 'package:flutter_dtic/models/Job.dart';
import 'package:flutter_dtic/models/Order.dart';
import 'package:flutter_dtic/models/Product.dart';
import 'package:flutter_dtic/models/User.dart';
import 'package:flutter_dtic/models/event.dart';

import 'Api.dart';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

class ProfileApi extends Api {
  final http.Client httpClient;

  ProfileApi({
    @required this.httpClient,
  }) : assert(httpClient != null);

  Future<User> getProfile() async {
    final url = '${Api.baseUrl}/profile';

    final response = await http.get(url, headers: await getHeaders());

    var res = jsonDecode(response.body)['data'];
    print(res);
    User u = User.fromJson(res);

    return u;
  }

  Future<List<Order>> getOrders() async {
    final url = '${Api.baseUrl}/user/orders';

    final response = await http.get(url, headers: await getHeaders());
    print(response.body);
    var res = jsonDecode(response.body)['data'] as List;

    List<Order> orders = res.map((e) => Order.fromMyJson(e)).toList();

    return orders;
  }

  Future<bool> resendCodeOrder(id) async {
    final url = '${Api.baseUrl}/orders/resend/otp/$id';

    final response = await http.get(url, headers: await getHeaders());
    print(response.body);
    var res = jsonDecode(response.body)['status'];

    return res;
  }

  Future<Map> updateProfile(data) async {
    final url = '${Api.baseUrl}/update-profile';
    final response = await http.put(url,
        body: jsonEncode(data), headers: await getHeaders());
    print(response.body);
    var resp = jsonDecode(response.body);
    User u;
    if (resp['status']) {
      var res = jsonDecode(response.body)['data']['user'];
      u = User.fromJson(res);
    }
    String msg = "";
    if (resp['status']) {
      msg = resp['message'];
    } else {
      if (resp['message']['errors'] is String) {
        msg = resp['message']['errors'];
      } else {
        msg = resp['message']['errors'].values.elementAt(0)[0];
      }
    }
    Map map = {
      'status': resp['status'],
      'user': u,
      'msg': msg,
    };
    return map;
  }

  Future<List<City>> getCities() async {
    final url = '${Api.baseUrl}/cities';

    final response = await http.get(url, headers: await getHeaders());

    var res = jsonDecode(response.body)['data'] as List;
    List<City> cities = res.map((e) => City.fromJson(e)).toList();

    return cities;
  }
}
